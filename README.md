# Web Admin by [ADKGamers](http://www.adkgamers.com/)

A web based administration tool for Battlefield 3 and Battlefield 4. It's designed to work with the [ProCon RCON](https://myrcon.com/) tool plugins [AdKats](https://forum.myrcon.com/showthread.php?6045) and [XpKiller's Chat, GUID, Stats and Mapstats Logger](https://forum.myrcon.com/showthread.php?6698)

*   Server Population Feed
*   Recent Bans Feed
*   Recent Admin Reports
*   Chatlog searching
*   Player History
*   Live Scoreboard with admin integration
    *   Includes live chat feed from server
*   User Manangement with groups
*   Add/Edit/Remove Bans
*   and more!

# Dependencies

You need at least PHP 5.4+ and the Mcrypt extenstion enabled

Mcrypt should already be installed and enabled on most hosting providers.

AdKats - Version 4.0 or higher

XpKiller Chat, GUID, Stats and Mapstats Logger - Version 1.0.0.2 or higher

# Installation

Instructions [here](http://www.adkgamers.com/topic/45777-install-instructions/)

# Copyright & CC Licence

The web admin is copyrighted by ADKGamers and is also under the [CC Licence Attribution-NonCommercial-NoDerivatives 4.0 International](http://creativecommons.org/licenses/by-nc-nd/4.0/).

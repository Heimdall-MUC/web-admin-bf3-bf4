$.boardActiveFetch = true;
$.chatActiveFetch = true;

function getHashUrl() {
    if (window.location.hash) {
        var hash = window.location.hash.substring(1);
        return hash;
    }
    else {
        return false;
    }
}

function setHashUrl(hashid) {
    window.location.hash = hashid;
}

$(document).ready(function () {
    $("[name=data_container]").hide();
    //Set up the inactive timer
    $(document).idleTimer("destroy");
    $(document).idleTimer(1800000);
    $(document).on('idle.idleTimer', function () {
        if ($("#server_select").val() != 'none') {
            $(document).idleTimer("destroy");
            $.boardActiveFetch = false;
            $.chatActiveFetch = false;
            $.modal.alert('You have been idle for 30 minutes. Live scoreboard and chat have been disabled. To renable close this message.',
                {
                    resizable: false,
                    blocker: false,
                    blockerVisible: false,
                    closeOnBlur: true,
                    classes: ['orange-gradient'],
                    contentBg: false,
                    buttons: {
                        'Close': {
                            classes: 'blue-gradient glossy big full-width',
                            click: function (modal) {
                                $.boardActiveFetch = true;
                                $.chatActiveFetch = true;
                                $(document).idleTimer(180000);
                                modal.closeModal();
                            }
                        }
                    }
                });
        }
    });

    //Set up events for autorefresh
    $("#scoreboard_refresh").change(function () {
        if ($(this).is(":checked") === true && $("#server_select").val() != 'none') {
            $("#scoreboard_refresh_button").hide();
            $.boardActiveFetch = true;
        }
        else {
            $("#scoreboard_refresh_button").show();
            $.boardActiveFetch = false;
        }
    });

    //set up events for server change
    $("#server_select").change(function () {
        $("#team1 > tbody, #team2 > tbody, #server_name, #round_time, #server_uptime, #current_map, #ticket_count_us, #ticket_count_ru, #chatlogs").empty();
        $.latestChatID = null;
        if ($(this).val() == "none") {
            $("[name=data_container], #auto_refresh_span").hide();
            setHashUrl($(this).val());
        }
        else {
            $("[name=data_container], #auto_refresh_span").show();
            setHashUrl($(this).val());
        }
    });

    //Set the hash
    if (getHashUrl()) {
        $("#server_select").val(getHashUrl()).change();
        setHashUrl($("#server_select").val());
    }

    //Call initial fetch of data
    if($("#server_select").val() != "none")
    {
        getServerData($("#server_select").val());
    }
    $.boardIntervalID = setInterval(getServerData, 5000);
    $.chatIntervalID = setInterval(getServerChat, 2000);
});

function getServerChat(serverID) {
    if ($("#server_select").val() != 'none' && $.chatActiveFetch || serverID != null) {
        $.ajax('api/battlefield/scoreboard/' + (serverID != null ? serverID : $("#server_select").val()) + '/chat',
        {
            type: 'GET',
            data: {
                'previd': $.latestChatID
            },
            dataType: 'json',
            success: function (data) {
                if (data.status == 'error')
                    return false;

                while ($("#chatlogs p").length > 20) {
                    $("#chatlogs p").last().remove();
                }

                //Data is either an array, or a single object
                if (data.more.length > 1) {
                    //Grab and store the latest chat ID
                    $.latestChatID = data.more[0].ID;
                    //Loop over the chat messages
                    $.each(data.more.reverse(), function (index, element) {
                        //Construct each chat message element
                        var chatElement = $('<p class="big-message"><span class="big-message-icon icon-speech"></span><strong>' + element.logSoldierName + '</span></strong><br />' + element.logMessage + '<br /><span class="float-right">' + element.logDate + '</span>' + '</p>');
                        //Check for admin chat
                        if ($.inArray(element.logSoldierName, $.admin_online) > -1) {
                            chatElement.addClass('anthracite-gradient');
                        }
                        //Prepend the constructed chat message
                        $("#chatlogs").prepend(chatElement);
                    });
                }
                else {
                    //Grab and store the latest chat ID
                    $.latestChatID = data.more[0].ID;
                    //Construct the chat message element
                    var chatElement = $('<p class="big-message"><span class="big-message-icon icon-speech"></span><strong>' + data.more[0].logSoldierName + '</span></strong><br />' + data.more[0].logMessage + '<br /><span class="float-right">' + data.more[0].logDate + '</span>' + '</p>');
                    //Check for admin chat
                    if ($.inArray(data.more[0].logSoldierName, $.admin_online) > -1) {
                        chatElement.addClass('anthracite-gradient');
                    }
                    //Prepend the constructed chat message
                    $("#chatlogs").prepend(chatElement);
                }
            },
            error: function () {
                notify("ERROR", 'Connection error. Chat could not be updated.');
            }
        });
    }
}

function getServerData(serverID)
{
    if ($("#server_select").val() != 'none' && $.boardActiveFetch || serverID != null) {
        var serverid = $("#server_select").val();
        $.getJSON('api/battlefield/3/scoreboard/' + serverid, function (data) {
            if (data.status == 'error') {
                notify('ERROR', data.message,
                {
                    classes: ['red-gradient glossy']
                });

                return;
            }

            var serverinfo = data.more.serverinfo,
                teaminfo = data.more.teaminfo;
            $.admin_online = data.more.adminsonline;

            $("#team1 > tbody, #team2 > tbody").empty();

            if (!$.isEmptyObject(data.more.adminsonline)) {
                $("#admins_online").html(data.more.adminsonline.toString()).show();
            }
            else {
                $("#admins_online").hide();
            }

            $("#round_time").text('Rnd: ' + serverinfo.roundTime);
            $("#server_uptime").text('Uptime: ' + serverinfo.serverTime);
            $("#current_map").text(serverinfo.currentmap + ' (' + serverinfo.currentplayers + '/' + serverinfo.maxplayers + ')');
            $("#ticket_count_us").html(teaminfo[1].ticketcount);
            $("#ticket_count_cn").html(teaminfo[2].ticketcount);

            if (teaminfo[1].playerlist !== undefined && teaminfo[1].playerlist.length > 0) {
                var pos = 0;
                $.each(teaminfo[1].playerlist, function () {
                    pos = ++pos;
                    $("#team1 > tbody:last").append(
                        '<tr><td class="low-padding">' + pos + '</td>'
                        + '<td><a href="bf3/playerinfo/' + this.playerid + '" target="_blank">'
                        + this.playername
                        + '</a></td><td class="hide-on-tablet">'
                        + this.playerscore
                        + '</td><td class="hide-on-tablet low-padding"><span class="tag">'
                        + this.playersquad.substring(0, 1)
                        + '</span></td><td class="low-padding">'
                        + this.playerkills
                        + '</td><td class="low-padding">'
                        + this.playerdeaths
                        + '</td></tr>'
                    );
                });
            }

            if (teaminfo[2].playerlist !== undefined && teaminfo[2].playerlist.length > 0) {
                pos = 0;
                $.each(teaminfo[2].playerlist, function () {
                    pos = ++pos;
                    $("#team2 > tbody:last").append(
                        '<tr><td class="low-padding">' + pos + '</td>'
                        + '<td><a href="bf3/playerinfo/' + this.playerid + '" target="_blank">'
                        + this.playername
                        + '</a></td><td class="hide-on-tablet">'
                        + this.playerscore
                        + '</td><td class="hide-on-tablet low-padding"><span class="tag">'
                        + this.playersquad.substring(0, 1)
                        + '</span></td><td class="low-padding">'
                        + this.playerkills
                        + '</td><td class="low-padding">'
                        + this.playerdeaths
                        + '</td></tr>'
                    );
                });
            }

        });
    }
}

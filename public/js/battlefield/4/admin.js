var squadList = new Array();
squadList[0] = 'None';
squadList[1] = 'Alpha';
squadList[2] = 'Bravo';
squadList[3] = 'Charlie';
squadList[4] = 'Delta';
squadList[5] = 'Echo';
squadList[6] = 'Foxtrot';
squadList[7] = 'Golf';
squadList[8] = 'Hotel';
squadList[9] = 'India';
squadList[10] = 'Juliet';
squadList[11] = 'Kilo';
squadList[12] = 'Lima';
squadList[13] = 'Mike';
squadList[14] = 'November';
squadList[15] = 'Oscar';
squadList[16] = 'Papa';
squadList[17] = 'Quebec';
squadList[18] = 'Romeo';
squadList[19] = 'Sierra';
squadList[20] = 'Tango';
squadList[21] = 'Uniform';
squadList[22] = 'Victor';
squadList[23] = 'Whiskey';
squadList[24] = 'Xray';
squadList[25] = 'Yankee';
squadList[26] = 'Zulu';
squadList[27] = 'Haggard';
squadList[28] = 'Sweetwater';
squadList[29] = 'Preston';
squadList[30] = 'Redford';
squadList[31] = 'Faith';
squadList[32] = 'Celeste';

$.boardActiveFetch = true;
$.chatActiveFetch = true;

function getHashUrl() {
    if (window.location.hash) {
        var hash = window.location.hash.substring(1);
        return hash;
    }
    else {
        return false;
    }
}

function setHashUrl(hashid) {
    window.location.hash = hashid;
}

$(document).ready(function () {
    $("[name=data_container]").hide();
    //Set up the inactive timer
    $(document).idleTimer("destroy");
    $(document).idleTimer(1800000);
    $(document).on('idle.idleTimer', function () {
        if ($("#server_select").val() != 'none') {
            $(document).idleTimer("destroy");
            $.boardActiveFetch = false;
            $.chatActiveFetch = false;
            $.modal.alert('You have been idle for 30 minutes. Live scoreboard and chat have been disabled. To renable close this message.',
                {
                    resizable: false,
                    blocker: false,
                    blockerVisible: false,
                    closeOnBlur: true,
                    classes: ['orange-gradient'],
                    contentBg: false,
                    buttons: {
                        'Close': {
                            classes: 'blue-gradient glossy big full-width',
                            click: function (modal) {
                                $.boardActiveFetch = true;
                                $.chatActiveFetch = true;
                                $(document).idleTimer(180000);
                                modal.closeModal();
                            }
                        }
                    }
                });
        }
    });

    //Set up events for autorefresh
    $("#scoreboard_refresh").change(function () {
        if ($(this).is(":checked") === true && $("#server_select").val() != 'none') {
            $("#scoreboard_refresh_button").hide();
            $.boardActiveFetch = true;
        }
        else {
            $("#scoreboard_refresh_button").show();
            $.boardActiveFetch = false;
        }
    });

    //set up events for server change
    $("#server_select").change(function () {
        $("#team1 > tbody, #team2 > tbody, #server_name, #round_time, #server_uptime, #current_map, #ticket_count_us, #ticket_count_ru, #chatlogs").empty();
        $.latestChatID = null;
        if ($(this).val() == "none") {
            $("[name=data_container], #auto_refresh_span").hide();
            setHashUrl($(this).val());
        }
        else {
            $("[name=data_container], #auto_refresh_span").show();
            setHashUrl($(this).val());
            $.adminUrl = "api/battlefield/4/scoreboard/" + $(this).val() + "/admin";
        }
    });

    //Set the hash
    if (getHashUrl()) {
        $("#server_select").val(getHashUrl()).change();
        setHashUrl($("#server_select").val());
    }

    //Call initial fetch of data
    if($("#server_select").val() != "none")
    {
        getServerData($("#server_select").val());
    }
    $.boardIntervalID = setInterval(getServerData, 5000);
    $.chatIntervalID = setInterval(getServerChat, 2000);
    $.adminUrl = "api/battlefield/4/scoreboard/" + $("#server_select").val() + "/admin";
});

function getServerChat(serverID) {
    if ($("#server_select").val() != 'none' && $.chatActiveFetch || serverID != null) {
        $.ajax('api/battlefield/scoreboard/' + (serverID != null ? serverID : $("#server_select").val()) + '/chat',
        {
            type: 'GET',
            data: {
                'previd': $.latestChatID
            },
            dataType: 'json',
            success: function (data) {
                if (data.status == 'error')
                    return false;

                while ($("#chatlogs p").length > 20) {
                    $("#chatlogs p").last().remove();
                }

                //Data is either an array, or a single object
                if (data.more.length > 1) {
                    //Grab and store the latest chat ID
                    $.latestChatID = data.more[0].ID;
                    //Loop over the chat messages
                    $.each(data.more.reverse(), function (index, element) {
                        //Construct each chat message element
                        var chatElement = $('<p class="big-message"><span class="big-message-icon icon-speech"></span><strong>' + element.logSoldierName + '</span></strong><br />' + element.logMessage + '<br /><span class="float-right">' + element.logDate + '</span>' + '</p>');
                        //Check for admin chat
                        if ($.inArray(element.logSoldierName, $.admin_online) > -1) {
                            chatElement.addClass('anthracite-gradient');
                        }
                        //Prepend the constructed chat message
                        $("#chatlogs").prepend(chatElement);
                    });
                }
                else {
                    //Grab and store the latest chat ID
                    $.latestChatID = data.more[0].ID;
                    //Construct the chat message element
                    var chatElement = $('<p class="big-message"><span class="big-message-icon icon-speech"></span><strong>' + data.more[0].logSoldierName + '</span></strong><br />' + data.more[0].logMessage + '<br /><span class="float-right">' + data.more[0].logDate + '</span>' + '</p>');
                    //Check for admin chat
                    if ($.inArray(data.more[0].logSoldierName, $.admin_online) > -1) {
                        chatElement.addClass('anthracite-gradient');
                    }
                    //Prepend the constructed chat message
                    $("#chatlogs").prepend(chatElement);
                }
            },
            error: function () {
                notify("ERROR", 'Connection error. Chat could not be updated.');
            }
        });
    }
}

function getServerData(serverID)
{
    if ($("#server_select").val() != 'none' && $.boardActiveFetch || serverID != null) {
        var serverid = $("#server_select").val();
        $.getJSON('api/battlefield/4/scoreboard/' + serverid, function (data) {
            if (data.status == 'error') {
                notify('ERROR', data.message,
                {
                    classes: ['red-gradient glossy']
                });

                return;
            }

            var serverinfo = data.more.serverinfo,
                teaminfo = data.more.teaminfo;
            $.admin_online = data.more.adminsonline;

            $("#team1 > tbody, #team2 > tbody").empty();

            if (!$.isEmptyObject(data.more.adminsonline)) {
                $("#admins_online").html(data.more.adminsonline.toString()).show();
            }
            else {
                $("#admins_online").hide();
            }

            $("#round_time").text('Rnd: ' + serverinfo.roundTime);
            $("#server_uptime").text('Uptime: ' + serverinfo.serverTime);
            $("#current_map").text(serverinfo.currentmap + ' (' + serverinfo.currentplayers + '/' + serverinfo.maxplayers + ')');
            $("#ticket_count_us").html(teaminfo[1].ticketcount);
            $("#ticket_count_cn").html(teaminfo[2].ticketcount);

            if (teaminfo[1].playerlist !== undefined && teaminfo[1].playerlist.length > 0) {
                var pos = 0;
                $.each(teaminfo[1].playerlist, function () {
                    pos = ++pos;
                    $("#team1 > tbody:last").append(
                        '<tr><td class="low-padding">' + pos + '</td>'
                        + '<td><a href="javascript://" onclick="openAdminModal(\'' + this.playername + '\', \'' + this.playerteam + '\', \'' + this.playersquadid + '\', \'' + this.playerid + '\')">'
                        + this.playername
                        + '</a></td><td class="hide-on-tablet">'
                        + this.playerscore
                        + '</td><td class="hide-on-tablet low-padding"><span class="tag">'
                        + this.playersquad.substring(0, 1)
                        + '</span></td><td class="low-padding">'
                        + this.playerkills
                        + '</td><td class="low-padding">'
                        + this.playerdeaths
                        + '</td><td class="low-padding">'
                        + this.playerping
                        +'</td></tr>'
                    );
                });
            }

            if (teaminfo[2].playerlist !== undefined && teaminfo[2].playerlist.length > 0) {
                pos = 0;
                $.each(teaminfo[2].playerlist, function () {
                    pos = ++pos;
                    $("#team2 > tbody:last").append(
                        '<tr><td class="low-padding">' + pos + '</td>'
                        + '<td><a href="javascript://" onclick="openAdminModal(\'' + this.playername + '\', \'' + this.playerteam + '\', \'' + this.playersquadid + '\', \'' + this.playerid + '\')">'
                        + this.playername
                        + '</a></td><td class="hide-on-tablet">'
                        + this.playerscore
                        + '</td><td class="hide-on-tablet low-padding"><span class="tag">'
                        + this.playersquad.substring(0, 1)
                        + '</span></td><td class="low-padding">'
                        + this.playerkills
                        + '</td><td class="low-padding">'
                        + this.playerdeaths
                        + '</td><td class="low-padding">'
                        + this.playerping
                        +'</td></tr>'
                    );
                });
            }

        });
    }
}

function openAdminModal(player, teamid, squadid, player_id) {
    var html = '<div class="button-group">'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminPunish(\'' + player + '\', \'' + teamid + '\')">Punish</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminForgive(\'' + player + '\', \'' + teamid + '\')">Forgive</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminKill(\'' + player + '\', \'' + teamid + '\')">Kill</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminKick(\'' + player + '\', \'' + teamid + '\')">Kick</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminTeamSwitch(\'' + player + '\', \'' + teamid + '\')">Team</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminSquad(\'' + player + '\', \'' + teamid + '\', \'' + squadid + '\')">Squad</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminSayPlayer(\'' + player + '\', \'' + teamid + '\')">Say</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminTbanPlayer(\'' + player + '\')">TBan</a>'
        + '<a href="javascript://" class="button anthracite-gradient glossy" onclick="$(this).closeModal(); adminPbanPlayer(\'' + player + '\')">PBan</a>'
        + '<a href="bf4/playerinfo/' + player_id + '" class="button anthracite-gradient glossy" target="_blank">Info</a>'
        + '</div>';
    $.modal(
    {
        content: html,
        title: 'Admin Actions for ' + player,
        actions: {
            'Close': {
                color: 'red',
                click: function (win) {
                    win.closeModal();
                },
            },
            'Center': {
                color: 'green',
                click: function (win) {
                    win.centerModal(true);
                }
            }
        },
        buttons: {
            'Close': {
                classes: 'red-gradient glossy',
                click: function (win) {
                    win.closeModal();
                }
            }
        },
        contentBg: false,
        buttonsLowPadding: true,
        closeOnBlur: true,
        maxWidth: false,
        maxHeight: false
    });
}

function adminKill(player, teamid) {
    $.modal.prompt('<p>Are you sure you want to kill <span class="red"><strong>' + player + '</strong></span>?</p><p>Enter kill reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>', function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a reason to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }
        else if ($("#prompt-value").val().length < 4) {
            $(this).getModalContentBlock().message("Your reason must be 4 characters or more.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminkill',
                player: player,
                id: $("#server_select").val(),
                message: $("#prompt-value").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient']
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminTeamSwitch(player, teamid) {
    $.modal.confirm('Are you sure you want to team switch <span class="red"><strong>' + player + '</strong></span>?'
        + '<p class="red-gradient message mid-margin-top">This will kill the player for the team switch</p>', function () {

        $.ajax($.adminUrl,
            {
                type: "POST",
                data: {
                    command: 'adminmove',
                    player: player,
                    id: $("#server_select").val()
                },
                success: function (data) {
                    if (data.status == 'error') {
                        notify('ERROR', data.message,
                            {
                                classes: ['red-gradient']
                            });
                    }
                    else if (data.status == 'success') {
                        notify(data.message,
                            {
                                classes: ['green-gradient']
                            });
                    }
                },
                error: function () {
                    notify('ERROR', 'Their was an error with processing your request. Please try again.',
                        {
                            classes: ['red-gradient']
                        });
                }
            });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        scrolling: false,
        contentBg: false,
        maxWidth: false,
        maxHeight: false
    });
}

function adminKick(player, teamid) {
    var html = '<p>Are you sure you want to kick <span class="red"><strong>' + player + '</strong></span>?</p>'
        + '<p>Enter kick reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a reason to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }
        else if ($("#prompt-value").val().length < 4) {
            $(this).getModalContentBlock().message("Your reason must be 4 characters or more.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminkick',
                message: $("#prompt-value").val(),
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminPunish(player, teamid) {
    var html = '<p>Are you sure you want to punish <span class="red"><strong>' + player + '</strong></span>?</p>'
        + '<p>Enter punish reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a reason to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }
        else if ($("#prompt-value").val().length < 4) {
            $(this).getModalContentBlock().message("Your reason must be 4 characters or more.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        var message = $("#prompt-value").val();

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminpunish',
                message: message,
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminForgive(player, teamid) {
    var html = '<p>Are you sure you want to forgive <span class="red"><strong>' + player + '</strong></span>?</p>'
        + '<div class="twelve-columns button-height block-label">'
        + '<label for="forgive_times" class="label">Number of time(s)</label>'
        + '<span class="number input margin-right">'
        + '<button type="button" class="button number-down">-</button>'
        + '<input type="text" value="1" size="3" class="input-unstyled" id="forgive_times" data-number-options=\'{"min":1,"max":20}\'>'
        + '<button type="button" class="button number-up">+</button>'
        + '</span><label for="prompt-value" class="label">Enter forgive reason</label></div>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a reason to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }
        else if ($("#prompt-value").val().length < 4) {
            $(this).getModalContentBlock().message("Your reason must be 4 characters or more.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        var message = $("#prompt-value").val();

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminforgive',
                message: message,
                forgive_ammount: $("#forgive_times").val(),
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });
}

function adminSquad(player, teamid, squadid) {
    var html = '<p>Please choose new squad for <span class="red"><strong>' + player + '</strong></span></p>'
        + '<p><strong>Current Squad:</strong> ' + squadList[squadid] + '</p>'
        + '<select class="select full-width" id="squad_list_select">'
        + '<option value="none">Select Squad</option>'
        + '</select>'
        + '<p class="message red-gradient mid-margin-top">This will kill the player for the squad switch.</p>';
    var newSquad = null;
    $.modal.prompt(html, function () {
        if (newSquad == null) {
            $(this).getModalContentBlock().message('You have not selected a squad. Please choose one or click cancel',
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        if (squadid == newSquad) {
            $(this).getModalContentBlock().message(player + ' is already in that squad. Please choose a different squad.',
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminsquad',
                old_sqid: squadid,
                new_sqid: newSquad,
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid, squadid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    $("#squad_list_select").change(function () {
        $("#prompt-value").val($(this).find("option:selected").text());
        newSquad = $(this).val();
    });

    $.each(squadList, function (key, value) {
        $("#squad_list_select").append('<option value="' + key + '">' + value + '</option>');
    });
}

function adminSayPlayer(player, teamid) {
    var html = '<p>Type your message to send to <span class="red"><strong>' + player + '</strong></span></p>'
        + '<p>Enter your message or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a message to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminsay',
                subcommand: "psay",
                message: $("#prompt-value").val(),
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminYellPlayer(player, teamid) {
    var html = '<p>Type your message to send to <span class="red"><strong>' + player + '</strong></span></p>'
        + '<p>Enter yell message or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a message to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminyell',
                subcommand: "pyell",
                message: $("#prompt-value").val(),
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player, teamid);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminTbanPlayer(player) {
    var html = '<p>Are you sure you wish to temp ban <span class="red"><strong>' + player + '</strong></span>?</p>'
        + '<p>Enter your ban reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>'
        + '<p class="mid-margin-top">How long should the temp ban be? (minutes)</p>'
        + '<span class="number input margin-right">'
        + '<button type="button" class="button number-down">-</button>'
        + '<input type="text" value="30" size="4" class="input-unstyled" data-number-options=\'{"min":1,"shiftIncrement":5}\' id="ban-duration">'
        + '<button type="button" class="button number-up">+</button>'
        + '</span>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a message to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        if ($("#prompt-value").val().length < 5) {
            $(this).getModalContentBlock().message("Your ban reason is to short. You need a minimum of 5 characters",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'tban',
                message: $("#prompt-value").val(),
                player: player,
                duration: $("#ban-duration").val(),
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient'],
                    });
            }
        });

    }, function () {
        openAdminModal(player);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminPbanPlayer(player) {
    var html = '<p>Are you sure you wish to permanently ban <span class="red"><strong>' + player + '</strong></span>?</p>'
        + '<p>Enter your ban reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a message to preform this action.",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        if ($("#prompt-value").val().length < 5) {
            $(this).getModalContentBlock().message("Your ban reason is to short. You need a minimum of 5 characters",
                {
                    append: false,
                    classes: ['red-gradient']
                });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'pban',
                message: $("#prompt-value").val(),
                player: player,
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient']
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient']
                    });
            }
        });

    }, function () {
        openAdminModal(player);
    },
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function adminYellServer() {
    var html = '<p>Select or type your message to be sent to the server</p>'
        + '<p>Enter your ban reason or select a predefined message.</p>'
        + '<select id="predefined_msg" class="select expandable-list" style="width:200px">'
        + '<option value="none">Select a Reason</option>'
        + '</select>'
        + '<p class="mid-margin-top">How long should the message stay up? (seconds)</p>'
        + '<span class="number input margin-right">'
        + '<button type="button" class="button number-down">-</button>'
        + '<input type="text" value="30" size="4" class="input-unstyled" data-number-options=\'{"min":1,"max":120,"shiftIncrement":5}\' id="timeout">'
        + '<button type="button" class="button number-up">+</button>'
        + '</span>';
    $.modal.prompt(html, function () {
        $(this).getModalContentBlock().clearMessages();
        if ($("#prompt-value").val() === '') {
            $(this).getModalContentBlock().message("You must enter a message to preform this action.",
            {
                append: false,
                classes: ['red-gradient']
            });

            return false;
        }

        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminyell',
                subcommand: 'yell',
                message: $("#prompt-value").val(),
                duration: $("#timeout").val(),
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient']
                        });
                }
                else if (data.status == 'success') {
                    notify(data.message,
                        {
                            classes: ['green-gradient']
                        });
                }
            },
            error: function () {
                notify('ERROR', 'Their was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient']
                    });
            }
        });

    }, function() {},
    {
        contentBg: false,
        maxWidth: false
    });

    getPremessages($("#server_select").val());

    $("#predefined_msg").change(function () {
        if ($(this).val() == 'none') {
            $("#prompt-value").val('');
        }
        else {
            $("#prompt-value").val($(this).val());
        }
    });
}

function getPremessages(serverid) {
    $.ajax('api/common/premessage',
    {
        type: 'GET',
        data: { id: serverid },
        success: function (data) {
            if (data.status == 'error') {
                notify('ERROR', data.message,
                    {
                        classes: ['red-gradient']
                    });
            }
            else if (data.status == 'success') {
                $.each(data.more, function (k, v) {
                    $("#predefined_msg").append('<option value="' + v + '">' + v + '</option>');
                });
            }
        }
    });
}

$("#chat_type").keypress(function (e) {
    if (e.which == 13) {
        if ($("#chat_type").val() === '') {
            notify("ERROR", 'You cannot send a blank message to the server. Please type your message',
                {
                    classes: ['red-gradient']
                });

            return false;
            e.preventDefault();
        }
        $.ajax($.adminUrl,
        {
            type: "POST",
            data: {
                command: 'adminsay',
                subcommand: "say",
                message: $("#chat_type").val(),
                id: $("#server_select").val()
            },
            success: function (data) {
                if (data.status == 'error') {
                    notify('ERROR', data.message,
                        {
                            classes: ['red-gradient'],
                        });
                }
                else if (data.status == 'success') {
                    notify('You sent "' + data.message + '" to the server.',
                        {
                            classes: ['green-gradient']
                        });

                    $("#chat_type").val('');
                }
            },
            error: function () {
                notify('ERROR', 'There was an error with processing your request. Please try again.',
                    {
                        classes: ['red-gradient']
                    });
            }
        });
    }
});

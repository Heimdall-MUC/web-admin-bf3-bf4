//Go to local page
function goToLocalURL(urlString){
    window.location.href = urlString;
}
function fetchSearchResults(player_name) {
    $.searchResults = new $.AdKat_Player_Collection();
    $.searchResults.fetch(player_name);
}

function displaySearchResults(searchResults) {
    $("#player_search_results").empty();
    var searchResultsView = new $.AdKat_Player_Table({collection: searchResults});
    $("#player_search_results").append(searchResultsView.render().el);

    var table = $("#player_search_results_table"), tableStyled = false;
    table.dataTable({
        "bSort": false,
        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
        'fnDrawCallback': function (oSettings) {
            // Only run once
            if (!tableStyled) {
                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                tableStyled = true;
            }
        }
    });
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

$(document).ready(function () {
    //Define all constructs
    $.AdKat_Player = Backbone.Model.extend({
        defaults: {
            player_id: null, //Player's ID assigned by stat logging plugin
            player_name: null, //Soldier name for this player
            player_guid: null, //EA GUID for this player. Might be null.
            player_game: null
        }
    });
    $.AdKat_Player_Collection = Backbone.Collection.extend({
        url: $("#thisurl").val(),
        model: $.AdKat_Player,
        fetch: function (player_name) {
            var collectionRef = this;
            $.ajax(this.url, {
                type: 'POST',
                dataType: 'json',
                data: { player: $("#player_search_input").val(), game: $("#thisgame").val() },
                beforeSend: function () {
                    $.blockUI({
                        message: '<h3 class="align-center white"><span class="loader big waiting on-dark with-small-padding"></span>Loading Search Results</h3>',
                        css: { backgroundColor: 'black' }
                    });
                },
                success: function (data) {
                    //Check for error
                    if (data.status == 'Not Found' || data.status == 'error') {
                        $("#error_msg").message("Search Error. " + data.message, {
                            classes: ['red-gradient glossy'],
                            closable: true,
                            showCloseOnHover: true,
                            groupSimilar: true
                        });
                    }
                    //Check for data
                    else if (data.more != undefined) {
                        $.each(data.more, function (index, element) {
                            collectionRef.models.push(new $.AdKat_Player(element));
                        });
                        displaySearchResults(collectionRef);
                    }
                    //Exception Case
                    else {
                        $("#error_msg").message('Unknown error. Please refresh the page to try again.', {
                            classes: ['red-gradient glossy'],
                            closable: true,
                            showCloseOnHover: true,
                            groupSimilar: true
                        });
                    }
                    $.unblockUI();
                },
                error: function () {
                    $("#error_msg").message('Data could not be loaded at this time. Please refresh the page to try again.', {
                        classes: ['red-gradient glossy'],
                        closable: true,
                        showCloseOnHover: true,
                        groupSimilar: true
                    });
                    $.unblockUI();
                }
            });
        }
    });
    $.AdKat_Player_LineItem = Backbone.View.extend({
        initialize: function () {
            var context = this;
            this.el = $("<tr></tr>");
        },
        render: function (updating) {
            //Get context
            var currentModel = this.model;
            //empty the element
            this.el.empty();

            //render the player id
            var player_id = this.model.get("PlayerID");
            if (player_id == null) {
                player_id = "No ID";
            }
            var player_id_element = $('<td>' + player_id + '</td>');
            $(this.el).append(player_id_element);

            //render the player name
            var player_name = this.model.get("SoldierName");
            if (player_name == null) {
                player_name = "No Name";
            }
            var player_name_element = $('<td>' + player_name + '</td>');
            $(this.el).append(player_name_element);

            //render the player guid
            var player_guid = this.model.get("EAGUID");
            if (player_guid == null) {
                player_guid = "No GUID";
            }
            var player_guid_element = $('<td>' + player_guid + '</td>');
            $(this.el).append(player_guid_element);

            //render the player status
            var player_game = this.model.get("Game");
            if (player_game == null) {
                player_game = "Unknown";
            }
            var player_game_element = $('<td>' + player_game + '</td>');
            $(this.el).append(player_game_element);

            //render the button for opening player
            var full_info_element = $('<td><button class="button compact blue-gradient glossy">Full Info</button></td>');
            $("button", full_info_element).click(function () {
                goToLocalURL(player_game.toLowerCase() + '/playerinfo/' + player_id);
            });
            $(this.el).append(full_info_element);

            //Return the element for initial render, subsequent renders will not use the return
            return this;
        }
    });
    $.AdKat_Player_Table = Backbone.View.extend({
        template: _.template($("#player_search_results_template").html()),
        options: {
            players: []
        },
        initialize: function () {
            var thisView = this;
            thisView.options.players = [];
            this.collection.each(function (playerModel) {
                thisView.options.players.push(new $.AdKat_Player_LineItem({model: playerModel}));
            });
        },
        render: function () {
            this.el = $(this.template({}));
            var element = $("tbody", this.el);

            $.each(this.options.players, function (index, playerLineItem) {
                element.append(playerLineItem.render().el);
            });

            return this;
        }
    });
    //Define Actions
    $("#player_search_button").click(function () {
        var query = $("#player_search_input").val();
        if (query.length < 4) {
            $("#error_msg").message("Search too short!", {
                classes: ['red-gradient glossy'],
                closable: true,
                showCloseOnHover: true,
                groupSimilar: true
            });
        }
        else {
            fetchSearchResults(query);
        }
    });
    $('#player_search_input').on('keypress', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            $("#player_search_button").trigger("click");
        }
    });
});

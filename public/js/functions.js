$.ajaxSetup({ cache: false });
function populationFeed(game)
{
    var classColor;

    if(game == 'bf3')
    {
        var bf3pop = $("#server_population").find("#feed");
        bf3pop.prev('h3').append('<span class="loader refreshing on-dark float-right" id="bf3pop_status"></span>');
        $.getJSON('api/battlefield/3/population', function(data, textStatus)
        {
            if(data.status == 'error')
            {
                bf3pop.clearMessages(false);
                $("#pop_status").remove();
                bf3pop.message(data.message,
                {
                    classes: ['red-gradient glossy'],
                    animate: false
                });

                return false;
            }

            // Clear the previous results for fresh data
            bf3pop.clearMessages(false);

            $.each(data.more.population, function(index, val)
            {
                classColor = (val.cur <= val.max*0.30) ? 'red' : (val.cur <= val.max*0.80) ? 'blue' : (val.cur == 0 && val.max == 0) ? 'black' : 'green';

                var msg = val.name + ' <span class="float-right">' + val.cur + '/' + val.max + '</span>';

                bf3pop.message(msg,
                {
                    node: 'a',
                    link: 'bf3/scoreboard#' + val.id,
                    classes: [classColor + '-gradient'],
                    closable: false,
                    showCloseOnHover: false,
                    position: 'bottom',
                    groupSimilar: false,
                    animate: false
                });
            });

            msg = data.more.poptotal.totalUsed + ' out of ' + data.more.poptotal.totalMax + ' slots used';

            bf3pop.message(msg,
            {
                classes: ['anthracite-gradient'],
                closable: false,
                showCloseOnHover: false,
                position: 'bottom',
                groupSimilar: false,
                animate: false
            });

            $('#server_population').find('#progress').setProgressValue(data.more.poptotal.percent + '%', true);

            $("#bf3pop_status").remove();
        })
        .fail(function()
        {
            bf3pop.clearMessages(false);
            bf3pop.message("Could not load the population feed. Retrying in 10 seconds.",
            {
                classes: ['red-gradient glossy'],
                closable: false,
                showCloseOnHover: false,
                position: 'bottom',
                groupSimilar: false,
                animate: false
            });
            $("#bf3pop_status").remove();
        });
    }
    else if(game == 'bf4')
    {
        var bf4pop = $("#server_population2").find("#feed");
        bf4pop.prev('h3').append('<span class="loader refreshing on-dark float-right" id="bf4pop_status"></span>');
        $.getJSON('api/battlefield/4/population', function(data, textStatus)
        {
            if(data.status == 'error')
            {
                bf4pop.clearMessages(false);
                $("#pop_status").remove();
                bf4pop.message(data.message,
                {
                    classes: ['red-gradient glossy'],
                    animate: false
                });

                return false;
            }

            // Clear the previous results for fresh data
            bf4pop.clearMessages(false);

            $.each(data.more.population, function(index, val)
            {
                classColor = (val.cur <= val.max*0.30) ? 'red' : (val.cur <= val.max*0.80) ? 'blue' : (val.cur == 0 && val.max == 0) ? 'black' : 'green';

                var msg = val.name + ' <span class="float-right">' + val.cur + '/' + val.max + '</span>';

                bf4pop.message(msg,
                {
                    node: 'a',
                    link: 'bf4/scoreboard#' + val.id,
                    classes: [classColor + '-gradient'],
                    closable: false,
                    showCloseOnHover: false,
                    position: 'bottom',
                    groupSimilar: false,
                    animate: false
                });
            });

            msg = data.more.poptotal.totalUsed + ' out of ' + data.more.poptotal.totalMax + ' slots used';

            bf4pop.message(msg,
            {
                classes: ['anthracite-gradient'],
                closable: false,
                showCloseOnHover: false,
                position: 'bottom',
                groupSimilar: false,
                animate: false
            });

            $('#server_population2').find('#progress').setProgressValue(data.more.poptotal.percent + '%', true);

            $("#bf4pop_status").remove();
        })
        .fail(function()
        {
            bf4pop.clearMessages(false);
            bf4pop.message("Could not load the population feed. Retrying in 10 seconds.",
            {
                classes: ['red-gradient glossy'],
                closable: false,
                showCloseOnHover: false,
                position: 'bottom',
                groupSimilar: false,
                animate: false
            });
            $("#bf4pop_status").remove();
        });
    }
}

function dateFormat(c)
{
    var d = new Date(c);
    var formated = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
    return formated;
}

function pullAdminReports()
{
    //Check local storage for report entries
    $.latestViewedReportID = localStorage.getItem('lastReportId');
    if($.latestViewedReportID != null)
    {
        $.latestViewedReportID = parseInt($.latestViewedReportID);
    }

    var record_id,
        target_name,
        source_name,
        record_message,
        record_time,
        ServerName;
    $.ajax('api/battlefield/reports',
    {
        type: 'POST',
        dataType: 'json',
        data: { lastRecordId: $.latestFetchedReportID},
        success: function(data)
        {
            if(data.status == 'error')
                return false;
            //Grab record ID of latest report
            $.latestFetchedReportID = data.more[0].record_id;

            while($("#report_container li").length > 30)
            {
                $("#report_container li").last().remove();
            }

            $.each(data.more.reverse(), function(index, val)
            {
                record_id      = val.record_id,
                target_name    = val.target_name,
                source_name    = val.source_name,
                record_message = val.message,
                record_time    = val.date,
                ServerName     = val.server;

                var html = '<li class="white-gradient" data-record="' + record_id + '"><ul class="message-info">' +
                           '<li class="blue" data-livestamp="' + record_time + '"></li></ul>' +
                           '<strong class="blue">' + source_name + '</strong><br />' +
                           '<strong><span class="red">' + target_name + '</span>: ' + record_message + '</strong><br /><span class="tag small">' + ServerName + '</span>';

                $("#report_container").prepend(html);

                if(record_id > $.latestViewedReportID)
                {
                    $("#report_sound_notify").get(0).play();
                    var html2 = '<p><span class="blue">' + source_name + '</span> reported <b class="red">' + target_name + '</b> for <span class="green">' + record_message + '</span></p><p class="tag small float-right">' + ServerName + '</p>';
                    notify('<span data-livestamp="' + record_time + '"></span>', html2,
                    {
                        showCloseOnHover: false,
                        hPos: 'left',
                        closeDelay: 300000,
                        classes: ['black-gradient glossy'],
                        rotate: true,
                        onClose:function(){
                            localStorage.setItem('lastReportId', record_id);
                        },
                        onClick:function(){
                            localStorage.setItem('lastReportId', record_id);
                        }
                    });
                }
            });
        },
        error: function(data)
        {
            notify('ERROR', 'Could not load recent admin reports');
        }
    });
}

# Web Admin Changelog

## 1.4.2
- Fixed install script with running migrations and creating the session table
- Updated vendors to the latest commit
- Moved some code around to make the application less error prone

## 1.4.1
- Fixed major security issue
- Fixed PHP error with missing class Config in battlefield admin scripts
- Enhanced recent bans list on dashboard to show how long ago the ban was issued
- Added function to check if new versions of webadmin are available
- Added enforce options to modify bans section. Issuing bans are defaulted to EA GUID
- Added comments in .htaccess files in the root and public directory about how to fix 500 errors if on 1and1 hosting.
  Could be used for other hosts as well if same conditions apply
- Setup controller will now delete itself if the web admin is already installed and apply any new database table updates
- Added admin reports with sound. Still need to add the disable sound notifications in site settings.
- Added checker for site administrators or users with the All Permissions permission to tell you if there is a new update available

## 1.4.0.6
- Fixed logic for issuing forgives on a player from the player info page
- Added config setting to change the max temp ban duration from the live scoreboard

## 1.4.0.5
- Issue forgives for a player without being in the server
- Fixed ban redirect issue
- Added ban issuing on player info
- Bug fixes
- Changed the way banlist renders. It will now load the bans via ajax call. Only shows Active and Disabled.
- Updated vendors to their latest build (Framework)
 (Note: If you have a lot of banned players this may take some time to process)

## 1.4.0.4-hotfix
- Fixed PHP error in helper script

## 1.4.0.4
- Improved loading time when using a remote connection to a external MySQL database. (I still highly recommend you run it on the same server as your database as it avoids the network lag)
- Fixed commands for BF4 that were not routed correctly and causing script errors
- Fixed automated installer in various ways
- Disabled database caching and reverted back to file caching.
- Fixed the pre-messages list
- Fixed ban duration calculation if the bans exceeds the 35 day mark to correctly reflect the ban duration.
- Cleaned up routes file to remove unused ones.

## 1.4.0.3
- Fixed installer issue with generating the application key
- Fixed BF4 Admin Controller

## 1.4.0.1
- Added missing backslash in BF4 admin script

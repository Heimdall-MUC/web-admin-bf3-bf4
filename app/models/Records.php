<?php
/**
 * Class to create/modify/add records for BF3/BF4
 */

class Records extends Eloquent
{
    protected $table = 'adkats_records_main';
    protected $primaryKey = 'record_id';
    public $timestamps = false;
}

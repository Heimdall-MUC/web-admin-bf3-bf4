<?php

class Servers extends Eloquent
{
    protected $table = 'tbl_server';
    protected $primaryKey = 'ServerID';
    public $timestamps = false;
}

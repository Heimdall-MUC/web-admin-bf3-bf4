<?php namespace App\Models\Battlefield;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Helpers\Helper;
class Bf3Scoreboard
{
    /**
     * Current Game ID
     * @var integer
     */
    public $GameID = -1;

    /**
     * List of pre defined messages.
     *
     * Messages are pulled from the AdKat Settings table
     * @var array
     */
    public $definedMessages = array();

    /**
     * References the BF class
     * @var object
     */
    public $bfconn;

    /**
     * Current ID of the selected server
     * @var integer
     */
    public $ServerID = -1;

    /**
     * Finished results are stored
     * @var array
     */
    public $data = array();

    public function initialize($id = NULL)
    {
        try
        {
            // Make sure ID is not empty
            if(is_null($id) || empty($id))
                throw new \BattlefieldException('Server ID was not provided');

            // Make sure we have a valid id
            if(!is_numeric($id) || $id < 1)
                throw new \BattlefieldException('Invalid Server ID');

            // Attempt to get game settings from database or fail
            $settings = \Gamesetting::findOrFail($id);

            $this->GameID = DB::table('tbl_games')->where('Name', 'BF3')->pluck('GameID');

            $info = DB::table('tbl_server')->where('ServerID', $id)->where('GameID', $this->GameID)->where('ConnectionState', 'on')->first();

            if(!$info)
                throw new \BattlefieldException('Could not locate the specified server');

            // Parse out the Hostname/IP without port number
            $serverIP       = Helper::getIPv4Addr($info->IP_Address);
            // Get the port number from the Hostname/IP
            $serverRconPort = Helper::getPortNum($info->IP_Address);
            // Get the hashed RCON password from the database and decrypt it
            $serverRconPass = Crypt::decrypt($settings->getRconHash());

            // Did we get a valid IP address?
            if(!Validator::make(array('ip' => $serverIP), array('ip' => 'IP'))->passes())
                throw new \BattlefieldException("Invalid IP Address > " . $serverIP);

            // Call and assign the class to interact with the game server
            $this->bfconn = new \BF3Conn(array($serverIP, $serverRconPort, null));

            if(!$this->bfconn->isConnected())
                throw new \BattlefieldException('Could not establish connection to server');

            // Attempt to login to the game server
            $this->bfconn->loginSecure($serverRconPass);

            // Is the RCON password correct?
            if(!$this->bfconn->isLoggedIn())
                throw new \BattlefieldException("Invalid RCON Password");

            // If we got past this point then we are now ready to do some stuff

            // Set the server id to be used globally
            $this->ServerID = (int) $id;

            // Fetch list of predefined messages
            $rawDefinedMessage = DB::table('adkats_settings')->where('server_id', '=', $this->ServerID)->where('setting_name', '=', 'Pre-Message List')->pluck('setting_value');
            $this->definedMessages = explode('|', urldecode(rawurldecode($rawDefinedMessage)));

            $this->fetchGameData();

            // Send back the information to the client
            return Response::json(Helper::doMessage('success', NULL, $this->data));
        }
        catch(\BattlefieldException $e)
        {
            return Response::json(Helper::doMessage('error', $e->getMessage()));
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(Helper::doMessage('error', 'No settings found for this server'));
        }
    }

    /**
     * Get the data from the game server and append it to the global $data var
     * @return void
     */
    private function fetchGameData()
    {
        $serverinfo = $this->bfconn->getServerInfo();
        $playerlist = $this->bfconn->adminGetPlayerlist();

        $serverTime = $serverinfo[16];
        $roundTime = $serverinfo[17];

        $this->data['serverinfo'] = array(
            'servername'        => $this->bfconn->getServerName(),
            'currentplayers'    => $this->bfconn->getCurrentPlayers(),
            'maxplayers'        => $this->bfconn->getMaxPlayers(),
            'roundTime'         => Helper::seconds2human($roundTime, true),
            'serverTime'        => Helper::seconds2human($serverTime, false),
            'currentmap'        => last($this->bfconn->getCurrentMapName()),
            'gamemode'          => last($this->bfconn->getCurrentPlaymodeName())
        );

        $this->data['teaminfo'][1]['ticketcount'] = (round($serverinfo[9]) >= 10000) ? "&infin;" : round($serverinfo[9]);
        $this->data['teaminfo'][2]['ticketcount'] = (round($serverinfo[10]) >= 10000) ? "&infin;" : round($serverinfo[10]);

        if($this->bfconn->getCurrentPlayers() > 0)
        {
            for($i=0; $i < $this->bfconn->getCurrentPlayers(); $i++)
            {
                $playerScore[]    = (int)$playerlist[($playerlist[1]) * $i + $playerlist[1] + 9];
                $playerName[]     = $playerlist[($playerlist[1]) * $i + $playerlist[1] + 3];
                $playerKill[]     = (int)$playerlist[($playerlist[1]) * $i + $playerlist[1] + 7];
                $playerDeath[]    = (int)$playerlist[($playerlist[1]) * $i + $playerlist[1] + 8];
                $playerTeamId[]   = (int)$playerlist[($playerlist[1]) * $i + $playerlist[1] + 5];
                $playerSquadId[]  = (int)$playerlist[($playerlist[1]) * $i + $playerlist[1] + 6];
                $playerGUID[]     = $playerlist[($playerlist[1]) * $i + $playerlist[1] + 4];
            }

            for($i=0; $i < count(array_filter($playerTeamId)); $i++)
            {
                $playerid = \Player::where('GameID', $this->GameID)->where('EAGUID', $playerGUID[$i])->pluck('PlayerID');

                // If player is not in the database then lets add them
                if(!$playerid)
                {
                    $player = new \Player;
                    $player->GameID         = $this->GameID;
                    $player->EAGUID         = $playerGUID[$i];
                    $player->SoldierName    = $playerName[$i];
                    $player->save();

                    $playerid = $player->PlayerID;
                }

                $this->data['teaminfo'][$playerTeamId[$i]]['playerlist'][] = array(
                    'playerid'      => (int) $playerid,
                    'playername'    => $playerName[$i],
                    'playerscore'   => $playerScore[$i],
                    'playerteam'    => $playerTeamId[$i],
                    'playersquad'   => Helper::squadName($playerSquadId[$i]),
                    'playersquadid' => $playerSquadId[$i],
                    'playerkills'   => $playerKill[$i],
                    'playerdeaths'  => $playerDeath[$i]
                );
            }

            /**
             * Sorts the playerlist for each team in descending order by player score
             * If you wish to change the order to ascending change "SORT_DESC" to "SORT_ASC"
             */

            for($i=0; $i < count($this->data['teaminfo']); $i++)
            {
                $sorting = array();

                if(isset($this->data['teaminfo'][$i]['playerlist']) && !empty($this->data['teaminfo'][$i]['playerlist']))
                {
                    foreach ($this->data['teaminfo'][$i]['playerlist'] as $key => $row)
                    {
                        $sorting[$key]  = $row['playerscore'];
                    }

                    array_multisort($sorting, SORT_DESC, $this->data['teaminfo'][$i]['playerlist']);
                }
                else
                {
                    $this->data['teaminfo'][$i]['playerlist'] = array();
                }
            }
        }
    }
}

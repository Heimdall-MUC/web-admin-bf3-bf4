<?php namespace App\Models\Battlefield;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Config;
use Helpers\Helper;
class Bf4Admin
{
    /**
     * Current Game ID
     * @var integer
     */
    public $GameID = -1;

    /**
     * List of pre defined messages.
     *
     * Messages are pulled from the AdKat Settings table
     * @var array
     */
    public $definedMessages = array();

    /**
     * References the BF class
     * @var object
     */
    public $bfconn;

    /**
     * Current user IGN name, if no name provided then it is set to false
     * @var boolean/string
     */
    public $adminName = FALSE;

    /**
     * References the current user object
     * @var object
     */
    public $user;

    /**
     * Current ID of the selected server
     * @var integer
     */
    public $ServerID = -1;

    /**
     * Current player EA GUID
     * @var string
     */
    public $playerEAGUID = NULL;

    /**
     * Current player id
     * @var integer
     */
    public $playerID = NULL;

    /**
     * Current admin player id
     * @var integer
     */
    public $adminID = NULL;

    public function initialize($id = NULL)
    {
        try
        {
            // Make sure ID is not empty
            if(is_null($id) || empty($id))
                throw new \BattlefieldException('Server ID was not provided');

            // Make sure we have a valid id
            if(!is_numeric($id) || $id < 1)
                throw new \BattlefieldException('Invalid Server ID');

            // Attempt to get game settings from database or fail
            $settings = \Gamesetting::findOrFail($id);

            $this->GameID = DB::table('tbl_games')->where('Name', 'BF4')->pluck('GameID');

            $info = DB::table('tbl_server')->where('ServerID', $id)->where('GameID', $this->GameID)->where('ConnectionState', 'on')->first();

            if(!$info)
                throw new \BattlefieldException('Could not locate the specified server');

            // Parse out the Hostname/IP without port number
            $serverIP       = Helper::getIPv4Addr($info->IP_Address);
            // Get the port number from the Hostname/IP
            $serverRconPort = Helper::getPortNum($info->IP_Address);
            // Get the hashed RCON password from the database and decrypt it
            $serverRconPass = Crypt::decrypt($settings->getRconHash());

            // Did we get a valid IP address?
            if(!Validator::make(array('ip' => $serverIP), array('ip' => 'IP'))->passes())
                throw new \BattlefieldException("Invalid IP Address > " . $serverIP);

            // Call and assign the class to interact with the game server
            $this->bfconn = new \BF4Conn(array($serverIP, $serverRconPort, null));

            if(!$this->bfconn->isConnected())
                throw new \BattlefieldException('Could not establish connection to server');

            // Attempt to login to the game server
            $this->bfconn->loginSecure($serverRconPass);

            // Is the RCON password correct?
            if(!$this->bfconn->isLoggedIn())
                throw new \BattlefieldException("Invalid RCON Password");

            // If we got past this point then we are now ready to do some stuff

            // Set the server id to be used globally
            $this->ServerID = (int) $id;

            // Fetch list of predefined messages
            $rawDefinedMessage = DB::table('adkats_settings')->where('server_id', '=', $this->ServerID)->where('setting_name', '=', 'Pre-Message List')->pluck('setting_value');
            $this->definedMessages = explode('|', urldecode(rawurldecode($rawDefinedMessage)));

            // Get the current user
            $this->user = \Auth::user();

            // Set the current admin name
            $this->setAdmin();

            if($this->admin == FALSE || empty($this->admin))
                throw new \BattlefieldException('We could not verify that you are a game admin');

            $command    = Input::get('command');
            $subCommand = Input::get('subcommand', NULL);
            $message    = Input::get('message', '');
            $player     = Input::get('player', '');

            if($this->bfconn->getCurrentPlayers() > 0 && $player != '')
            {
                // Get the selected player EAGUID
                $this->playerEAGUID = $this->bfconn->adminGetPlayerGUID($player);

                if($this->playerEAGUID == 'PlayerNotFoundError')
                {
                    return Response::json(Helper::doMessage('error', 'Could not locate this player on this server. Please try again.'));
                }

                // Get the selected player DB ID
                $this->playerID = \Player::where('EAGUID', '=', $this->playerEAGUID)->where('GameID', '=', $this->GameID)->pluck('PlayerID');
            }

            $response = $this->routeAction($command, $subCommand, $message, $player);

            // Send back the information to the client
            return Response::json($response);
        }
        catch(\BattlefieldException $e)
        {
            return Response::json(Helper::doMessage('error', $e->getMessage()));
        }
        catch(ModelNotFoundException $e)
        {
            return Response::json(Helper::doMessage('error', 'No settings found for this server'));
        }
    }

    private function routeAction($command, $subcommand = NULL, $message = '', $player = '')
    {
        switch($command)
        {
            case "adminsay":
                if($this->user->can("bf4_say") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    switch($subcommand)
                    {
                        case 'psay':
                            if($this->user->can('bf4_psay') || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                            {
                                return $this->adminSayPlayer($message, $player);
                            }
                            else
                            {
                                return Helper::doMessage('error', 'You do not have the required permission to preform this action');
                            }
                        break;

                        case 'tsay':
                            if($this->user->can('bf4_tsay') || $this->user->can('allperms') || $this->user->can('fullbfaccess') || $this->user->can('fullbfaccess'))
                            {
                                return $this->adminSayTeam($message, $player, Input::get('team'));
                            }
                            else
                            {
                                return Helper::doMessage('error', 'You do not have the required permission to preform this action');
                            }
                        break;

                        case 'say':
                            return $this->adminSayAll($message);
                        break;

                        default:
                            return Helper::doMessage('error', 'No routable command found.');
                        break;
                    }
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminyell":
                if($this->user->can("bf4_yell") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    switch($subcommand)
                    {
                        case 'pyell':
                            if(is_numeric(Input::get('duration')) && ($this->user->can('bf4_pyell') || $this->user->can('allperms') || $this->user->can('fullbfaccess')))
                            {
                                return $this->adminYellPlayer($message, $player, (int) Input::get('duration'));
                            }
                            else
                            {
                                return $this->adminYellPlayer($message, $player);
                            }
                        break;

                        case 'tyell':
                            if(is_numeric(Input::get('duration')) && ($this->user->can('bf4_tyell') || $this->user->can('allperms') || $this->user->can('fullbfaccess')))
                            {
                                return $this->adminYellTeam($message, $player, (int) Input::get('team'), (int) Input::get('duration'));
                            }
                            else
                            {
                                return $this->adminYellTeam($message, (int) Input::get('team'));
                            }
                        break;

                        case 'yell':
                            if(is_numeric(Input::get('duration')))
                            {
                                return $this->adminYell($message, (int) Input::get('duration'));
                            }
                            else
                            {
                                return $this->adminYell($message);
                            }
                        break;

                        default:
                            return Helper::doMessage('error', 'No routeable command found.');
                        break;
                    }
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminkill":
                if($this->user->can("bf4_kill") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    if(!empty($message))
                    {
                        return $this->adminKillPlayer($player, $message);
                    }
                    else
                    {
                        return $this->adminKillPlayer($player);
                    }
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminkick":
                if($this->user->can("bf4_kick") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    if(!empty($message))
                    {
                        return $this->adminKickPlayer($player, $message);
                    }
                    else
                    {
                        return $this->adminKickPlayer($player);
                    }
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminpunish":
                if($this->user->can("bf4_punish") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    return $this->adminPunishPlayer($player, $message);
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminforgive":
                if($this->user->can("bf4_forgive") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    return $this->adminForgivePlayer($player, $message, Input::get('forgive_ammount'));
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminmove":
                if($this->user->can("bf4_team") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    return $this->adminMovePlayer($player, Input::get('force'));
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "adminsquad":
                if($this->user->can("bf4_squad") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    return $this->adminSquad($player, Input::get('old_sqid'), Input::get('new_sqid'), Input::get('force'));
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "tban":
                if($this->user->can("bf4_tban") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    $duration = (is_numeric(Input::get('duration')) && Input::get('duration') != '') ? Input::get('duration') : NULL;
                    return $this->adminTempBan($player, $message, $duration);
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "pban":
                if($this->user->can("bf4_ban") || $this->user->can('allperms') || $this->user->can('fullbfaccess'))
                {
                    return $this->adminPermBan($player, $message);
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "nuke":
                if($this->user->can("bf4_nuke") || $this->user->can('allperms'))
                {
                    return $this->adminNuke();
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            case "kickall":
                if($this->user->can("bf4_kickall") || $this->user->can('allperms'))
                {
                    return $this->adminKickAll();
                }
                else
                {
                    return Helper::doMessage('error', 'You do not have the required permission to preform this action.');
                }
            break;

            default:
                return Helper::doMessage('error', 'No routable command found.');
            break;
        }
    }

    /**
     * Send global server message
     * @param  string $message
     * @return array
     */
    private function adminSayAll($message)
    {
        // Check if the user typed a command instead of an actual message
        if( (!is_bool(strpos($message, "@")) && strpos($message, "@") == 0) || (!is_bool(strpos($message, "!")) && strpos($message, "!") == 0) )
        {
            // Typed in a command with @ or !
            return Helper::doMessage('error', 'Commands through chat are not supported.');
        }
        else if( (!is_bool(strpos($message, "/@")) && strpos($message, "/@") == 0) || (!is_bool(strpos($message, "/!")) && strpos($message, "/!") == 0) )
        {
            // Typed in a command with /@ or /!
            return Helper::doMessage('error', 'Commands through chat are not supported.');
        }
        else if( !is_bool(strpos($message, "/")) && strpos($message, "/") == 0 )
        {
            // Typed in a command with /
            return Helper::doMessage('error', 'Commands through chat are not supported.');
        }

        if(in_array($message, $this->definedMessages))
        {
            $this->bfconn->adminSayMessageToAll($message);
        }
        else
        {
            $this->bfconn->adminSayMessageToAll($this->admin . ': ' . $message);
        }

        $this->addRecord('admin_say', $this->admin, $message);
        Helper::createLogEntry('success', 'SendMessage', $message, 'BF4');

        return Helper::doMessage('success', $message);
    }

    /**
     * Send server message to specified player
     * @param  string $message
     * @param  string $player
     * @return array
     */
    private function adminSayPlayer($message, $player)
    {
        if(in_array($message, $this->definedMessages))
        {
            $this->bfconn->adminSayMessageToPlayer($player, $message);
        }
        else
        {
            $this->bfconn->adminSayMessageToPlayer($player, $this->admin . ': ' . $message);
        }

        $this->addRecord('player_say', $player, $message);
        Helper::createLogEntry('success', 'SendPlayerMessage', $message, 'BF4');

        return Helper::doMessage('success', $message);
    }

    /**
     * Send server message to specified team
     * @param  string $message
     * @param  integer $team
     * @return array
     */
    private function adminSayTeam($message, $team)
    {
        if(in_array($message, $this->definedMessages))
        {
            $this->bfconn->adminSayMessageToTeam($team, $message);
        }
        else
        {
            $this->bfconn->adminSayMessageToTeam($team, $this->admin . ': ' . $message);
        }

        $this->addRecord('admin_say', $this->admin, $message);
        Helper::createLogEntry('success', 'SendTeamMessage', $message, 'BF4');

        return Helper::doMessage('success', $message);
    }

    /**
     * Send server wide yell message
     * @param  string  $message
     * @param  integer $duration Sets how long the message is displayed for in seconds
     * @return array
     */
    private function adminYell($message, $duration = 30)
    {
        $this->bfconn->adminYellMessage($message, '{%all%}', $duration);
        $this->addRecord('admin_yell', 'Server', $message);
        Helper::createLogEntry('success', 'SendYellMessage', $message . ' for ' . $duration . ' duration', 'BF4');
        return Helper::doMessage('success', $message);
    }

    /**
     * Send yell message to specified team
     * @param  string  $message
     * @param  integer  $team
     * @param  integer $duration Sets how long the message is displayed for in seconds
     * @return array
     */
    private function adminYellTeam($message, $team, $duration = 30)
    {
        $this->bfconn->adminYellMessageToTeam($message, $team, $duration);
        $this->addRecord('admin_yell', 'Server', $message);
        Helper::createLogEntry('success', 'SendTeamYellMessage', $message . ' for ' . $duration . ' seconds', 'BF4');
        return Helper::doMessage('success', $message);
    }

    /**
     * Send yell message to specified player
     * @param  string  $message
     * @param  string  $player
     * @param  integer $duration Sets how long the message is displayed for in seconds
     * @return array
     */
    private function adminYellPlayer($message, $player, $duration = 30)
    {
        $this->bfconn->adminYellMessage($message, $player, $duration);
        $this->addRecord('player_yell', $player, $message);
        Helper::createLogEntry('success', 'SendPlayerYellMessage', $message . ' for ' . $duration . ' seconds', 'BF4');
        return Helper::doMessage('success', $message);
    }

    /**
     * Move specified player to the other team
     * @param  string  $player
     * @param  boolean $force  Force move the player to the other team
     * @return array
     */
    private function adminMovePlayer($player)
    {
        switch($this->bfconn->adminMovePlayerSwitchTeam($player, true))
        {
            case "CommandIsReadOnly":
                Helper::createLogEntry('error', 'TeamSwitch', 'Attempted to switch ' . $player . ' to the other team on an offical server', 'BF4');
                return Helper::doMessage('error', 'You cannot team switch this player on an offical server type.');
            break;

            case "InvalidArguments":
                Helper::createLogEntry('error', 'TeamSwitch', 'Attempted to switch ' . $player . ' to the other team but was missing invalid args', 'BF4');
                return Helper::doMessage('error', 'Invalid Arguments were sent.');
            break;

            case "InvalidTeamId":
                Helper::createLogEntry('error', 'TeamSwitch', 'Attempted to switch ' . $player . ' to the other team with invalid team id', 'BF4');
                return Helper::doMessage('error', 'Invalid team id');
            break;

            case "InvalidPlayerName":
                Helper::createLogEntry('error', 'TeamSwitch', 'Attempted to switch ' . $player . ' to the other team with invalid name', 'BF4');
                return Helper::doMessage('error', 'Invalid player name');
            break;

            case "InvalidForceKill":
                return Helper::doMessage('error', 'Invalid force kill');
            break;

            case "PlayerNotDead":
                return Helper::doMessage('error', 'Player is not dead.');
            break;

            case "SetTeamFailed":
                Helper::createLogEntry('error', 'TeamSwitch', 'Attempted to switch ' . $player . ' to the other team', 'BF4');
                return Helper::doMessage('error', 'Team set failure');
            break;

            case "SetSquadFailed":
                return Helper::doMessage('error', 'Squad set failure');
            break;

            case "OK":
                $this->addRecord('player_fmove', $player, "ForceMove");
                Helper::createLogEntry('success', 'TeamSwitch', $player . ' switched to the other team', 'BF4');
                return Helper::doMessage('success', $player . ' was switched to the other team');
            break;
        }
    }

    /**
     * Move specified player to different squad
     * @param  string  $player
     * @param  integer  $old
     * @param  integer  $new
     * @param  boolean $force  Force move the player to the new squad
     * @return array
     */
    private function adminSquad($player, $old, $new)
    {
        if(!is_numeric($old) || !is_numeric($new))
        {
            return Helper::doMessage('error', 'Must be an integer! Cannot continue.');
        }

        switch($this->bfconn->adminMovePlayerSwitchSquad($player, $new, true))
        {
            case "OK":
                $this->bfconn->adminSayMessageToPlayer($player, 'You were moved from ' . Helper::squadName($old) . ' squad to ' . Helper::squadName($new) . ' squad.');
                $this->addRecord('player_join', $player, 'Moved to ' . Helper::squadName($new) . ' squad from ' . Helper::squadName($old) . ' squad.');
                Helper::createLogEntry('success', 'SquadSwitch', $player . ' moved to ' . Helper::squadName($new) . ' squad from ' . Helper::squadName($old) . ' squad.', 'BF4');
                return Helper::doMessage('success', $player . ' moved to ' . Helper::squadName($new) . ' squad from ' . Helper::squadName($old) . ' squad.');
            break;

            case "SetSquadFailed":
                Helper::createLogEntry('error', 'SquadSwitch', 'Attempted to switch ' . $player . ' to squad ' . Helper::squadName($new), 'BF4');
                return Helper::doMessage('error', 'Could not set ' . $player . ' to squad ' . Helper::squadName($new));
            break;

            case "SetTeamFailed":
                Helper::createLogEntry('error', 'SquadSwitch', 'Attempted to switch ' . $player . ' to squad ' . Helper::squadName($new) . ' on the current team.', 'BF4');
                return Helper::doMessage('error', 'Could not set ' . $player . ' to squad ' . Helper::squadName($new) . ' on the current team.');
            break;

            case "PlayerNotDead":
                $this->bfconn->adminSayMessageToPlayer($player, 'You will be moved to squad ' . Helper::squadName($new) . ' on next death.');
                return Helper::doMessage('success', $player . ' will be moved to squad ' . Helper::squadName($new) . ' on next death.');
            break;

            default:
                return Helper::doMessage('error', 'Unknown error has occured');
            break;
        }
    }

    /**
     * Kill the specified player
     * @param  string $player
     * @param  string $message Kill reason. Leave blank to use default message
     * @return array
     */
    private function adminKillPlayer($player, $message = 'You were killed by an admin')
    {
        switch($this->bfconn->adminKillPlayer($player))
        {
            case "OK":
                $this->bfconn->adminSayMessageToPlayer($player, $message);
                $this->addRecord('player_kill', $player, $message);
                Helper::createLogEntry('success', 'KillPlayer', $player . ' has been killed for ' . $message, 'BF4');
                return Helper::doMessage('success', $player . ' has been killed for ' . $message);
            break;

            case "InvalidPlayerName":
                return Helper::doMessage('error', 'Invalid player name');
            break;

            case "SoldierNotAlive":
                return Helper::doMessage('error', $player . ' is already dead.');
            break;

            case "InvalidArguments":
                return Helper::doMessage('error', 'Unknown error has occured');
            break;

            default:
                return Helper::doMessage('error', 'Unknown error has occured');
            break;
        }
    }

    /**
     * Kick the specified player from the server
     * @param  string $player
     * @param  string $message Kick reason. Leave blank to use default message
     * @return array
     */
    private function adminKickPlayer($player, $message = 'Kicked by administrator')
    {
        switch($this->bfconn->adminKickPlayerWithReason($player, $message))
        {
            case "OK":
                $this->bfconn->adminSayMessageToAll($player . ' has been kicked from the server for ' . $message);
                $this->addRecord('player_kick', $player, $message);
                Helper::createLogEntry('success', 'KillPlayer', $player . ' was kicked from the server for ' . $message, 'BF4');
                return Helper::doMessage('success', $player . ' was kicked from the server for ' . $message);
            break;

            case "PlayerNotFound":
                return Helper::doMessage('error', 'Player not found');
            break;

            default:
                return Helper::doMessage('error', 'Unknown error has occured. Player was not kicked.');
            break;
        }
    }

    /**
     * Forgive player
     * @param  string  $player
     * @param  string  $message Reason
     * @param  integer $amount How many times player should be forgiven
     * @return array
     */
    private function adminForgivePlayer($player, $message, $amount = 1)
    {
        $forgive = \Records::where('command_type', Helper::getAdkatsCommandId('player_forgive'))
                         ->where('record_time', '>=', date('Y-m-d H:i:s', strtotime('-30 Seconds')))
                         ->where('record_time', '<=', date('Y-m-d H:i:s', time()))
                         ->where('target_id', '=', $this->playerID)
                         ->where('server_id', '=', $this->ServerID)
                         ->orderBy('record_id', 'desc')->first();

        // Has the player already been forgiven?
        if($forgive)
        {
            Helper::createLogEntry('error', 'ForgivePlayer', $player . ' has already been forgiven in the last 30 seconds by ' . $forgive->source_name, 'BF4');
            return Helper::doMessage('error', $player . ' has already been forgiven in the last 30 seconds by ' . $forgive->source_name);
        }
        // Lets forgive this player
        else
        {
            for($i=0; $i < $amount; $i++)
            {
                $this->addRecord('player_forgive', $player, $message);
            }

            Helper::createLogEntry('success', 'ForgivePlayer', $player . ' forgiven ' . $amount . ' time(s) for ' . $message, 'BF4');

            return Helper::doMessage('success', $this->admin . ', you have forgiven ' . $player . ' ' . $amount . ' time(s) for ' . $message);
        }
    }

    /**
     * Punish player
     * @param  string $player
     * @param  string $message Punish reason
     * @return array
     */
    private function adminPunishPlayer($player, $message)
    {
        $punish = \Records::where('command_type', Helper::getAdkatsCommandId('player_punish'))
                         ->where('record_time', '>=', date('Y-m-d H:i:s', strtotime('-30 Seconds')))
                         ->where('record_time', '<=', date('Y-m-d H:i:s', time()))
                         ->where('target_id', '=', $this->playerID)
                         ->where('server_id', '=', $this->ServerID)
                         ->orderBy('record_id', 'desc')->first();

        if($punish)
        {
            Helper::createLogEntry('error', 'PunishPlayer', $player . ' has already been punished in the last 30 seconds by ' . $forgive->source_name, 'BF4');
            return Helper::doMessage('error', $player . ' has already been punished in the last 30 seconds by ' . $punish->source_name);
        }
        else
        {
            $iropunish = \Records::where('command_type', Helper::getAdkatsCommandId('player_punish'))
                                ->where('record_time', '>=', date('Y-m-d H:i:s', strtotime('-5 Minutes')))
                                ->where('record_time', '<=', date('Y-m-d H:i:s', time()))
                                ->where('target_id', '=', $this->playerID)
                                ->where('server_id', '=', $this->ServerID)
                                ->orderBy('record_id', 'desc')->first();
            if($iropunish)
            {
                $this->addRecord('player_punish', $player, $message . ' [IRO]');
            }

            Helper::createLogEntry('success', 'PunishPlayer', $message, 'BF4');

            $this->addRecord('player_punish', $player, $message, TRUE);
        }

        return Helper::doMessage('success', $player . ' was punished for ' . $message);
    }

    /**
     * Temp ban a player for X minutes
     * @param $player   string   Player name
     * @param $message  string   Ban reason
     * @param $duration integer  How long the ban should last in minutes
     * @return array
     */
    private function adminTempBan($player, $message, $duration = 120)
    {
        if(is_null($duration) || empty($duration) || !is_numeric($duration) || $duration < 1)
        {
            $duration = Config::get('webadmin.max_tban');
        }

        if($duration > Config::get('webadmin.max_tban'))
        {
            $duration = Config::get('webadmin.max_tban');
        }

        $this->addRecord('player_ban_temp', $player, $message, FALSE, $duration);
        return Helper::doMessage('success', $player . ' was temp banned for ' . $duration . ' minutes for ' . $message);
    }

    /**
     * Perma ban a player for life
     * @param $player   string   Player name
     * @param $message  string   Ban reason
     * @return array
     */
    private function adminPermBan($player, $message)
    {
        $this->addRecord('player_ban_perm', $player, $message);
        return Helper::doMessage('success', $player . ' was permanently banned for ' . $message);
    }

    /**
     * This will kill all the players on the server.
     * @return array
     */
    private function adminNuke()
    {
        if($this->bfconn->getCurrentPlayers() < 2)
            return Helper::doMessage('error', 'Need at least 2 players in the server to preform a nuke.');

        $playerlist = $this->bfconn->adminGetPlayerlist();

        for($i=0; $i < $this->bfconn->getCurrentPlayers(); $i++)
        {
            $playerName[] = $playerlist[($playerlist[1]) * $i + $playerlist[1] + 3];
        }

        for($i=0; $i < count(array_filter($playerName)); $i++)
        {
            Helper::createLogEntry('success', 'Nuke', 'Killed ' . $playerName[$i], 'BF4');
            $this->adminKillPlayer($playerName[$i], 'You were killed by an admin nuke.');
        }

        Helper::createLogEntry('success', 'Nuke', 'Killed all players on server ' . $this->ServerID, 'BF4');

        return Helper::doMessage('success', 'Nuke successfuly completed.');
    }

    /**
     * This will kick all players from the server.
     * @return array
     */
    private function adminKickAll()
    {
        $playerlist = $this->bfconn->adminGetPlayerlist();

        for($i=0; $i < $this->bfconn->getCurrentPlayers(); $i++)
        {
            $playerName[] = $playerlist[($playerlist[1]) * $i + $playerlist[1] + 3];
        }

        for($i=0; $i < count(array_filter($playerName)); $i++)
        {
            Helper::createLogEntry('success', 'NukeKick', 'Kicked ' . $playerName[$i], 'BF4');
            $this->adminKickPlayer($playerName[$i], 'You were kicked from the server.');
        }

        Helper::createLogEntry('success', 'NukeKick', 'Kicked all players on server ' . $this->ServerID, 'BF4');

        return Helper::doMessage('success', 'You kicked all players from the server.');
    }

    /**
     * Gets the current user admin name
     */
    protected function setAdmin()
    {
        // If we cannot load the current user gamer name then exit out of the function
        if(!isset($this->user->gamer_name) || empty($this->user->gamer_name))
            return;

        $local_admin = \Player::where('SoldierName', '=', $this->user->gamer_name)->where('GameID', '=', $this->GameID)->select('SoldierName', 'PlayerID')->first();

        if($local_admin)
        {
            $this->admin = $local_admin->SoldierName;
            $this->adminID   = $local_admin->PlayerID;
        }
        else
        {
            $this->admin = FALSE;
        }
    }

    /**
     * Adds the action to records table
     * @param string  $command  Command issued
     * @param string  $player   Player Name
     * @param string  $message  Reason for action
     * @param boolean $adkats   Should AdKats act on this record?
     * @param integer $duration Used for bans
     * @return void
     */
    protected function addRecord($command, $player, $message, $adkats = FALSE, $duration = 0)
    {
        // Create the chatlog entry
        if($command == 'admin_say')
        {
            DB::table('tbl_chatlog')->insert(array(
                'logDate'           => date('Y-m-d H:i:s', time()),
                'ServerID'          => $this->ServerID,
                'logSubset'         => 'Global',
                'logSoldiername'    => $this->admin,
                'logMessage'        => $message
            ));

            return;
        }

        // Add record to the database
        $record = new \Records;
        $record->server_id          = $this->ServerID;
        $record->command_type       = Helper::getAdkatsCommandId($command);
        $record->command_action     = Helper::getAdkatsCommandId($command);
        $record->target_name        = $player;
        $record->target_id          = $this->playerID;
        $record->target_name        = $player;
        $record->source_name        = $this->admin;
        $record->source_id          = $this->adminID;
        $record->record_message     = $message;
        $record->adkats_read        = $adkats ? 'N' : 'Y'; // Should AdKats act on this record? N = Yes, Y = No
        $record->adkats_web         = TRUE;
        $record->record_time        = date('Y-m-d H:i:s', time());
        $record->command_numeric    = $duration;
        $record->save();

        if($command == 'player_ban_perm' || $command == 'player_ban_temp')
        {
            // If a ban record already exists lets update that one instead of creating a new one
            if($ban = \Ban::where('player_id', '=', $this->playerID)->first())
            {
                $ban->latest_record_id = $record->record_id;
                $ban->ban_startTime    = date('Y-m-d H:i:s', time());
                $ban->ban_status       = 'Active';
                $ban->ban_notes        = "Updated by " . $this->admin . " on " . date('D M jS, Y g:i:sA T', time());

                // Perma ban player
                if($command == 'player_ban_perm')
                {
                    $ban->ban_endTime = date('Y-m-d H:i:s', strtotime("+20 years")); // Ban a player for 20 years from now
                }
                else if($command == 'player_ban_temp')
                {
                    $ban->ban_endTime = date('Y-m-d H:i:s', strtotime("+" . $duration . " minutes")); // Ban a player for certain amount of time
                }

                $ban->save();

                Helper::createLogEntry('success', 'BanUpdate', 'Updated ' . $player . ' ban record. Ban #' . $ban->ban_id, 'BF4');
            }
            // Create a new ban if one was not found
            else
            {
                $ban                   = new \Ban;
                $ban->player_id        = $this->playerID;
                $ban->latest_record_id = $record->record_id;
                $ban->ban_startTime    = date('Y-m-d H:i:s', time());
                $ban->ban_status       = 'Active';

                // Perma ban player
                if($command == 'player_ban_perm')
                {
                    $ban->ban_endTime = date('Y-m-d H:i:s', strtotime("+20 years")); // Ban a player for 20 years from now
                }
                else if($command == 'player_ban_temp')
                {
                    $ban->ban_endTime = date('Y-m-d H:i:s', strtotime("+" . $duration . " minutes")); // Ban a player for certain amount of time
                }

                $ban->save();

                Helper::createLogEntry('success', 'BanIssued', $player . ' has been banned. Ban #' . $ban->ban_id, 'BF4');
            }

            // Get the recent ban record
            $banInfo = \Ban::find($ban->ban_id);

            // Get the appeal message for ban
            $banAppealMessage    = DB::table('adkats_settings')->where('setting_name', '=', 'Additional Ban Message')->where('server_id', '=', $this->ServerID)->pluck('setting_value');

            // Should we use the appeal message? True or False
            $banUseAppealMessage = DB::table('adkats_settings')->where('setting_name', '=', 'Use Additional Ban Message')->where('server_id', '=', $this->ServerID)->pluck('setting_value');

            // As the function reads, Calcuate the Date/Time difference
            $dateTimeDiff = Helper::calculateDateTimeDifference(strtotime($banInfo->ban_startTime), strtotime($banInfo->ban_endTime));

            // If the ban is greater than 1000 days just say perm
            if($dateTimeDiff->days > 1000)
            {
                $banDurationString = "[perm]";
            }
            else
            {
                $banDurationString = "[" . Helper::formatBanTimespan($dateTimeDiff) . "]";
            }

            // Appeal message; empty string if we are not using it
            $banAppend = ($banUseAppealMessage == 'True' ? "[" . $banAppealMessage . "]" : "");

            // User who issued the ban
            $banSourceName = "[" . $this->admin . "]";

            $finalMessage = $message . " " . $banDurationString . $banSourceName . $banAppend;

            // Trim the kick message if neccessary
            $cutLength = strlen($finalMessage) - 80;

            if($cutLength > 0)
            {
                $shortenMessage = substr($message, strlen($message) - $cutLength);
                $finalMessage = $shortenMessage . " " . $banDurationString . $banSourceName . $banAppend;
            }

            $finalMessage = trim($finalMessage); // Remove any whitespace that may have been left behind

            $this->bfconn->adminKickPlayerWithReason($player, $finalMessage);
            $this->bfconn->adminSayMessageToAll("Enforcing ban on " . $player . " for " . $message);
        }
    }
}

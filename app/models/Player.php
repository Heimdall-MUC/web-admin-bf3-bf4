<?php

class Player extends Eloquent
{
    protected $table = 'tbl_playerdata';
    protected $primaryKey = 'PlayerID';
    public $timestamps = false;
}

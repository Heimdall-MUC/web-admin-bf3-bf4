<?php
/**
 * Class to create/modify/add bans for BF3/BF4
 */

class Ban extends Eloquent
{
    protected $table = 'adkats_bans';
    protected $primaryKey = 'ban_id';
    public $timestamps = false;

    public function record()
    {
        return $this->belongsTo('Records', 'latest_record_id');
    }

    public function player()
    {
        return $this->belongsTo('Player', 'player_id');
    }
}

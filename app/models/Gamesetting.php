<?php

class Gamesetting extends Eloquent
{
	protected $primaryKey = 'server_id';

	public function getRconHash()
	{
		return $this->rconHash;
	}

    public function getRconPass()
    {
        return Crypt::decrypt($this->rconHash);
    }

	public function getSetting()
	{
		return $this->settings;
	}

    public function serverinfo()
    {
        return $this->belongsTo('Servers', 'server_id');
    }
}

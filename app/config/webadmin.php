<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Version Number
    |--------------------------------------------------------------------------
    | Current version of the Web Admin
    |
    */

   'version' => "1.4.2",

   /*
    |--------------------------------------------------------------------------
    | Battlefield 3 Support
    |--------------------------------------------------------------------------
    | Set to TRUE if you are going to use battlefield 3 on the web admin
    | otherwise set to FALSE
    |
    */

   'bf3' => TRUE,

   /*
    |--------------------------------------------------------------------------
    | Battlefield 4 Support
    |--------------------------------------------------------------------------
    | Set to TRUE if you are going to use battlefield 4 on the web admin
    | otherwise set to FALSE
    |
    */

   'bf4' => TRUE,

   /*
    |--------------------------------------------------------------------------
    | Maximum temp ban duration in minutes
    |--------------------------------------------------------------------------
    | Set how long you can temp ban a player from the live scoreboard in minutes
    |
    | Default: 120 (2 hours)
    |
    */

   'max_tban' => 120,

   /*
    |--------------------------------------------------------------------------
    | Server List Accending Ordering
    |--------------------------------------------------------------------------
    | To order your server listing throughout the site specify one of these
    | settings. This is case sensitive and only these values will work.
    |
    | Default: ServerID
    |
    | Valid Settings: ServerID, ServerName
    |
    */

    'server_list_order' => 'ServerID',

);

<?php

class BaseController extends Controller
{
    public $user  = FALSE;

    public $group = FALSE;

    public $logged_in = FALSE;

    public function __construct()
    {
        $this->logged_in = Auth::check();

        if($this->logged_in)
        {
            $this->user  = Auth::user();
            $this->group = $this->user->getRole();
        }
        else
        {
            $this->user  = (object) array('username' => 'Guest', 'email' => '');
            $this->group = (object) array('name' => 'Guest');
        }
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout))
        {
            $this->layout = View::make($this->layout);

            $navpages = DB::table('pages')->get();

            foreach($navpages as $p)
            {
                if($p->section == 'bf3') {
                    $bf3Nav[] = $p;
                } else if($p->section == 'bf3admin') {
                    $bf3ANav[] = $p;
                } else if($p->section == 'bf4') {
                    $bf4Nav[] = $p;
                } else if($p->section == 'bf4admin') {
                    $bf4ANav[] = $p;
                } else if($p->section == 'webadmin') {
                    $webNav[] = $p;
                }
            }

            // Remove the player info page
            foreach($bf3Nav as $key => $nav)
            {
                if(in_array($nav->uri, array("bf3/playerinfo")))
                {
                    unset($bf3Nav[$key]);
                }
            }

            foreach($bf3ANav as $key => $nav)
            {
                if(in_array($nav->uri, array("bf3/admin/banmanangement", "bf3/admin/playermanangement")))
                {
                    unset($bf3ANav[$key]);
                }
            }

            foreach($bf4Nav as $key => $nav)
            {
                if(in_array($nav->uri, array("bf4/playerinfo")))
                {
                    unset($bf4Nav[$key]);
                }
            }

            foreach($bf4ANav as $key => $nav)
            {
                if(in_array($nav->uri, array("bf4/admin/banmanangement", "bf4/admin/playermanangement")))
                {
                    unset($bf4ANav[$key]);
                }
            }

            // Pass the varables to the view
            View::share('user', $this->user);
            View::share('title', Helper::getPageTitle());
            View::share('group', $this->group);
            View::share('navbf3', $bf3Nav);
            View::share('navbf4', $bf4Nav);
            View::share('navbf3admin', $bf3ANav);
            View::share('navbf4admin', $bf4ANav);
            View::share('navweb', $webNav);
            View::share('authcheck', Auth::check());
        }
    }

}

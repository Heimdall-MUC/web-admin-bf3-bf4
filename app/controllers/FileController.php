<?php

class FileController extends \BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $results = DB::table('uploads')->where('user_id', '=', Auth::user()->id)->get();
        return Response::json($results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->layout->content = View::make('upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        // Before we actually do anything, check to make
        // sure the directory exists and is writeable
        // otherwise attempt to create it
        if(!file_exists(storage_path() . "/uploads"))
        {
            if(!mkdir(storage_path() . "/uploads", 0777))
                return self::doMessage('error', 'Could not create the uploads directory. Please check if storage folder has write permissions.');
        }

        // If we got to this point lets start gathering
        // the information we need to store the file

        // Get the current user
        $user = User::find(Auth::user()->id);

        // Path to the users folder
        $userDir = storage_path() . "/uploads/user/" . strtolower($user->username);

        // Does the user's folder exist?
        if(!is_dir($userDir))
        {
            // If not attempt to create it otherwise return an error back to the user
            if(!mkdir($userDir, 0777))
                return self::doMessage('error', 'Could not create the directory.');
        }

        if(!Input::hasFile('files'))
            return self::doMessage('error', 'No file was sent.');

        $allowedExtension = array('gif', 'jpg', 'jpeg', 'png', 'mp4', 'avi', 'wmv', 'mp3');

        $files = Input::file('files');

        // Yay! Everything checked out now let the real work begin

        foreach($files as $file)
        {
            // Check if file is in the allowed list, if its not skip it
            if(!in_array($file->getClientOriginalExtension(), $allowedExtension)) continue;

            #############################
            # Storeing the file to disk #
            #############################

            $hash       = md5_file($file);                                  // MD5 hash of file
            $extension  = strtolower($file->getClientOriginalExtension());  // File extension
            $mime       = $file->getMimeType();                             // Mime type of file
            $bytesize   = $file->getSize();                                 // Byte size of file
            $name       = $file->getClientOriginalName();                   // Original file name
            $filename   = time() . "_" . $hash . ".{$extension}";           // Final filename

            // Check to make sure file doesn't already exist
            $exist = DB::table('uploads')->where('user_id', '=', $user->id)->where('hash', '=', $hash)->first();

            // If it does exist redirect to the file
            if($exist) continue;

            $uploadSucc = $file->move($userDir . '/', $filename);   // Move the uploaded file to the user directory and rename it

            ###############################################
            # Database section for storeing the file info #
            ###############################################

            $fileDetails = array(
                'user_id'       => $user->id,
                'hash'          => $hash,
                'mime'          => $mime,
                'bytes'         => $bytesize,
                'extension'     => $extension,
                'original_name' => $name,
                'file_name'     => $filename
            );

            DB::table('uploads')->insert($fileDetails);
        }

        return Redirect::action('FileController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $file = DB::table('uploads')->where('id', '=', $id)->first();

        if(!$file)
            App::abort(404);

        $user = User::find($file->user_id);

        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: inline; filename=\"" . $file->original_name . "\"");
        header("Content-Type: " . $file->mime);
        header("Content-Transfer-Encoding: binary");

        readfile(storage_path() . "/uploads/user/" . strtolower($user->username) . '/' . $file->file_name);
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $file = DB::table('uploads')->where('id', '=', $id)->first();

        if(!$file)
            App::abort(404);

        $user = User::find($file->user_id);

        unlink(storage_path() . "/uploads/user/" . strtolower($user->username) . '/' . $file->file_name);
        DB::table('uploads')->where('id', '=', $id)->delete();

        return Helper::doMessage('success', $file->original_name . ' deleted.');
    }
}

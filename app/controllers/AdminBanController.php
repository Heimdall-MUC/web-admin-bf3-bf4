<?php

class AdminBanController extends BaseController
{
    protected $layout = 'layouts.master';

    public function __construct()
    {
        parent::__construct();

        // Update expired bans
        DB::update("UPDATE `adkats_bans` SET `ban_status` = 'Expired' WHERE `ban_status` = 'Active' AND `ban_endTime` <= UTC_TIMESTAMP()");
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->layout->content = View::make('banlist');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $end_date       = Input::get('modifyend_date', NULL);
        $end_time       = Input::get('modifyend_time', NULL);
        $end_meridiem   = Input::get('modifyend_meridiem', NULL);
        $ban_type       = Input::get('ban_type', 7);
        $start_date     = Input::get('modifystart_date', NULL);
        $start_time     = Input::get('modifystart_time', NULL);
        $start_meridiem = Input::get('modifystart_meridiem', NULL);
        $ban_id         = Input::get('ban_id', NULL);

        // Validation check
        $rules = array(
            'modifyend_date'       => 'required_without:ban_type|date',
            'modifyend_meridiem'   => 'in:am,pm',
            'modifystart_date'     => 'required_without:ban_type|date',
            'modifystart_meridiem' => 'in:am,pm',
            'ban_playerReason'     => 'required',
            'ban_playerSourceName' => 'required|alpha_dash',
            'ban_playerServer'     => 'required|numeric'
        );

        $validation = Validator::make(Input::all(), $rules);

        if($validation->fails())
        {
            if(Request::ajax())
            {
                return Response::json(Helper::doMessage('error', 'Missing fields', array('errors' => $validation->messages()->all())));
            }
            else
            {
                return Redirect::route(strtolower(Request::segment(1)) . 'banshow', array($ban_id))->withErrors($validation);
            }
        }

        if(isset($end_date, $end_time, $end_meridiem, $start_date, $start_time, $start_meridiem) && $ban_type == 7)
        {
            $end_hour            = strstr($end_time, '_', TRUE);
            $end_min             = str_replace('_', '', strstr($end_time, '_'));
            $endtime             = $end_hour . ":" . $end_min . ":00 " . strtoupper($end_meridiem);

            $start_hour          = strstr($start_time, '_', TRUE);
            $start_min           = str_replace('_', '', strstr($start_time, '_'));
            $starttime           = $start_hour . ":" . $start_min . ":00 " . strtoupper($start_meridiem);

            $start_epoc          = strtotime($start_date . " " . $starttime);
            $end_epoc            = strtotime(Helper::convertLocalToUTC(strtotime($end_date . " " . $endtime), TRUE));
            $minutesDiff         = ($end_epoc - $start_epoc) / 60;

            $datetimestart_final = Helper::convertLocalToUTC(strtotime($start_date . " " . $starttime), TRUE);
            $datetimeend_final   = Helper::convertLocalToUTC(strtotime($end_date . " " . $endtime), TRUE);
        }
        else
        {
            $start_epoc          = strtotime("now", time());
            $end_epoc            = strtotime("+20 years", time());
            $minutesDiff         = ($end_epoc - $start_epoc) / 60;

            $datetimestart_final = date('Y-m-d H:i:s', $start_epoc);
            $datetimeend_final   = date('Y-m-d H:i:s', $end_epoc);
        }

        if(is_null($ban_id))
        {
            $ban = new Ban;
            $ban->player_id = Input::get('ban_targetid');
        }
        else
        {
            $ban = Ban::findOrFail($ban_id);
        }

        $source_player_id = Player::where('GameID', Helper::fetchGameId(Input::get('game')))->where('SoldierName', Input::get('ban_playerSourceName'))->pluck('PlayerID');

        $record = new Records;
        $record->server_id       = Input::get('ban_playerServer');
        $record->command_type    = $ban_type;
        $record->command_action  = $ban_type;
        $record->target_name     = (Input::has('ban_targetname')) ? Input::get('ban_targetname') : $ban->record->target_name;
        $record->target_id       = (Input::has('ban_targetid')) ? Input::get('ban_targetid') : $ban->record->target_id;
        $record->source_name     = Input::get('ban_playerSourceName');
        $record->source_id       = $source_player_id;
        $record->record_message  = Input::get('ban_playerReason');
        $record->adkats_read     = 'Y';
        $record->adkats_web      = TRUE;
        $record->record_time     = date('Y-m-d H:i:s', $start_epoc);
        $record->command_numeric = abs($minutesDiff);
        $record->save();

        $ban->latest_record_id = $record->record_id;
        $ban->ban_status       = 'Active';
        $ban->ban_startTime    = $datetimestart_final;
        $ban->ban_endTime      = $datetimeend_final;
        $ban->save();

        if(Request::ajax())
        {
            return Response::json(Helper::doMessage('success', 'Ban has been created.', array('ban_id' => $ban->ban_id)));
        }
        else
        {
            return Redirect::route(strtolower(Request::segment(1)) . 'banshow', array($ban_id))->with('success', 'Ban has been created.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $ban = Ban::findOrFail($id);

        View::share('title', $ban->player->SoldierName . ' Ban Record');

        $edit = FALSE;

        $game = strtolower(Request::segment(1));

        if(Entrust::can('edit' . $game . 'ban') || Entrust::can('fullbfaccess') || Entrust::can('allperms'))
        {
            $edit = TRUE;
        }

        foreach(range(1, 12) as $hour)
        {
            if($hour < 10) $hour = '0' . $hour;

            foreach(range(0, 59, 5) as $min)
            {
                if($min < 10) $min = '0' . $min;
                $key = $hour . '_' . $min;
                $times[$key] = $hour . ':' . $min;
                unset($min);
            }
        }

        $servers = Servers::select('ServerID', 'ServerName')->where('ConnectionState', 'on')
                            ->where('GameID', Helper::fetchGameId(strtolower(Request::segment(1))))
                            ->orderBy(Config::get('webadmin.server_list_order'), 'asc')->get();
        if($servers)
        {
            foreach($servers as $server)
            {
                $serverlist[$server->ServerID] = $server->ServerName;
            }
        }
        else
        {
            $serverlist = array();
        }

        $this->layout->content = View::make('singleban', array('ban' => $ban, 'edit' => $edit, 'game' => $game, 'times' => $times, 'serverlist' => $serverlist));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $end_date         = Input::get('modifyend_date');
        $end_time         = Input::get('modifyend_time');
        $end_meridiem     = Input::get('modifyend_meridiem');
        $ban_status       = Input::get('ban_status', 'Disabled');
        $ban_type         = Input::get('ban_type', 7);
        $ban_start        = date('Y-m-d H:i:s');
        $ban_enforce_ip   = Input::get('ban_enforce_ip', 'N');
        $ban_enforce_name = Input::get('ban_enforce_name', 'N');
        $ban_enforce_guid = Input::get('ban_enforce_guid', 'N');

        // Validation check
        $rules = array(
            'modifyend_date'     => 'required_without:ban_type|date',
            'modifyend_meridiem' => 'in:am,pm'
        );

        $validation = Validator::make(Input::all(), $rules);

        if($validation->fails())
        {
            return Redirect::route(strtolower(Request::segment(1)) . 'banshow', array($id))->withErrors($validation);
        }

        if($ban_enforce_guid == 'N' && $ban_enforce_name == 'N' && $ban_enforce_ip == 'N')
        {
            return Redirect::route(strtolower(Request::segment(1)) . 'banshow', array($id))->with('error', 'You must choose an enforcement option. Recommendation: Enforce by EAGUID');
        }

        if($ban_type == 7)
        {
            $end_hour            = strstr($end_time, '_', TRUE);
            $end_min             = str_replace('_', '', strstr($end_time, '_'));
            $endtime             = $end_hour . ":" . $end_min . ":00 " . strtoupper($end_meridiem);

            $start_epoc          = strtotime("now", time());
            $end_epoc            = strtotime(Helper::convertLocalToUTC(strtotime($end_date . " " . $endtime), TRUE));
            $minutesDiff         = ($end_epoc - $start_epoc) / 60;

            $datetimeend_final   = Helper::convertLocalToUTC(strtotime($end_date . " " . $endtime), TRUE);
        }
        else
        {
            $start_epoc          = strtotime("now", time());
            $end_epoc            = strtotime("+20 years", time());
            $minutesDiff         = ($end_epoc - $start_epoc) / 60;

            $datetimeend_final   = date('Y-m-d H:i:s', $end_epoc);
        }

        $ban = Ban::findOrFail($id);

        $ban->ban_enforceName = $ban_enforce_name;
        $ban->ban_enforceGUID = $ban_enforce_guid;
        $ban->ban_enforceIP   = $ban_enforce_ip;

        if(strtotime($datetimeend_final) != strtotime($ban->ban_endTime) || trim(Input::get('ban_playerReason')) != trim($ban->record->record_message))
        {
            $record = new Records;
            $record->server_id       = $ban->record->server_id;
            $record->command_type    = $ban_type;
            $record->command_action  = $ban_type;
            $record->target_name     = $ban->record->target_name;
            $record->target_id       = $ban->record->target_id;
            $record->source_name     = $ban->record->source_name;
            $record->source_id       = $ban->record->source_id;
            $record->record_message  = Input::get('ban_playerReason');
            $record->adkats_read     = 'Y';
            $record->adkats_web      = TRUE;
            $record->record_time     = $ban_start;
            $record->command_numeric = abs($minutesDiff);
            $record->save();

            $ban->ban_notes        = "Ban last updated by " . Auth::user()->gamer_name . " on " . date('M j, Y g:i A T', time());
            $ban->latest_record_id = $record->record_id;
            $ban->ban_status       = $ban_status;
            $ban->ban_startTime    = $ban_start;
            $ban->ban_endTime      = $datetimeend_final;
            $ban->save();
        }
        else
        {
            $ban->ban_notes        = "Ban last updated by " . Auth::user()->gamer_name . " on " . date('M j, Y g:i A T', time());
            $ban->ban_status       = $ban_status;
            $ban->save();
        }

        return Redirect::route(strtolower(Request::segment(1)) . 'banshow', array($id))->with('success', 'Ban has been updated.');
    }

}

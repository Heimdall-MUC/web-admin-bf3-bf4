<?php

class PlayerController extends BaseController
{
    protected $layout = 'layouts.master';

    public function bf3info($id = -1)
    {
        $player_details = Helper::buildPlayerProfile($id, 'BF3');

        if(is_array($player_details) && $player_details['status'] == 'error') App::abort(404);

        $player_session = Helper::getPlayerSessions($id);
        $player_stats   = Helper::getPlayerStats($id);
        $this->layout->content = View::make('bf3.playerdetails', array('info' => $player_details, 'sessions' => $player_session['more'], 'stats' => $player_stats['more']));
    }

    public function bf4info($id = -1)
    {
        $player_details = Helper::buildPlayerProfile($id, 'BF4');

        if(is_array($player_details) && $player_details['status'] == 'error') App::abort(404);

        $player_session = Helper::getPlayerSessions($id);
        $player_stats   = Helper::getPlayerStats($id);
        $this->layout->content = View::make('bf4.playerdetails', array('info' => $player_details, 'sessions' => $player_session['more'], 'stats' => $player_stats['more']));
    }

    public function searchbf3()
    {
        if(Input::has('player'))
        {
            return Helper::searchForPlayer(Input::get('player', FALSE));
        }

        $recentplayers = Player::where('GameID', '=', Helper::fetchGameId('BF3'))->orderBy('PlayerID', 'desc')->take(20)->get();
        $this->layout->content = View::make('bf3.searchplayer', array('players' => $recentplayers));
    }

    public function searchbf4()
    {
        if(Input::has('player'))
        {
            return Helper::searchForPlayer(Input::get('player', FALSE));
        }

        $recentplayers = Player::where('GameID', '=', Helper::fetchGameId('BF4'))->orderBy('PlayerID', 'desc')->take(20)->get();
        $this->layout->content = View::make('bf4.searchplayer', array('players' => $recentplayers));
    }

    public function issueForgive()
    {
        $validator = Validator::make(Input::all(), array(
                'forgive_targetname' => 'required|alpha_dash',
                'forgive_game'       => 'required|in:BF3,BF4',
                'forgive_amount'     => 'required|numeric|between:1,100',
                'forgive_targetid'   => 'required|numeric',
                'forgive_server'     => 'required|numeric'
            ));

        if($validator->fails())
        {
            return Helper::doMessage('error', 'Validation failed', $validator->messages()->all());
        }

        if(Auth::guest())
        {
            return Helper::doMessage('error', 'Not logged in');
        }

        $user = Auth::user();

        if(Input::get('forgive_game') == 'BF3')
        {
            if(!$user->can('bf3_forgive') && !$user->can('allperms'))
            {
                return Response::json(Helper::doMessage('error', 'You do not have the required permission to preform this action'));
            }
        }
        else if(Input::get('forgive_game') == 'BF4')
        {
            if(!$user->can('bf4_forgive') && !$user->can('allperms'))
            {
                return Response::json(Helper::doMessage('error', 'You do not have the required permission to preform this action'));
            }
        }
        else
        {
            return Helper::doMessage('error', 'No game specified');
        }

        $sourcePlayer = Player::where('SoldierName', $user->gamer_name)->where('GameID', Helper::fetchGameId(Input::get('forgive_game')))->first();

        if(!$sourcePlayer) return Helper::doMessage('error', 'Could not find you in the database. ' . $user->username . ', is your gamer name correct?');

        $infraction_servers = DB::table('adkats_infractions_server')->where('player_id', Input::get('forgive_targetid'))->where('server_id', Input::get('forgive_server'))->first();

        if(!$infraction_servers)
        {
            return Response::json(Helper::doMessage('error', NULL, array('errors' => array("You can't forgive a player if they don't have any prior infraction history for this server"))));
        }

        for($i=0; $i < Input::get('forgive_amount', 1); $i++)
        {
            $record                  = new Records;
            $record->server_id       = Input::get('forgive_server');
            $record->command_type    = 10;
            $record->command_action  = 10;
            $record->command_numeric = 0;
            $record->target_name     = Input::get('forgive_targetname');
            $record->target_id       = Input::get('forgive_targetid');
            $record->source_name     = $sourcePlayer->SoldierName;
            $record->source_id       = $sourcePlayer->PlayerID;
            $record->record_message  = Input::get('forgive_reason', 'ForgivePlayer');
            $record->record_time     = date('Y-m-d H:i:s', time());
            $record->adkats_read     = 'Y';
            $record->adkats_web      = TRUE;
            $record->save();
        }

        if($record->record_id)
        {
            return Helper::doMessage('success', 'Added ' . Input::get('forgive_amount', 1) . ' forgive point(s) to ' . Input::get('forgive_targetname'));
        }
        else
        {
            return Helper::doMessage('error', 'Failed to give ' . Input::get('forgive_amount', 1) . ' forgive point(s) to ' . Input::get('forgive_targetname'));
        }
    }
}

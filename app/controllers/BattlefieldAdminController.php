<?php

class BattlefieldAdminController extends BaseController
{
    protected $layout = 'layouts.master';

    public function showAdKatRecords()
    {
        $GameID = Helper::fetchGameId(strtoupper(Request::segment(1)));

        $records = Records::where('tbl_server.GameID', '=', $GameID)
                          ->leftJoin('tbl_server', 'adkats_records_main.server_id', '=', 'tbl_server.ServerID')
                          ->leftJoin('adkats_commands', 'adkats_records_main.command_action', '=', 'adkats_commands.command_id')
                          ->select(DB::raw('adkats_records_main.record_id, adkats_records_main.target_name, adkats_records_main.target_id, adkats_records_main.source_name, adkats_records_main.source_id, adkats_records_main.record_message, adkats_records_main.record_time, adkats_records_main.adkats_web, adkats_commands.command_name, adkats_records_main.server_id'))
                          ->orderBy('adkats_records_main.record_id', 'desc')->get();

        $this->layout->content = View::make(strtolower(Request::segment(1)) . '.admin.records', array('records' => $records));
    }
}

<?php

class HomeController extends BaseController
{
    protected $layout = 'layouts.master';

    public function index()
    {
        $recentBf3 = Ban::where('tbl_games.Name', '=', 'BF3')
                         ->leftJoin('tbl_playerdata', 'adkats_bans.player_id', '=', 'tbl_playerdata.PlayerID')
                         ->leftJoin('tbl_games', 'tbl_playerdata.GameID', '=', 'tbl_games.GameID')
                         ->where('ban_status', '=', 'Active')
                         ->orderBy('latest_record_id', 'desc')
                         ->select('tbl_playerdata.SoldierName', 'adkats_bans.player_id', 'adkats_bans.ban_endTime', 'adkats_bans.ban_startTime')->take(20)->get();

        $recentBf4 = Ban::where('tbl_games.Name', '=', 'BF4')
                         ->leftJoin('tbl_playerdata', 'adkats_bans.player_id', '=', 'tbl_playerdata.PlayerID')
                         ->leftJoin('tbl_games', 'tbl_playerdata.GameID', '=', 'tbl_games.GameID')
                         ->where('ban_status', '=', 'Active')
                         ->orderBy('latest_record_id', 'desc')
                         ->select('tbl_playerdata.SoldierName', 'adkats_bans.player_id', 'adkats_bans.ban_endTime', 'adkats_bans.ban_startTime')->take(20)->get();


        $this->layout->content = View::make('dashboard', array('bf3bans' => $recentBf3, 'bf4bans' => $recentBf4));
    }

    public function bf4scoreboard()
    {
        $serverlist = DB::table('tbl_server')
                        ->join('tbl_games', 'tbl_server.GameID', '=', 'tbl_games.GameID')
                        ->where('tbl_games.Name', 'BF4')
                        ->where('ConnectionState', 'on')
                        ->select('ServerID', 'ServerName', 'usedSlots', 'maxSlots')
                        ->orderBy(Config::get('webadmin.server_list_order'), 'asc')->get();

        $this->layout->content = View::make('bf4.scoreboard', array('serverlist' => $serverlist));
    }

    public function bf3scoreboard()
    {
        $serverlist = DB::table('tbl_server')
                        ->join('tbl_games', 'tbl_server.GameID', '=', 'tbl_games.GameID')
                        ->where('tbl_games.Name', 'BF3')
                        ->where('ConnectionState', 'on')
                        ->select('ServerID', 'ServerName', 'usedSlots', 'maxSlots')
                        ->orderBy(Config::get('webadmin.server_list_order'), 'asc')->get();

        $this->layout->content = View::make('bf3.scoreboard', array('serverlist' => $serverlist));
    }
}

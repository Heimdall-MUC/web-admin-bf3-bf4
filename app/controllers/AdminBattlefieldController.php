<?php

class AdminBattlefieldController extends BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $servers = Servers::all();

        foreach($servers as $server)
        {
            $result[] = (object) array(
                'name'   => $server->ServerName,
                'ip'     => $server->IP_Address,
                'id'     => $server->ServerID,
                'status' => ($server->ConnectionState == 'on' ? TRUE : FALSE),
                'conn'   => Helper::isPortOpen($server->IP_Address),
                'game'   => $server->GameID
            );
        }

        $this->layout->content =  View::make('admin.gamesettings.serverlist')->with('servers', $result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        if($server = Gamesetting::find($id))
        {
            return Redirect::action('AdminBattlefieldController@edit', array($id));
        }

        $setting = new Gamesetting;
        $setting->server_id = $id;
        $setting->save();

        Helper::createLogEntry('debug', 'AddServer', 'Added server #' . $id . ' to gamesettings');

        return Redirect::action('AdminBattlefieldController@edit', array($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $server = Gamesetting::findOrFail($id);

        View::share('title', 'Edit Server Settings');

        $this->layout->content =  View::make('admin.gamesettings.edit')->with('server', $server);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $setting    = Gamesetting::findOrFail($id);
        $rconpass   = Input::get('rconpass');
        $nameformat = Input::get('nameformat');
        $id         = Input::get('serverid');
        $status     = Input::get('status', 'off');

        if(!empty($rconpass))
        {
            $setting->rconHash = Crypt::encrypt($rconpass);
        }

        $setting->settings = $nameformat;
        $setting->save();

        $server = Servers::find($id);
        $server->ConnectionState = $status;
        $server->save();

        Helper::createLogEntry('debug', 'UpdateServer', 'Updated gamesettings for server #' . $id);

        return Redirect::action('AdminBattlefieldController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $server = Gamesetting::findOrFail($id);
        $server_name = $server->serverinfo->ServerName;
        $server->delete();

        Helper::createLogEntry('debug', 'DelServer', 'Removed server #' . $id . ' from gamesettings');

        return Redirect::action('AdminBattlefieldController@index')->with('success', 'Removed server "' . $server_name . '" from gamesettings');
    }

}

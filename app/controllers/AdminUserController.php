<?php

class AdminUserController extends BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::orderBy('username', 'asc')->get();

        $this->layout->content =  View::make('admin.usermanangement.userlist', array('users' => $users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        foreach(Role::all() as $role)
        {
            $roles[$role->id] = $role->name;
        }

        View::share('title', 'Edit User');

        $this->layout->content =  View::make('admin.usermanangement.edituser', array('user' => $user, 'roles' => $roles));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        $user2 = User::findOrFail($id);

        $validation = Validator::make(Input::all(), array(
                'user_username' => 'required|alpha_dash|unique:users,username,' . $user->id,
                'email'         => 'required|email|unique:users,email,' . $user->id,
                'ign'           => 'required|alpha_dash:unique:users,gamer_name,' . $user->id
            )
        );

        if($validation->fails())
        {
            return Redirect::action('AdminUserController@edit', array($id))->withErrors($validation);
        }

        $user->email      = Input::get('email');
        $user->username   = Input::get('user_username');
        $user->gamer_name = Input::get('ign');
        $user->confirmed  = Input::get('status', 0);
        $user->timezone   = Input::get('timezone', 'UTC');

        if(Input::has('password'))
        {
            $newpassword                 = Input::get('password', str_random(11));
            $user->password              = $newpassword;
            $user->password_confirmation = $newpassword;

            Mail::send('emails.auth.passreset', array('user' => $user, 'newpassword' => $newpassword), function($message)
            {
                $message->to($user2->email, $user2->username)->subject('Your password has been changed');
            });
        }

        $user->roles()->detach($user->getRole()->role_id);
        $user->roles()->attach(Input::get('role', 7));
        $user->updateUniques();

        Helper::createLogEntry('debug', 'UpdateUser', 'Updated ' . $user->username . ' account');

        return Redirect::action('AdminUserController@index')->with('success', 'Successfully updated ' . $user->username . ' account.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $deleted_user = $user->username;
        $user->roles()->detach($user->getRole()->role_id);
        $user->delete();

        Helper::createLogEntry('debug', 'DeleteUser', 'Deleted ' . $deleted_user . ' account');

        return Redirect::action('AdminUserController@index')->with('success', 'Successfully deleted ' . $deleted_user . ' account.');
    }

}

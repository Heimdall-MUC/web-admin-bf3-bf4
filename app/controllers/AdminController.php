<?php

class AdminController extends BaseController
{
    protected $layout = 'layouts.master';

    public function memberList()
    {
        $users = User::paginate(30);

        $this->layout->content =  View::make('admin.userlist', array('users' => $users));
    }

    public function showUser($id)
    {
        $user = User::findOrFail($id);
        $role = $user->getUserGroups();

        $this->layout->content = View::make('admin.showuser', array('user' => $user, 'role' => $role));
    }
}

<?php
/*
|--------------------------------------------------------------------------
| Confide Controller Template
|--------------------------------------------------------------------------
|
| This is the default Confide controller template for controlling user
| authentication. Feel free to change to your needs.
|
*/

class UserController extends BaseController {

    protected $layout = 'layouts.master';

    public $user;

    /**
     * Displays the form for account creation
     *
     */
    public function create()
    {
        return View::make(Config::get('confide::signup_form'));
    }

    /**
     * Stores new account
     *
     */
    public function store()
    {
        $user = new User;

        $user->username     = Input::get('username');
        $user->email        = Input::get('email');
        $user->password     = Input::get('password');
        $user->gamer_name   = Input::get('ign');

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $user->password_confirmation = Input::get( 'password_confirmation' );

        // Save if valid. Password field will be hashed before save
        $user->save();

        if ( $user->id )
        {
            // Assign them to the Registered group
            $user->roles()->attach(7);

            // Redirect with success message, You may replace "Lang::get(..." for your custom message.
            return Response::json(Helper::doMessage('success', Lang::get('confide::confide.alerts.account_created')));
        }
        else
        {
            // Get validation errors (see Ardent package)
            $error = $user->errors()->all(':message');
            return Response::json(Helper::doMessage('error', $error));
        }
    }

    /**
     * Displays the login form
     *
     */
    public function login()
    {
        if( Confide::user() )
        {
            // Redirect user back to the dashboard
            return Redirect::to('/dashboard');
        }
        else
        {
            return View::make('user.login');
        }
    }

    /**
     * Attempt to do login
     *
     */
    public function do_login()
    {
        $input = array(
            'email'    => Input::get( 'identity' ), // May be the username too
            'username' => Input::get( 'identity' ), // so we have to pass both
            'password' => Input::get( 'password' ),
            'remember' => Input::get( 'remember' ),
        );

        if ( Confide::logAttempt( $input, Config::get('confide::signup_confirm') ) )
        {
            return Response::json(Helper::doMessage('success'));
        }
        else
        {
            $user = new User;

            // Check if there was too many login attempts
            if(Confide::isThrottled($input))
            {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            }
            elseif($user->checkUserExists($input) and !$user->isConfirmed($input))
            {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            }
            else
            {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Response::json(Helper::doMessage('error', $err_msg));
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string  $code
     */
    public function confirm( $code )
    {
        if ( Confide::confirm( $code ) )
        {
            $notice_msg = Lang::get('confide::confide.alerts.confirmation');
                        return Redirect::action('UserController@login')
                            ->with( 'notice', $notice_msg );
        }
        else
        {
            $error_msg = Lang::get('confide::confide.alerts.wrong_confirmation');
                        return Redirect::action('UserController@login')
                            ->with( 'error', $error_msg );
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function forgot_password()
    {
        return View::make(Config::get('confide::forgot_password_form'));
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function do_forgot_password()
    {
        if( Confide::forgotPassword( Input::get( 'email' ) ) )
        {
            $notice_msg = Lang::get('confide::confide.alerts.password_forgot');

            return Response::json(Helper::doMessage('success', $notice_msg));
        }
        else
        {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_forgot');

            return Response::json(Helper::doMessage('success', $error_msg));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function reset_password( $token )
    {
        return View::make(Config::get('confide::reset_password_form'))
                ->with('token', $token);
    }

    /**
     * Attempt change password of the user
     *
     */
    public function do_reset_password()
    {
        $input = array(
            'token'=>Input::get( 'token' ),
            'password'=>Input::get( 'password' ),
            'password_confirmation'=>Input::get( 'password_confirmation' ),
        );

        // By passing an array with the token, password and confirmation
        if( Confide::resetPassword( $input ) )
        {
            $notice_msg = Lang::get('confide::confide.alerts.password_reset');
                        return Redirect::action('UserController@login')
                            ->with( 'notice', $notice_msg );
        }
        else
        {
            $error_msg = Lang::get('confide::confide.alerts.wrong_password_reset');
                        return Redirect::action('UserController@reset_password', array('token'=>$input['token']))
                            ->withInput()
                ->with( 'error', $error_msg );
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function logout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    public function show_profile($username = NULL)
    {
        if($username == NULL)
        {
            $user = User::where('id', '=', Auth::user()->id)->select('username', 'email', 'gamer_name', 'created_at', 'id')->first();
        }
        else
        {
            $user = User::where('username', '=', $username)->select('username', 'email', 'gamer_name', 'created_at', 'id')->first();
        }

        $group = DB::table('assigned_roles')->join('roles', 'assigned_roles.role_id', '=', 'roles.id')->select('name', 'role_id')->where('user_id', $user->id)->first();

        $data['info'] = (object) array(
            'user'  => $user,
            'group' => $group
        );

        View::share('title', $user->username . ' Profile');

        $this->layout->content = View::make('profile', $data);
    }

    public function edit_profile()
    {
        $datetime = new DateTime(null, new DateTimeZone($this->user->timezone));
        $currentdatetime = $datetime->format('D M j, Y g:i:s a T');
        View::share('title', 'Profile Settings');
        $this->layout->content = View::make('user.edit', array('currentdatetime' => $currentdatetime));
    }

    public function do_edit_profile()
    {
        // Curent user object
        $user = User::find(Auth::user()->id);

        $IGN                = trim(Input::get('ign', NULL));                // In-game Name
        $TZ                 = trim(Input::get('timezone', NULL));           // User Timezone
        $email              = trim(Input::get('email', NULL));              // Email
        $newPassword        = trim(Input::get('password', NULL));        // Updated Password
        $confirmNewPassword = trim(Input::get('password_confirmation', NULL));  // Confirm Updated Password
        $currentPassword    = trim(Input::get('current_password', NULL));        // Current Password; Used for verification for account setting changes

        // Validation check
        $rules = array(
            'email'    => 'email|unique:users,email',
            'password' => 'confirmed|min:6',
            'ign' => 'alpha_dash|unique:users,gamer_name,' . $user->id . '|min:3'
        );

        $fields = array(
            'email'    => $email,
            'password' => $newPassword,
            'password_confirmation' => $confirmNewPassword,
            'ign' => $IGN
        );

        $validation = Validator::make($fields, $rules);

        if($validation->fails())
        {
            return Redirect::to('/profile/edit')->withErrors($validation);
        }

        // Update the user gamer name if it's different
        if(!empty($IGN) && $user->gamer_name != $IGN)
        {
            $user->gamer_name = $IGN;
        }

        // Update the user timezone
        if($user->timezone != $TZ)
        {
            $user->timezone = $TZ;
        }

        // Check if we are making account changes
        if(!empty($currentPassword) && !empty($currentPassword))
        {


            // Check to make sure the provided password matches with the current user so we can verify they are who they say they are
            if(Hash::check($currentPassword, $user->getAuthPassword()))
            {
                // Now lets update the fields that need updateing
                if(!empty($email) && $user->email != $email)
                {
                    $user->email = $email;
                }

                if(!empty($newPassword) && !empty($confirmNewPassword) && $newPassword == $confirmNewPassword)
                {
                    $user->password = $newPassword;
                    $user->password_confirmation = $confirmNewPassword;
                }
            }
        }

        $dirty = $user->getDirty();

        if(empty($dirty))
        {
            Session::flash('notice', 'There was nothing to update.');
        }
        else
        {
            $user->updateUniques();
            Session::flash('success', 'Successfully updated your profile');
        }

        return Redirect::to('/profile/edit');
    }

}

<?php

class AdminRoleController extends BaseController
{
    protected $layout = 'layouts.master';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles     = Role::all();
        $protected = array('Site Administrator', 'Banned', 'Registered');

        $this->layout->content = View::make('admin.rolesandperms.list', array('roles' => $roles, 'protected_roles' => $protected));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::all();

        foreach($permissions as $perm)
        {
            $section                      = strstr($perm->name, "_", true);
            if($perm->name == 'allperms') continue;
            $perms[$section][$perm->name] = $perm->display_name;
        }

        View::share('title', 'Create Role');

        $this->layout->content = View::make('admin.rolesandperms.createrole', array('perms' => $perms));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = Validator::make(
            array(
                'role_name' => Input::get('role_name')
            ),
            array(
                'role_name' => 'required|unique:roles,name'
            )
        );

        if($validation->fails())
        {
            return Redirect::action('AdminRoleController@create')->withErrors($validation);
        }

        if(!Input::has('permsbf3') && !Input::has('permsbf4') && !Input::has('permswa'))
        {
            return Redirect::action('AdminRoleController@create')
                        ->with('error', 'You need to choose at least 1 permission node to create the ' . Input::get('role_name') . ' role.')
                        ->with('rolename', Input::get('role_name'));
        }

        $role       = new Role;
        $role->name = Input::get('role_name');
        $role->save();

        if(Input::has('permsbf3'))
        {
            foreach(Input::get('permsbf3') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        if(Input::has('permsbf4'))
        {
            foreach(Input::get('permsbf4') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        if(Input::has('permswa'))
        {
            foreach(Input::get('permswa') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        $role->perms()->sync($setperms);

        Session::flash('success', Input::get('role_name') . ' Role Successfully Created');
        Helper::createLogEntry('debug', 'CreateRole', 'Created role "' . $role->name . '"');

        return Redirect::action('AdminRoleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // Block any attempt to delete the main roles needed by the web admin
        $protectedRoles = Role::whereIn('name', array('Site Administrator', 'Banned', 'Registered', 'Guest'))->get();

        foreach($protectedRoles as $protectrole)
        {
            if($id == $protectrole->id)
            {
                return Redirect::action('AdminRoleController@index')->with('error', 'You cannot delete this protected role.');
            }
        }

        $role        = Role::findOrFail($id);
        $permissions = Permission::all();
        $temp        = array();

        foreach($permissions as $perm)
        {
            $section                      = strstr($perm->name, "_", true);
            if($perm->name == 'allperms') continue;
            $perms[$section][$perm->name] = $perm->display_name;
        }

        $rolePerms = Permission::join('permission_role', 'permissions.id', '=', 'permission_role.permission_id')->where('role_id', '=', $id)->select('permissions.*')->get();

        foreach($rolePerms as $p)
        {
            $temp[] = $p->name;
        }

        unset($rolePerms);

        $rolePerms = $temp;

        View::share('title', 'Edit Role');

        $this->layout->content = View::make('admin.rolesandperms.editrole', array('role' => $role, 'roleperms' => $rolePerms, 'perms' => $perms));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validation = Validator::make(
            array(
                'role_name' => Input::get('role_name')
            ),
            array(
                'role_name' => 'required|unique:roles,name,' . $id
            )
        );

        if($validation->fails())
        {
            return Redirect::action('AdminRoleController@edit', array($id))->withErrors($validation);
        }

        if(!Input::has('permsbf3') && !Input::has('permsbf4') && !Input::has('permswa'))
        {
            return Redirect::action('AdminRoleController@edit', array($id))
                        ->with('error', 'You need to choose at least 1 permission node to edit the ' . Input::get('role_name') . ' role.')
                        ->with('rolename', Input::get('role_name'));
        }

        $role       = Role::findOrFail($id);
        DB::table('permission_role')->where('role_id', $id)->delete();

        if(Input::has('permsbf3'))
        {
            foreach(Input::get('permsbf3') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        if(Input::has('permsbf4'))
        {
            foreach(Input::get('permsbf4') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        if(Input::has('permswa'))
        {
            foreach(Input::get('permswa') as $perm)
            {
                $setperms[] = Permission::where('name', $perm)->pluck('id');
            }
        }

        $role->name = Input::get('role_name');
        $role->save();

        $role->perms()->sync($setperms);

        Session::flash('success', Input::get('role_name') . ' Role Successfully Updated');
        Helper::createLogEntry('debug', 'UpdateRole', 'Updated ' . $role->name  . ' role');

        return Redirect::action('AdminRoleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // Block any attempt to delete the main roles needed by the web admin
        $protectedRoles = Role::whereIn('name', array('Site Administrator', 'Banned', 'Registered'))->get();

        foreach($protectedRoles as $protectrole)
        {
            if($id == $protectrole->id)
            {
                return Redirect::action('AdminRoleController@index')->with('error', 'You cannot delete this protected role.');
            }
        }

        // Get all the users that have the selected role
        $users = DB::table('assigned_roles')->where('role_id', $id)->get();

        // If we found users with that role lets move them to a different group as
        // this group will be deleted
        if($users)
        {
            // Loop through the users and update them
            foreach($users as $user)
            {
                DB::table('assigned_roles')->where('user_id', $user->id)->update(array('role_id' => 7));
            }
        }

        DB::table('permission_role')->where('role_id', $id)->delete();

        $role         = Role::findOrFail($id);
        $deleted_role = $role->name;
        $role->delete();

        Helper::createLogEntry('debug', 'DeleteRole', 'Deleted ' . $deleted_role . ' role');

        return Redirect::action('AdminRoleController@index')->with('success', 'Successfully deleted the ' . $deleted_role . ' role.');
    }
}

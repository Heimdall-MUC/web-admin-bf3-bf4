<?php

class SetupController extends Controller
{
    function shutdown()
    {
        // We need to remove the route to this controller
        $file = app_path() . '/routes.php';

        $routeFile = file_get_contents($file);

        // BEGIN code remove process
        $code = "Route::get('install', 'SetupController@install');";
        eval("\$code = \"$code\";");
        $routeFile = str_replace($code, '', $routeFile);
        // END code remove process

        file_put_contents($file, $routeFile);

        // Delete this file so it can never run again
        unlink(__FILE__);
    }

    public static function checkDependencyTablesExist()
    {
        // Required tables
        $tables = array(
            'adkats_bans',
            'adkats_commands',
            'adkats_infractions_global',
            'adkats_infractions_server',
            'adkats_records_debug',
            'adkats_records_main',
            'adkats_rolecommands',
            'adkats_roles',
            'adkats_settings',
            'adkats_users',
            'adkats_usersoldiers',
            'tbl_chatlog',
            'tbl_games',
            'tbl_mapstats',
            'tbl_playerdata',
            'tbl_server',
            'tbl_server_player',
            'tbl_sessions'
        );

        // Loop over the $tables and make sure they exist
        foreach($tables as $table)
        {
            // Check if the table is in the database
            if(!Schema::hasTable($table))
            {
                // If table doesn't exist, add it to the missing table list
                $missing_tables[] = $table;
            }
        }

        if(isset($missing_tables))
        {
            return $missing_tables;
        }

        return FALSE;
    }

    public static function isInstalled()
    {
        $tables = array(
            'users',
            'password_reminders',
            'roles',
            'assigned_roles',
            'permissions',
            'permission_role',
            'gamesettings',
            'log',
            'pages',
        );

        // Loop over the $tables and see if any of them exist
        foreach($tables as $table)
        {
            // Check if the table is in the database
            if(!Schema::hasTable($table))
            {
                // If table doesn't exist, add it to the found table list
                $found_tables[] = $table;
            }
        }

        if(isset($found_tables))
        {
            return FALSE;
        }

        return TRUE;
    }

    public function generate_app_key_fix()
    {
        $filePath = app_path() . '/config/app.php';

        $file = file_get_contents($filePath);

        $filemod = str_replace('YourKeyHere!!!!', str_random(32), $file);

        file_put_contents($filePath, $filemod);
    }

    public function install()
    {
        $check_version = FALSE;
        $check_mcrypt  = FALSE;
        $check_pdo     = FALSE;
        if(version_compare(PHP_VERSION, "5.4.0") >= 0) $check_version = TRUE;
        if(extension_loaded('mcrypt')) $check_mcrypt = TRUE;
        if(class_exists('PDO')) $check_pdo = TRUE;


        if(!$check_version)
        {
            die('You do not meet the required PHP version to run this application. You need at least 5.4, you are running ' . PHP_VERSION);
        }

        if(!$check_mcrypt)
        {
            die('Mcrypt extension is not loaded or installed. You need to have this installed and enabled before you can run this application.');
        }

        if(!$check_pdo)
        {
            die('The PDO class is missing or not enabled. This is required for database interactions');
        }

        $dependecy = self::checkDependencyTablesExist();

        // Did we not meet the schema dependency requirements?
        if($dependecy)
        {
            $response = 'Missing the following dependecy tables:<br/>';

            foreach($dependecy as $table)
            {
                $response .= $table . '<br/>';
            }

            die($response);
        }

        if(self::isInstalled())
        {
            // Migrate any new tables
            Artisan::call('migrate');

            $this->shutdown();

            return Redirect::to('/dashboard');
        }
        else
        {
            // Now we can setup the application

            // Generate the application key
            $this->generate_app_key_fix();

            // Migrate the tables
            Artisan::call('migrate');

            // Add data to the tables
            Artisan::call('db:seed');

            // Lets log you in now :-)
            Auth::loginUsingId(1);

            $this->shutdown();

            return Redirect::to('/dashboard');
        }
    }
}

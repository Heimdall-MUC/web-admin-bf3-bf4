<?php use Illuminate\Database\Eloquent\ModelNotFoundException;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::filter('site_admin', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Entrust::hasRole('Site Administrator'))
    {
        return View::make('error.401');
    }
});

Route::filter('bf3_admin', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Config::get('webadmin.bf3'))
    {
        return View::make('error.400');
    }

    if(!Entrust::can('viewbf3admin') && !Entrust::hasRole('Site Administrator'))
    {
        return View::make('error.401');
    }
});

Route::filter('bf4_admin', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Config::get('webadmin.bf4'))
    {
        return View::make('error.400');
    }

    if(!Entrust::can('viewbf4admin') && !Entrust::hasRole('Site Administrator'))
    {
        return View::make('error.401');
    }
});

Route::filter('bf3_manange_bans', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Entrust::can('editbf3ban') && !Entrust::hasRole('Site Administrator'))
    {
        return View::make('error.401');
    }
});

Route::filter('bf4_manange_bans', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Entrust::can('editbf4ban') && !Entrust::hasRole('Site Administrator'))
    {
        return View::make('error.401');
    }
});

Route::filter('manange_bf3', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Entrust::can('editbf3admin'))
    {
        return View::make('error.401');
    }
});

Route::filter('manange_bf4', function()
{
    if(Auth::guest())
    {
        Session::flash('redirect', URL::full());
        return Redirect::to('/user/login');
    }

    if(!Entrust::can('editbf4admin'))
    {
        return View::make('error.401');
    }
});

Route::filter('bf3_active', function()
{
    if(!Config::get('webadmin.bf3'))
    {
        return View::make('error.400');
    }
});

Route::filter('bf4_active', function()
{
    if(!Config::get('webadmin.bf4'))
    {
        return View::make('error.400');
    }
});

Route::group(array('prefix' => 'user'), function()
{
    Route::post('/create', 'UserController@store');
    Route::get('/login', 'UserController@login');
    Route::post('/login', 'UserController@do_login');
    Route::get('/confirm/{code}', 'UserController@confirm');
    Route::post('/forgot_password', 'UserController@do_forgot_password');
    Route::get('/reset_password/{token}', 'UserController@reset_password');
    Route::post('/reset_password', 'UserController@do_reset_password');
    Route::get('/logout', 'UserController@logout');
});

Route::group(array('before' => 'auth'), function()
{
    Route::get('user/profile/{username?}', 'UserController@show_profile');
    Route::get('/profile/edit', 'UserController@edit_profile');
    Route::post('/profile/edit', 'UserController@do_edit_profile');
});

// Begin API Route
Route::group(array('prefix' => 'api'), function()
{
    // General Battlefield Routes
    Route::group(array('prefix' => 'battlefield'), function()
    {
        Route::get('scoreboard/{id}/chat', function($id)
        {
            $isBF4 = (DB::table('tbl_server')->join('tbl_games', 'tbl_server.GameID', '=', 'tbl_games.GameID')->where('ServerID', $id)->pluck('Name') == 'BF4') ? TRUE : FALSE;
            return Response::json(Helper::getServerChatScoreboard($id, $isBF4));
        });

        Route::any('/reports', function()
        {
            return Response::json(Helper::getAdminReports());
        });

        Route::post('playerSearch/{name?}', function($name = FALSE)
        {
            return Response::json(Helper::searchForPlayer($name));
        });

        Route::post('/playerchat/{id}', function($id)
        {
            $chat = Helper::getPlayerChatLogs($id, 1000);

            return Response::json($chat);
        });

        Route::post('/forgive', 'PlayerController@issueForgive');
    });


    // Battlefield 3 Specific Routes
    Route::group(array('prefix' => 'battlefield/3', 'before' => 'bf3_active'), function()
    {
        Route::get('scoreboard/{id}', function($id = NULL)
        {
            $b = new App\Models\Battlefield\Bf3Scoreboard;
            return $b->initialize($id);
        });

        Route::post('scoreboard/{id}/admin', function($id = NULL)
        {
            $b = new App\Models\Battlefield\Bf3Admin;
            return $b->initialize($id);
        });

        Route::get('population', function()
        {
            return Response::json(Helper::fetchServerPopulation(Helper::fetchGameId('bf3')));
        });
    });

    Route::post('bf3/admin_reports', function()
    {
        return Response::json(array('status' => 'success'));
    });

    Route::group(array('prefix' => 'battlefield/4', 'before' => 'bf4_active'), function()
    {
        Route::get('scoreboard/{id}', function($id = NULL)
        {
            $b = new App\Models\Battlefield\Bf4Scoreboard;
            return $b->initialize($id);
        });

        Route::post('scoreboard/{id}/admin', function($id = NULL)
        {
            $b = new App\Models\Battlefield\Bf4Admin;
            return $b->initialize($id);
        });

        Route::get('population', function()
        {
            return Response::json(Helper::fetchServerPopulation(Helper::fetchGameId('bf4')));
        });
    });

    Route::group(array('prefix' => 'common'), function()
    {
        Route::get('premessage', function()
        {
            $messages = Helper::fetchPreMessages(Input::get('id'));

            if(!empty($messages))
            {
                $response = Helper::doMessage('success', NULL, $messages);
            }
            else
            {
                $response = Helper::doMessage('error', NULL, $messages);
            }

            return Response::json($response);

        });

        Route::post('/latest', function()
        {
            $response = Requests::get('https://bitbucket.org/api/1.0/repositories/adifferentkindllc/web-admin-bf3-bf4/branches-tags/');

            $data = json_decode($response->body, true);

            $version = last($data['tags']);

            $parsed = $version['name'];

            if($parsed == Config::get('webadmin.version'))
            {
                return Helper::doMessage('success', 'Running Latest Version', version_compare($parsed, Config::get('webadmin.version')));
            }
            else
            {
                return Helper::doMessage('error', 'You are running an outdated version. Current version is ' . $parsed . ', you\'re running ' . Config::get('webadmin.version'));
            }
        });

        Route::get('/repofeed', function()
        {
            return Response::json(Helper::fetchRepoActivity());
        });
    });
});
// End API Route

// Begin Page Route
Route::get('install', 'SetupController@install');
Route::get('/', function()
{
    return Redirect::to('/dashboard');
});

Route::get('dashboard', 'HomeController@index');

Route::group(array('prefix' => 'bf4', 'before' => 'bf4_active'), function()
{
    Route::get('scoreboard', 'HomeController@bf4scoreboard');
    Route::get('playerinfo/{id}', 'PlayerController@bf4info')->where('id', '[0-9]+');
    Route::get('playersearch', 'PlayerController@searchbf4');
    Route::post('playersearch', 'PlayerController@searchbf4');

    // Only users with the permission to view the battlefield 4 admin section are allowed
    Route::group(array('prefix' => 'admin', 'before' => 'bf4_admin'), function()
    {
        Route::get('chatlog', function()
        {
            return View::make('error.503');
        });
        Route::get('records', 'BattlefieldAdminController@showAdKatRecords');
        Route::get('banlist', array('as' => 'bf4banlist', 'uses' => 'AdminBanController@index'));
        Route::post('banlist', function()
        {
            return Response::json(Helper::fetchBanList('bf4'));
        });
        Route::get('banlist/{id}', array('as' => 'bf4banshow', 'uses' => 'AdminBanController@show'))->where('id', '[0-9]+');
        Route::post('banlist/{id}/update', array('as' => 'bf4banupdate', 'before' => 'bf4_manange_bans', 'uses' => 'AdminBanController@update'))->where('id', '[0-9]+');
        Route::post('banlist/create', array('as' => 'bf4bancreate', 'before' => 'bf4_manange_bans', 'uses' => 'AdminBanController@store'));
    });
});

Route::group(array('prefix' => 'bf3', 'before' => 'bf3_active'), function()
{
    Route::get('scoreboard', 'HomeController@bf3scoreboard');
    Route::get('playerinfo/{id}', 'PlayerController@bf3info')->where('id', '[0-9]+');
    Route::get('playersearch', 'PlayerController@searchbf3');
    Route::post('playersearch', 'PlayerController@searchbf3');
    Route::group(array('prefix' => 'admin', 'before' => 'bf3_admin'), function()
    {
        Route::get('chatlog', function()
        {
            return View::make('error.503');
        });
        Route::get('records', 'BattlefieldAdminController@showAdKatRecords');
        Route::get('banlist', array('as' => 'bf3banlist', 'uses' => 'AdminBanController@index'));
        Route::post('banlist', function()
        {
            return Response::json(Helper::fetchBanList('bf3'));
        });
        Route::get('banlist/{id}', array('as' => 'bf3banshow', 'uses' => 'AdminBanController@show'))->where('id', '[0-9]+');
        Route::post('banlist/{id}/update', array('as' => 'bf3banupdate','before' => 'bf3_manange_bans', 'uses' => 'AdminBanController@update'))->where('id', '[0-9]+');
        Route::post('banlist/create', array('as' => 'bf3bancreate','before' => 'bf3_manange_bans', 'uses' => 'AdminBanController@store'));
    });
});

Route::group(array('prefix' => 'admin', 'before' => 'site_admin'), function()
{
    Route::group(array('prefix' => 'role'), function()
    {
        Route::get('/', 'AdminRoleController@index');
        Route::get('/create', 'AdminRoleController@create');
        Route::get('/delete/{id}', 'AdminRoleController@destroy')->where('id', '[0-9]+');
        Route::get('/edit/{id}', 'AdminRoleController@edit')->where('id', '[0-9]+');
        Route::post('/update/{id}', 'AdminRoleController@update')->where('id', '[0-9]+');
        Route::post('/create', 'AdminRoleController@store');
    });

    Route::group(array('prefix' => 'users'), function()
    {
        Route::get('/', 'AdminUserController@index');
        Route::get('/show/{id}', 'AdminUserController@show')->where('id', '[0-9]+');
        Route::get('/edit/{id}', 'AdminUserController@edit')->where('id', '[0-9]+');
        Route::get('/delete/{id}', 'AdminUserController@destroy')->where('id', '[0-9]+');
        Route::post('/update/{id}', 'AdminUserController@update')->where('id', '[0-9]+');
    });

    Route::group(array('prefix' => 'servers'), function()
    {
        Route::get('/', 'AdminBattlefieldController@index');
        Route::get('/create/{id}', 'AdminBattlefieldController@create')->where('id', '[0-9]+');
        Route::get('/delete/{id}', 'AdminBattlefieldController@destroy')->where('id', '[0-9]+');
        Route::get('/edit/{id}', 'AdminBattlefieldController@edit')->where('id', '[0-9]+');
        Route::post('/update/{id}', 'AdminBattlefieldController@update')->where('id', '[0-9]+');
        Route::post('/format', function()
        {
            $stripd = trim(str_replace(explode(",", Input::get('split_string')), NULL, Servers::findOrFail(Input::get('serverid'))->ServerName));

            return Response::json(Helper::doMessage('success', $stripd));
        });
    });
});

// End Page Route

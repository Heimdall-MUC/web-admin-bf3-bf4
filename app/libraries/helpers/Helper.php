<?php namespace Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use DateTimeZone;
use DateTime;

class Helper
{
    /**
     * Function to send back a JSON response
     * @param  string $code
     * @param  string $message
     * @param  array  $data
     * @return array
     */
    public static function doMessage($code = '', $message = '', $data = array())
    {
        return array(
            'status'    => $code,
            'message'   => $message,
            'more'      => $data
        );
    }

    /**
     * Gets the game id from database
     * @param  string $game Game name
     * @return integer      Game ID
     */
    public static function fetchGameId($game)
    {
        return DB::table('tbl_games')->where('Name', '=', strtoupper($game))->pluck('GameID');
    }

    /**
     * Gets a players database id
     * @param  string $GameID
     * @param  string $EAGUID
     * @param  string $playername
     * @return PlayerID
     */
    public static function fetchPlayerId($GameID = '', $EAGUID = '', $playername = '')
    {
        $player = DB::table('tbl_playerdata')->where('GameID', $GameID)->where('EAGUID', $EAGUID)->first();

        if($player)
        {
            return (int) $player->PlayerID;
        }
        else
        {
            return DB::table('tbl_playerdata')->insertGetId(
                array('GameID' => $GameID, 'EAGUID' => $EAGUID, 'SoldierName' => $playername)
            );
        }
    }

    /**
     * Get either a Gravatar URL or complete image tag for a specified email address.
     *
     * @param string $email The email address
     * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
     * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
     * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
     * @param boole $img True to return a complete IMG tag False for just the URL
     * @param array $atts Optional, additional key/value attributes to include in the IMG tag
     * @return String containing either just a URL or a complete image tag
     * @source http://gravatar.com/site/implement/images/php/
     */
    public static function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
    {
        $url = '//www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    /**
     * Convert seconds to something humans can read
     * @param  integer $ss
     * @param  boolean $round  If this is used for a game round
     * @return string
     */
    public static function seconds2human($ss, $round, $precision = false)
    {
        $s = $ss % 60;
        $m = floor(($ss % 3600) / 60);
        $h = floor(($ss % 86400) / 3600);
        $d = floor(($ss % 2592000) / 86400);
        //$M = floor($ss / 2592000);

        if($precision == true)
        {
            return "{$d}d {$h}h {$m}m {$s}s";
        }
        else
        {
            return ($round == true ? "{$h}h {$m}m {$s}s" : "{$d}d {$h}h {$m}m");
        }
    }

    /**
     * Gets the pre-set messages from the database.
     * @param  integer $server
     * @return array
     */
    public static function fetchPreMessages($server = 0)
    {
        if(Schema::hasTable('adkats_settings'))
        {
            $messages = DB::table('adkats_settings')->where('server_id', $server)->where('setting_name', 'Pre-Message List')->get();

            if($messages)
                return explode('|', urldecode(rawurldecode($messages[0]->setting_value)));

            return self::doMessage('error', 'No results founds');
        }
        else
        {
            return self::doMessage('error', "Table adkats_settings doesn't exist in the database. Please run the AdKats setup script.");
        }
    }

    /**
     * Gets the squads name by id
     * @param  integer $squad_id
     * @return string
     */
    public static function squadName($squad_id)
    {
        switch((int) $squad_id)
        {
            case 0:  $sq = 'None'; break;
            case 1:  $sq = 'Alpha'; break;
            case 2:  $sq = 'Bravo'; break;
            case 3:  $sq = 'Charlie'; break;
            case 4:  $sq = 'Delta'; break;
            case 5:  $sq = 'Echo'; break;
            case 6:  $sq = 'Foxtrot'; break;
            case 7:  $sq = 'Golf'; break;
            case 8:  $sq = 'Hotel'; break;
            case 9:  $sq = 'India'; break;
            case 10: $sq = 'Juliet'; break;
            case 11: $sq = 'Kilo'; break;
            case 12: $sq = 'Lima'; break;
            case 13: $sq = 'Mike'; break;
            case 14: $sq = 'November'; break;
            case 15: $sq = 'Oscar'; break;
            case 16: $sq = 'Papa'; break;
            case 17: $sq = 'Quebec'; break;
            case 18: $sq = 'Romeo'; break;
            case 19: $sq = 'Sierra'; break;
            case 20: $sq = 'Tango'; break;
            case 21: $sq = 'Uniform'; break;
            case 22: $sq = 'Victor'; break;
            case 23: $sq = 'Whiskey'; break;
            case 24: $sq = 'Xray'; break;
            case 25: $sq = 'Yankee'; break;
            case 26: $sq = 'Zulu'; break;
            case 27: $sq = 'Haggard'; break;
            case 28: $sq = 'Sweetwater'; break;
            case 29: $sq = 'Preston'; break;
            case 30: $sq = 'Redford'; break;
            case 31: $sq = 'Faith'; break;
            case 32: $sq = 'Celeste'; break;
            default: $sq = NULL; break;
        }

        // Returns the squad name
        return $sq;
    }

    /**
     * Gets port from IPv4 address
     * @param string $ip IP address to get port from
     */
    public static function getPortNum($ip)
    {
        if ($c=preg_match_all ("/.*?(:)(\\d+)/is", $ip, $matches))
        {
            if(strlen((string) $matches[2][0]) == 5)
            {
                return $matches[2][0];
            }
            else
            {
                return FALSE;
            }
        }
    }

    /**
     * Returns only IPv4 address and strips all other data
     * @param string $ip IP Address
     */
    public static function getIPv4Addr($ip)
    {
        // Check if $ip is a FQDN
        if($c = preg_match_all("/((?:[a-z][a-z\\.\\d\\-]+)\\.(?:[a-z][a-z\\-]+))(?![\\w\\.])/is", $ip, $matches))
        {
            if(filter_var(gethostbyname($matches[1][0]), FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
            {
                return gethostbyname($matches[1][0]);
            }
            else
            {
                return FALSE;
            }
        }
        // Check if $ip is an actual ip address
        elseif($c = preg_match_all("/((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])/is", $ip, $matches))
        {
            if(filter_var($matches[1][0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
            {
                return $matches[1][0];
            }
            else
            {
                return FALSE;
            }
        }
    }

    /**
     * Removes any unwanted text from the server name
     *
     * For example, the following string
     *
     *      =ADK= EU #3 | Conquest Large Rotation | ADKGamers.com |
     *
     * would be reduced to this
     *
     *      EU Conquest Large Rotation
     *
     * based on this comma delimited list
     *
     *     =ADK=,24/7,#3,ADKGamers.com,|
     *
     * @param  integer $id Server ID
     * @return string
     */
    public static function formatServerName($id = 0)
    {
        $server = DB::table('tbl_server')->where('ServerID', $id)->select('ServerName')->remember(5)->first();
        $gameset = DB::table('gamesettings')->where('server_id', $id)->select('settings')->remember(5)->first();

        if($gameset)
        {
            // Return back formated server name
            return preg_replace('/\s\s+/', ' ', trim(str_replace(explode(',', $gameset->settings), '', $server->ServerName)));
        }
        else
        {
            // Return back server name
            return $server->ServerName;
        }
    }

    /**
     * Gets the server population for all servers
     * @param  integer $GameID
     * @return array
     */
    public static function fetchServerPopulation($GameID = 0)
    {
        $server = DB::table('tbl_server')->where('GameID', $GameID)->where('ConnectionState', 'on')->orderBy(Config::get('webadmin.server_list_order'), 'asc')->get();

        if($server)
        {
            foreach($server as $serv)
            {
                $feed['population'][] = array(
                    'id'    => $serv->ServerID,
                    'name'  => self::formatServerName($serv->ServerID),
                    'max'   => $serv->maxSlots,
                    'cur'   => $serv->usedSlots
                );
            }

            $totalUsed = DB::table('tbl_server')->where('GameID', $GameID)->where('ConnectionState', 'on')->sum('usedSlots');
            $totalMax  = DB::table('tbl_server')->where('GameID', $GameID)->where('ConnectionState', 'on')->sum('maxSlots');

            $feed['poptotal'] = array(
                'totalUsed' => (int)$totalUsed,
                'totalMax'  => (int)$totalMax,
                'percent'   => self::calculatePercent($totalUsed, $totalMax)
            );

            return self::doMessage('success', NULL, $feed);
        }

        return self::doMessage('error', 'No servers could be found or are not enabled');
    }

    /**
     * Return a percentage from two numbers
     * @param  integer  $val1
     * @param  integer  $val2
     * @param  integer $precision
     * @return integer
     */
    public static function calculatePercent($val1, $val2, $precision = 1)
    {
        if($val1 != 0 && $val2 != 0)
        {
            return round(($val1 / $val2) * 100, $precision);
        }
        else
        {
            return 0;
        }
    }

    /**
     * Returns the date/time difference between two dates
     * @param  integer $start UNIX Timestamp
     * @param  integer $end   UNIX Timestamp
     * @return object         Returns the DateInterval object
     */
    public static function calculateDateTimeDifference($start, $end)
    {
        if(is_string($start)) $start = strtotime($start);
        if(is_string($end)) $end = strtotime($end);
        try
        {
            $startDate = new DateTime(date('Y-m-d H:i:s', $start));
            $endDate   = new DateTime(date('Y-m-d H:i:s', $end));

            return $startDate->diff($endDate);
        }
        catch(Exception $e)
        {
            return 'Failed to parse time string';
        }
    }

    /**
     * Formats the ban timespan
     *
     * Function converted from AdKats source to something PHP can use
     * @param  object $datetimediff DateTimeDiff Object
     * @return string
     */
    public static function formatBanTimespan($difference)
    {
        $year           = $difference->y;
        $month          = $difference->m;
        $hours          = $difference->h;
        $minutes        = $difference->i;
        $seconds        = $difference->s;
        $days          = $difference->days;
        $formatedTime   = '';

        // Only show day if greater than 1 day
        if ($days > 0)
        {
            if($year > 0)
            {
                $formatedTime .= $year . "y ";
            }

            if($month > 0)
            {
                $formatedTime .= $month . "m ";
            }

            if($year > 0 || $month > 0)
            {
                // Show day count
                $formatedTime .= $difference->d . "d ";
            }
            else
            {
                // Show day count
                $formatedTime .= $days . "d ";
            }
        }

        // Only show more information if less than 35 days
        if ($days < 35)
        {
            // Only show hours if days exist, or hours > 0
            if ($hours > 0 || $days > 0)
            {
                //Show hour count
                $formatedTime .= $hours . "h ";
            }

            // Only show more infomation if less than 1 day
            if ($days < 1)
            {
                // Only show minutes if Hours exist, or minutes > 0
                if ($minutes > 0 || $hours > 0)
                {
                    //Show hour count
                    $formatedTime .= $minutes . "m ";
                }

                // Only show more infomation if less than 1 hour
                if ($hours < 1)
                {
                    // Only show seconds if minutes exist, or seconds > 0
                    if ($seconds > 0 || $minutes > 0)
                    {
                        // Show hour count
                        $formatedTime .= $seconds . "s ";
                    }
                }
            }
        }

        $finaltime = trim(($formatedTime == '' ? '::' : $formatedTime));

        return $finaltime;
    }

    /**
     * Finds the remaining time from the future end date and now
     * @param  integer $enddate UNIX timestamp
     * @return object           Returns the DateInterval object
     */
    public static function getRemainingTime($enddate)
    {
        $now = time();

        if(is_string($enddate)) $enddate = strtotime($enddate);

        $date = new DateTime(date('Y-m-d H:i:s', $enddate));

        if($enddate <= $now) $date = new DateTime(date('Y-m-d H:i:s', $now));

        $nowdate = new DateTime(date('Y-m-d H:i:s', $now));
        return $date->diff($nowdate);
    }

    /**
     * Converts minutes to something humans can read
     * @param  integer $minutes
     * @return string
     */
    public static function minutesToHuman($minutes = 0)
    {
        $d = floor ($minutes / 1440);
        $h = floor (($minutes - $d * 1440) / 60);
        $m = $minutes - ($d * 1440) - ($h * 60);

        $formated = '';

        if($minutes <= 60)
        {
            $formated .= $minutes . "m";
        }
        else
        {
            if($d > 0)
            {
                $formated .= $d . "d ";
            }

            $formated .= "{$h}h {$m}m";
        }

        return $formated;
    }

    /**
     * Sets the resource URL for all images, js, css
     * @return string
     */
    public static function cdn()
    {
        // Get the cached resource url if not expired
        if(Cache::has('resource_url'))
        {
            return Cache::get('resource_url');
        }
        else
        {
            // Check if the resource is online
            $socket = fsockopen("www.adkwebadmin.com", 80, $errno, $errstr, 30);

            if($socket)
            {
                $url = "http://adkwebadmin.com/resource/";
            }
            else
            {
                $url = "http://adkstatic.info/resource/";
            }

            // Update the cache for the resource url for 30 minutes
            Cache::put('resource_url', $url, 30);

            return $url;
        }
    }

    /**
     * Gets the basic player info from the playerdata table
     * @param  integer $id Player ID
     * @return array
     */
    public static function getBasicPlayerInfo($id = 0, $game = NULL)
    {
        $player = DB::table('tbl_playerdata')->where('PlayerID', $id)->where('GameID', self::fetchGameId($game))->first();

        // Check if player was found
        if($player)
        {
            return self::doMessage('success', 'Player found', $player);
        }
        else
        {
            return self::doMessage('error', 'No player found');
        }
    }

    /**
     * Gets all the players bans
     * @param  integer $id Player ID
     * @return array
     */
    public static function getPlayerBans($id = 0)
    {
        // Check if player has any bans
        $banCheck = DB::table('adkats_records_main')->where('target_id', $id)->whereIn('command_action', array(7, 8))->count();

        if($banCheck == 0)
            return self::doMessage('error', 'No ban records found');

        // Update expired bans
        DB::update("UPDATE `adkats_bans` SET `ban_status` = 'Expired' WHERE `ban_endTime` <= UTC_TIMESTAMP() AND `ban_status` = 'Active'");

        $currentBan = DB::table('adkats_bans')->where('player_id', $id)
                        ->join('adkats_records_main', 'adkats_bans.latest_record_id', '=', 'adkats_records_main.record_id')
                        ->select('ban_id', 'ban_status', 'ban_startTime', 'ban_endTime', 'command_action', 'target_name', 'source_name', 'record_message', 'latest_record_id', 'ban_notes')->first();

        if($currentBan)
        {
            // Append to the current ban object and added javascript equilvents for ban start/end time
            $currentBan->ban_startTime_js = strtotime($currentBan->ban_startTime) * 1000;
            $currentBan->ban_endTime_js   = strtotime($currentBan->ban_endTime) * 1000;
        }

        $previousBan = DB::table('adkats_records_main')
                         ->where('target_id', $id)
                         ->whereIn('command_action', array(7, 8))
                         ->orderBy('record_id', 'desc')->get();

        return self::doMessage('success', 'Ban records found', (object) array(
                        'currentBan'   => $currentBan,
                        'previousBans' => $previousBan
                    ));
    }

    /**
     * Gets a players infraction history
     * @param  integer $id Player ID
     * @return array
     */
    public static function getPlayerInfractions($id = 0)
    {
        // Get server specific infraction points
        $server = DB::table('adkats_infractions_server')
                    ->where('player_id', $id)
                    ->join('tbl_server', 'adkats_infractions_server.server_id', '=', 'tbl_server.ServerID')
                    ->select('player_id', 'punish_points', 'forgive_points', 'total_points', 'ServerName')->get();

        // Get over infraction points
        $overall = DB::table('adkats_infractions_global')->where('player_id', $id)->get();

        return self::doMessage('success', NULL, array(
            'overallInfactions'    => head($overall),
            'perServerInfractions' => $server
        ));
    }

    /**
     * Gets a players entire history from the beginning of time
     * @param  integer $id Player ID
     * @return array
     */
    public static function getPlayerRecordHistory($id = 0)
    {
        $history = DB::table('adkats_records_main')
                     ->where('target_id', $id)
                     ->orWhere('source_id', $id)
                     ->select('record_id', 'command_action', 'target_name', 'source_name', 'record_message', 'record_time', 'adkats_web AS web', 'server_id', 'target_id', 'source_id')
                     ->orderBy('record_id', 'desc')->get();

        if($history)
        {
            return self::doMessage('success', 'Loaded history', $history);
        }
        else
        {
            return self::doMessage('error', 'No history found');
        }
    }

    public static function getPlayerStats($id = 0)
    {
        $stats = DB::table('tbl_server_player')
                   ->where('PlayerID', $id)
                   ->join('tbl_playerstats', 'tbl_server_player.StatsID', '=', 'tbl_playerstats.StatsID')
                   ->select('tbl_playerstats.*', 'tbl_server_player.ServerID')->get();

        if($stats)
        {
            return self::doMessage('success', 'Found stats', $stats);
        }
        else
        {
            return self::doMessage('error', 'Did not find stats for player');
        }
    }

    /**
     * Builds a response for the entire players history
     * @param  integer $id Player ID
     * @return array
     */
    public static function buildPlayerProfile($id = 0, $game = NULL, $skipchat = TRUE)
    {
        if($id === 0)
            return self::doMessage('error', 'Player ID must be provided.');

        // Get the specified player basic infomation
        $playerInfo         = self::getBasicPlayerInfo($id, $game);

        if($playerInfo['status'] == 'error') return $playerInfo;

        // Get the specified player ban history if any
        $playerBan          = self::getPlayerBans($id);
        // Get the specified player infraction history if any
        $playerInfraction   = self::getPlayerInfractions($id);
        // Get the specified player record history if any
        $playerHistory      = self::getPlayerRecordHistory($id);

        if(!$skipchat)
        {
            // Get the specified player chat history if any
            $playerChatHistory  = self::getPlayerChatLogs($playerInfo['more']->SoldierName);
            $results = (object) array(
                'player_info'       => $playerInfo['more'],
                'player_ban'        => $playerBan['more'],
                'player_infraction' => $playerInfraction['more'],
                'player_chat'       => $playerChatHistory['more'],
                'player_records'    => $playerHistory['more']
            );
        }
        else
        {
            $results = (object) array(
                'player_info'       => $playerInfo['more'],
                'player_ban'        => $playerBan['more'],
                'player_infraction' => $playerInfraction['more'],
                'player_records'    => $playerHistory['more']
            );
        }

        // Is the current user logged in and has the permission to view the player IP Address
        if(!\Auth::check())
        {
            $ip = $results->player_info->IP_Address;

            $results->player_info->IP_Address = (is_null($ip) ? $ip : strtoupper('classified'));
        }

        return $results;
    }

    /**
     * Gets a players chat log history
     * @param  string  $player  Player Name
     * @param  integer $limit How many to pull
     * @return array
     */
    public static function getPlayerChatLogs($player = '', $limit = 500)
    {
        // If limit is less than 1 set it back to 500 results
        if($limit < 1)
            $limit = 500;

        if(is_null($player)) return self::doMessage('error', 'No player id provided.');

        $chat = DB::table('tbl_chatlog')
                  ->where('logPlayerID', $player)
                  ->join('tbl_server', 'tbl_chatlog.ServerID', '=', 'tbl_server.ServerID')
                  ->select('ID', 'logDate', 'logSubset', 'logMessage', 'ServerName', 'logSoldierName')
                  ->take($limit)
                  ->orderBy('ID', 'desc')->get();

        foreach($chat as $key => $v)
        {
            $chat[$key]->logDate = self::convertUTCToLocal($v->logDate);
        }

        return self::doMessage(($chat ? 'success' : 'error'), NULL, $chat);
    }

    /**
     * Search for a player
     * @param  string $player Player Name
     * @return array
     */
    public static function searchForPlayer($player = '')
    {
        if($player == FALSE)
            return self::doMessage('error', 'No Player Name, EAGUID or IP Address was provided.');

        // Remove whitespace from input
        $player = trim($player);

        // If player name equals to Server then return an error
        // as we dont allow that
        if(ucfirst($player) == 'Server')
            return self::doMessage('error', 'You cannot do a search on Server. Thats unthinkable. Please try again.');

        $players = DB::table('tbl_playerdata')
                     ->join('tbl_games', 'tbl_games.GameID', '=', 'tbl_playerdata.GameID')
                     ->select('PlayerID', 'SoldierName', 'EAGUID', 'PBGUID', 'Name AS Game');

        if(\Input::has('game'))
        {
            $players->where('tbl_playerdata.GameID', '=', self::fetchGameId(strtoupper(\Input::get('game'))));
        }

        // Check if request search was an EAGUID
        if(preg_match("/(EA)(_)/is", $player))
        {
            if(strlen($player) == 35)
            {
                $players = $players->where('EAGUID', $player)->get();
            }
            else
            {
              return self::doMessage('error', 'This is not a valid EAGUID.');
            }
        }
        // Check if request search was on IP
        elseif(preg_match("/((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))(?![\\d])/is", $player))
        {
            $players = $players->where('IP_Address', $player)->get();
        }
        // Check if request search was on player name
        elseif(preg_match('/^[A-Za-z0-9_ -]+$/', $player))
        {
            $players = $players->where('SoldierName', 'LIKE', "%" . $player . "%")->get();
        }
        // If no matches return an error
        else
        {
            return self::doMessage('error', 'Your request did not fall within current guidelines. Only alphanum, IP, EAGUID searches allowed.');
        }

        return self::doMessage(($players ? 'success' : 'error'), ($players ? 'Found' : 'Not Found'), $players);
    }

    /**
     * Gets session history for player
     * @param  string $player Player Name
     * @return array
     */
    public static function getPlayerSessions($playerid = '')
    {
        $sessions = DB::table('tbl_playerdata')
                      ->where('tbl_playerdata.PlayerID', $playerid)
                      ->join('tbl_server_player', 'tbl_playerdata.PlayerID', '=', 'tbl_server_player.PlayerID')
                      ->join('tbl_sessions', 'tbl_server_player.StatsID', '=', 'tbl_sessions.StatsID')
                      ->join('tbl_server', 'tbl_server_player.ServerID', '=', 'tbl_server.ServerID')
                      ->select(DB::raw('tbl_sessions.*, tbl_server.ServerName'))
                      ->orderBy('tbl_sessions.SessionID', 'desc')
                      ->get();

        if(!$sessions)
            return self::doMessage('error', 'No sessions found');

        // Loop through results to pretify each record
        foreach($sessions as $session)
        {
            $data[] = (object) array(
                'SessionID'     => $session->SessionID,
                'StatsID'       => $session->StatsID,
                'StartTime'     => $session->StartTime,
                'EndTime'       => $session->EndTime,
                'RoundCount'    => $session->RoundCount,
                'Playtime'      => self::seconds2human($session->Playtime, false, true),
                'ServerName'    => $session->ServerName
            );
        }

        return self::doMessage('success', NULL, $data);
    }

    /**
     * Get the most recent admin reports
     * @return array
     */
    public static function getAdminReports()
    {
        $data = array();
        $reports = \Records::whereIn('command_action', array(18, 20))
                          ->where('record_time', '>', DB::raw('UTC_TIMESTAMP - INTERVAL 15 MINUTE'))
                          ->orderBy('record_id', 'desc');

        if(Input::has('lastRecordId'))
            $reports = $reports->where('record_id', '>', Input::get('lastRecordId'));

        $result = $reports->get();

        if($result->count() < 1)
            return self::doMessage('error', 'No reports returned');

        foreach($result as $report)
        {
            $data[] = array(
                'record_id'   => (int) $report->record_id,
                'server'      => self::formatServerName($report->server_id),
                'target_name' => $report->target_name,
                'target_id'   => (int) $report->target_id,
                'source_name' => $report->source_name,
                'source_id'   => (int) $report->source_id,
                'message'     => $report->record_message,
                'date'        => date('c', strtotime($report->record_time)),
                'server_id'   => (int) $report->server_id
            );
        }

        return self::doMessage('success', NULL, $data);
    }

    /**
     * Chatlog searching
     * @return array
     */
    public static function searchChatLogs()
    {
        $fromDate = Input::get('fromDate', NULL);
        $toDate   = Input::get('toDate', NULL);
        $server   = Input::get('server', NULL);
        $search   = urldecode(Input::get('query', NULL));
        $order    = Input::get('order', 'desc');

        // Check if the order by exists
        if(Input::has('order'))
        {
            // Check if $order matches either DESC or ASC. If it doesn't match then set $order to DESC
            if(!in_array($order, array('desc', 'asc')))
            {
                $order = 'desc';
            }
        }

        // Begin building the query
        $query = DB::table('tbl_chatlog')->leftJoin('tbl_server', 'tbl_chatlog.ServerID', '=', 'tbl_server.ServerID')->select('ID', 'logDate', 'logSubset','logSoldierName', 'logMessage', 'ServerName');

        if(!is_null($server) && is_numeric($server) && Input::has('server'))
        {
            $query = $query->where('tbl_chatlog.ServerID', $server);
        }

        // If both from and to date have been filled out create the where clause for it
        if(!is_null($fromDate) && !is_null($toDate) && Input::has('fromDate') && Input::has('toDate'))
        {
            $query = $query->where('logDate', '>=', urldecode($fromDate))->where('logDate', '<=', urldecode($toDate));
        }

        // If requested search has players then search for those players only
        if(Input::has('players'))
        {
            $query = $query->where(function($query)
            {
                $players  = explode(',', Input::get('players', NULL));
                // Loop though the list of players
                foreach($players as $player)
                {
                    $query = $query->orWhere('logSoldierName', '=', $player);
                }
            });
        }

        // Search for a specific string in the message column if user is looking for something
        if(!is_null($search) && !empty($search) && Input::has('query'))
        {
            $query = $query->whereRaw("`logMessage` LIKE ?", array("%".$search."%"));
        }

        // Order the results in either DESC or ASC order then fetch the results
        $query = $query->orderBy('logDate', 'desc')->orderBy('ID', $order)->paginate(30)->toArray();

        // Return the results back to the client
        return self::doMessage( (!empty($query['data']) ? 'success' : 'error'), (!empty($query['data']) ? 'Results found' : 'No results found'), $query);
    }

    /**
     * Get the current page title
     * @param  string $page
     * @param  string $section
     * @return string
     */
    public static function getPageTitle()
    {
        if(str_contains(\Request::path(), 'admin'))
        {
            if(in_array(\Request::segment(1), array('bf3', 'bf4')))
            {
                return DB::table('pages')->where('uri', '=', \Request::segment(1) . '/admin/' . \Request::segment(3))->pluck('title');
            }
            else
            {
                return DB::table('pages')->where('uri', '=', 'admin/' . \Request::segment(2))->pluck('title');
            }
        }
        else
        {
            if(\Request::segment(3))
            {
                return DB::table('pages')->where('uri', '=', \Request::segment(1) . '/' . \Request::segment(2))->pluck('title');
            }
            else
            {
                return DB::table('pages')->where('uri', '=', \Request::path())->pluck('title');
            }

        }
    }

    /**
     * Gets the server chat for the live scoreboard
     * @param  integer $serverid Gameserver Database ID
     * @return array
     */
    public static function getServerChatScoreboard($serverid, $BF4 = FALSE)
    {
        $chat = DB::table('tbl_chatlog')->where('ServerID', $serverid)->where('logSoldierName', '<>', 'Server')->orderBy('ID', 'desc');

        if(Input::has('previd'))
        {
            $chat = $chat->where('ID', '>', Input::get('previd'))->get();
        }
        else
        {
            $chat = $chat->take(30)->get();
        }

        if($chat)
        {

            // Used only for Battlefield 4
            if($BF4)
            {
                $comoRoseCode = array(
                    'ID_CHAT_REQUEST_MEDIC'     => 'Need a Medic',
                    'ID_CHAT_REQUEST_AMMO'      => 'Need Ammo',
                    'ID_CHAT_THANKS'            => 'Thanks',
                    'ID_CHAT_REQUEST_RIDE'      => 'Need a ride!',
                    'ID_CHAT_AFFIRMATIVE'       => 'Affirmative',
                    'ID_CHAT_GOGOGO'            => 'Go! Go! Go!',
                    'ID_CHAT_SORRY'             => 'Sorry!',
                    'ID_CHAT_ATTACK/DEFEND'     => 'Attack/Defend',
                    'ID_CHAT_REQUEST_ORDER'     => 'Requesting New Orders! Over.',
                    'ID_CHAT_GET_IN'            => 'Hop in.',
                    'ID_CHAT_NEGATIVE'          => 'Negative',
                    'ID_CHAT_GET_OUT'           => 'Bail out!',
                    'ID_CHAT_REQUEST_REPAIRS'   => 'Requesting Repairs! Over.'
                );
            }

            for($i=0; $i < count($chat); $i++)
            {
                if($BF4 && array_key_exists($chat[$i]->logMessage, $comoRoseCode) !== FALSE)
                {
                    $chat[$i]->logMessage = 'ComoRose: ' . $comoRoseCode[$chat[$i]->logMessage];
                }
                $chat[$i]->logDate    = self::convertUTCToLocal($chat[$i]->logDate);
            }

            return self::doMessage('success', NULL, $chat);
        }

        return self::doMessage('error', 'No new chat has been detected.', array('previd' => (int)Input::get('previd')));
    }

    /**
     * Generate a timezone list
     * @source http://stackoverflow.com/a/17355238
     * @return array
     */
    public static function generate_timezone_list()
    {
        static $regions = array(
            DateTimeZone::AFRICA,
            DateTimeZone::AMERICA,
            DateTimeZone::ANTARCTICA,
            DateTimeZone::ASIA,
            DateTimeZone::ATLANTIC,
            DateTimeZone::AUSTRALIA,
            DateTimeZone::EUROPE,
            DateTimeZone::INDIAN,
            DateTimeZone::PACIFIC,
            DateTimeZone::UTC
        );

        $timezones = array();
        foreach( $regions as $region )
        {
            $timezones = array_merge( $timezones, DateTimeZone::listIdentifiers( $region ) );
        }

        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
        }

        // sort timezone by offset
        asort($timezone_offsets);

        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone";
        }

        return $timezone_list;
    }

    /**
     * Gets the numeric value for the AdKats command
     * @param  string $key Name of command
     * @return integer Command ID
     */
    public static function getAdkatsCommandId($key)
    {
        $adkats_commands = array(
            array('command_id' => '1','command_key' => 'command_confirm','command_name' => 'Confirm Command'),
            array('command_id' => '2','command_key' => 'command_cancel','command_name' => 'Cancel Command'),
            array('command_id' => '3','command_key' => 'player_kill','command_name' => 'Kill Player'),
            array('command_id' => '4','command_key' => 'player_kill_lowpop','command_name' => 'Kill Player (Low Population)'),
            array('command_id' => '5','command_key' => 'player_kill_repeat','command_name' => 'Kill Player (Repeat Kill)'),
            array('command_id' => '6','command_key' => 'player_kick','command_name' => 'Kick Player'),
            array('command_id' => '7','command_key' => 'player_ban_temp','command_name' => 'Temp-Ban Player'),
            array('command_id' => '8','command_key' => 'player_ban_perm','command_name' => 'Permaban Player'),
            array('command_id' => '9','command_key' => 'player_punish','command_name' => 'Punish Player'),
            array('command_id' => '10','command_key' => 'player_forgive','command_name' => 'Forgive Player'),
            array('command_id' => '11','command_key' => 'player_mute','command_name' => 'Mute Player'),
            array('command_id' => '12','command_key' => 'player_join','command_name' => 'Join Player'),
            array('command_id' => '13','command_key' => 'player_roundwhitelist','command_name' => 'Round Whitelist Player'),
            array('command_id' => '14','command_key' => 'player_move','command_name' => 'On-Death Move Player'),
            array('command_id' => '15','command_key' => 'player_fmove','command_name' => 'Force Move Player'),
            array('command_id' => '16','command_key' => 'self_teamswap','command_name' => 'Teamswap Self'),
            array('command_id' => '17','command_key' => 'self_kill','command_name' => 'Kill Self'),
            array('command_id' => '18','command_key' => 'player_report','command_name' => 'Report Player'),
            array('command_id' => '19','command_key' => 'player_report_confirm','command_name' => 'Report Player (Confirmed)'),
            array('command_id' => '20','command_key' => 'player_calladmin','command_name' => 'Call Admin on Player'),
            array('command_id' => '21','command_key' => 'admin_say','command_name' => 'Admin Say'),
            array('command_id' => '22','command_key' => 'player_say','command_name' => 'Player Say'),
            array('command_id' => '23','command_key' => 'admin_yell','command_name' => 'Admin Yell'),
            array('command_id' => '24','command_key' => 'player_yell','command_name' => 'Player Yell'),
            array('command_id' => '25','command_key' => 'admin_tell','command_name' => 'Admin Tell'),
            array('command_id' => '26','command_key' => 'player_tell','command_name' => 'Player Tell'),
            array('command_id' => '27','command_key' => 'self_whatis','command_name' => 'What Is'),
            array('command_id' => '28','command_key' => 'self_voip','command_name' => 'VOIP'),
            array('command_id' => '29','command_key' => 'self_rules','command_name' => 'Request Rules'),
            array('command_id' => '30','command_key' => 'round_restart','command_name' => 'Restart Current Round'),
            array('command_id' => '31','command_key' => 'round_next','command_name' => 'Run Next Round'),
            array('command_id' => '32','command_key' => 'round_end','command_name' => 'End Current Round'),
            array('command_id' => '33','command_key' => 'server_nuke','command_name' => 'Server Nuke'),
            array('command_id' => '34','command_key' => 'server_kickall','command_name' => 'Kick All Guests'),
            array('command_id' => '35','command_key' => 'adkats_exception','command_name' => 'Logged Exception'),
            array('command_id' => '36','command_key' => 'banenforcer_enforce','command_name' => 'Enforce Active Ban')
        );

        foreach($adkats_commands as $command)
        {
            if($key == $command['command_key']) return $command['command_id'];
        }
    }

    /**
     * Gets the command name for the AdKats command
     * @param  string $id Command ID
     * @return integer Command Name
     */
    public static function getAdkatsCommandName($id)
    {
        $adkats_commands = array(
            array('command_id' => '1','command_key' => 'command_confirm','command_name' => 'Confirm Command'),
            array('command_id' => '2','command_key' => 'command_cancel','command_name' => 'Cancel Command'),
            array('command_id' => '3','command_key' => 'player_kill','command_name' => 'Kill Player'),
            array('command_id' => '4','command_key' => 'player_kill_lowpop','command_name' => 'Kill Player (Low Population)'),
            array('command_id' => '5','command_key' => 'player_kill_repeat','command_name' => 'Kill Player (Repeat Kill)'),
            array('command_id' => '6','command_key' => 'player_kick','command_name' => 'Kick Player'),
            array('command_id' => '7','command_key' => 'player_ban_temp','command_name' => 'Temp-Ban Player'),
            array('command_id' => '8','command_key' => 'player_ban_perm','command_name' => 'Permaban Player'),
            array('command_id' => '9','command_key' => 'player_punish','command_name' => 'Punish Player'),
            array('command_id' => '10','command_key' => 'player_forgive','command_name' => 'Forgive Player'),
            array('command_id' => '11','command_key' => 'player_mute','command_name' => 'Mute Player'),
            array('command_id' => '12','command_key' => 'player_join','command_name' => 'Join Player'),
            array('command_id' => '13','command_key' => 'player_roundwhitelist','command_name' => 'Round Whitelist Player'),
            array('command_id' => '14','command_key' => 'player_move','command_name' => 'On-Death Move Player'),
            array('command_id' => '15','command_key' => 'player_fmove','command_name' => 'Force Move Player'),
            array('command_id' => '16','command_key' => 'self_teamswap','command_name' => 'Teamswap Self'),
            array('command_id' => '17','command_key' => 'self_kill','command_name' => 'Kill Self'),
            array('command_id' => '18','command_key' => 'player_report','command_name' => 'Report Player'),
            array('command_id' => '19','command_key' => 'player_report_confirm','command_name' => 'Report Player (Confirmed)'),
            array('command_id' => '20','command_key' => 'player_calladmin','command_name' => 'Call Admin on Player'),
            array('command_id' => '21','command_key' => 'admin_say','command_name' => 'Admin Say'),
            array('command_id' => '22','command_key' => 'player_say','command_name' => 'Player Say'),
            array('command_id' => '23','command_key' => 'admin_yell','command_name' => 'Admin Yell'),
            array('command_id' => '24','command_key' => 'player_yell','command_name' => 'Player Yell'),
            array('command_id' => '25','command_key' => 'admin_tell','command_name' => 'Admin Tell'),
            array('command_id' => '26','command_key' => 'player_tell','command_name' => 'Player Tell'),
            array('command_id' => '27','command_key' => 'self_whatis','command_name' => 'What Is'),
            array('command_id' => '28','command_key' => 'self_voip','command_name' => 'VOIP'),
            array('command_id' => '29','command_key' => 'self_rules','command_name' => 'Request Rules'),
            array('command_id' => '30','command_key' => 'round_restart','command_name' => 'Restart Current Round'),
            array('command_id' => '31','command_key' => 'round_next','command_name' => 'Run Next Round'),
            array('command_id' => '32','command_key' => 'round_end','command_name' => 'End Current Round'),
            array('command_id' => '33','command_key' => 'server_nuke','command_name' => 'Server Nuke'),
            array('command_id' => '34','command_key' => 'server_kickall','command_name' => 'Kick All Guests'),
            array('command_id' => '35','command_key' => 'adkats_exception','command_name' => 'Logged Exception'),
            array('command_id' => '36','command_key' => 'banenforcer_enforce','command_name' => 'Enforce Active Ban')
        );

        foreach($adkats_commands as $command)
        {
            if($id == $command['command_id']) return $command['command_name'];
        }
    }

    /**
     * Converts a UTC timestamp into the user local time
     * @param  string $time year-month-day hour:minute:seconds
     * @return string Converted time
     */
    public static function convertUTCToLocal($time, $raw = FALSE)
    {
        if(is_integer($time))
            $time = date('Y-m-d H:i:s', $time);

        if(\Auth::check())
        {
            $userzone = \Auth::user()->timezone;
        }
        else
        {
            $userzone = 'UTC';
        }

        $newDate = DateTime::createFromFormat('Y-m-d H:i:s', $time, new DateTimeZone('UTC'));

        $newDate->setTimeZone(new DateTimeZone($userzone));

        if($raw) return $newDate->format('Y-m-d H:i:s');

        return $newDate->format('M jS, Y g:i:s A T');
    }

    // Same as function convertUTCToLocal except in reverse
    public static function convertLocalToUTC($time, $raw = FALSE)
    {
        if(is_integer($time))
            $time = date('Y-m-d H:i:s', $time);

        if(\Auth::check())
        {
            $userzone = \Auth::user()->timezone;
        }
        else
        {
            $userzone = 'UTC';
        }

        $newDate = DateTime::createFromFormat('Y-m-d H:i:s', $time, new DateTimeZone($userzone));

        $newDate->setTimeZone(new DateTimeZone('UTC'));

        if($raw) return $newDate->format('Y-m-d H:i:s');

        return $newDate->format('M jS, Y g:i:s A T');
    }

    /**
     * Adds a record to the log table for all actions done on the webadmin, includes game activity
     * @param  string $code    OK, ERROR, DEBUG
     * @param  string $action  Action ran by user
     * @param  string $message Reponse of action
     * @param  string $game    BF3, BF4, NULL (NULL if action is by webadmin)
     * @return void
     */
    public static function createLogEntry($code = '', $action = NULL, $message = '', $game = NULL)
    {
        // Set the color class for when you view the logs page
        if($code == 'ok' || $code == 'success')
        {
            $color = 'green-bg';
        }
        else if($code == 'error')
        {
            $color = 'red-bg';
        }
        else if($code == 'debug')
        {
            $color = 'orange-bg';
        }
        else
        {
            $color = 'grey-bg';
        }

        $user = \Auth::user();

        $log = new \Applog;
        $log->created_at    = date('Y-m-d H:i:s', time());
        $log->code          = strtoupper($code);
        $log->codecolor     = $color;
        $log->user          = (!empty($user->gamer_name) ? $user->gamer_name : $user->username);
        $log->action        = $action;
        $log->message       = $message;
        $log->game          = (is_null($game) ? NULL : $game);
        $log->save();
    }

    /**
     * Fetchs the recent activity on the bitbucket repo for the web admin
     * @return array
     */
    public static function fetchRepoActivity()
    {
        // If we already have a cached version of the results
        // we need to decode the JSON object then return it
        if(Cache::has('app_history_bitbucket'))
        {
            return json_decode(Cache::get('app_history_bitbucket'), true);
        }

        // Fetch the RSS feed
        $content = file_get_contents("https://bitbucket.org/adifferentkindllc/web-admin-bf3-bf4/rss?token=15ea7e7c7e5f1e007a91391247906dd7");

        // Convert the content to its object format
        $feedobj = new \SimpleXmlElement($content);

        // Get all the feed items
        $items = $feedobj->channel->item;

        // Loop though all the items
        foreach($items as $item)
        {
            $parsed[] = array(
                'commitlink'    => last($item->link),
                'pubDate'       => last($item->pubDate),
                //'description'   => last($item->description),
                'author'        => last($item->author),
                'commitMessage' => trim(last($item->title))
            );
        }

        // Store the results in the cache for faster viewing for 1 hour
        if(!Cache::has('app_history_bitbucket'))
            Cache::put('app_history_bitbucket', json_encode($parsed, true), 60);

        // Return the results
        return $parsed;
    }

    /**
     * Checks if the current request is indeed the current page
     * @param  string  $request URI
     * @return boolean
     */
    public static function isCurrentPage($request = '')
    {
        if($request == \Request::path()) return ' class="current collapsible-current"';

        return NULL;
    }

    /**
     * Gets the current AdKats Version number for the specified server
     * @param  integer $id Server ID
     * @return string
     */
    public static function fetchAdkatsVersion($id = -1)
    {
        if(is_null($id) || $id < 1 || !is_numeric($id)) return self::doMessage('error', 'No server id was provided or invalid input', array('result' => $id));

        return DB::table('adkats_settings')->where('setting_name', '=' , 'Plugin Version')->where('server_id', '=', $id)->pluck('setting_value');
    }

    /**
     * Convert bytes to human readable format
     * @source http://codeaid.net/php/convert-size-in-bytes-to-a-human-readable-format-(php)
     * @param integer bytes Size in bytes to convert
     * @return string
     */
    public static function bytesToSize($bytes, $precision = 2)
    {
        $kilobyte = 1024;
        $megabyte = $kilobyte * 1024;
        $gigabyte = $megabyte * 1024;
        $terabyte = $gigabyte * 1024;

        if (($bytes >= 0) && ($bytes < $kilobyte)) {
            return $bytes . ' B';

        } elseif (($bytes >= $kilobyte) && ($bytes < $megabyte)) {
            return round($bytes / $kilobyte, $precision) . ' KB';

        } elseif (($bytes >= $megabyte) && ($bytes < $gigabyte)) {
            return round($bytes / $megabyte, $precision) . ' MB';

        } elseif (($bytes >= $gigabyte) && ($bytes < $terabyte)) {
            return round($bytes / $gigabyte, $precision) . ' GB';

        } elseif ($bytes >= $terabyte) {
            return round($bytes / $terabyte, $precision) . ' TB';
        } else {
            return $bytes . ' B';
        }
    }

    /**
     * Generates a random list of players from the database
     * @param  string  $game  BF3 or BF4
     * @param  integer $limit How many to return
     * @return array          List of players
     */
    public static function generateRandomPlayerList($game = '', $limit = 20)
    {
        // Gets the total amount of players in the database
        $totalplayers = \Player::max('PlayerID');

        // Gets the Game ID for the specified game
        $GameID = self::fetchGameId(strtoupper($game));

        // Set the iteration
        $iteration = 0;

        // If the iteration is less than the limit run the loop
        while($iteration <= $limit)
        {
            // Store the player object
            $player = \Player::where('GameID', '=', $GameID)
                            ->where('PlayerID', '=', rand(1, $totalplayers))
                            ->select('PlayerID', 'SoldierName')->first();

            // Did we get a player?
            if($player)
            {
                // Skip this iteration and try again if we have no player name
                if(empty($player->SoldierName)) continue;

                // Add the player to the players array to be sent back
                $players[] = $player;

                // Incrase the iteration loop by 1
                $iteration++;
            }
        }

        // Return the list of random players
        return $players;
    }

    /**
     * Checks if a port is open on requested server
     * @param  string  $host
     * @param  integer $port
     * @return boolean
     */
    public static function isPortOpen($host, $port = NULL)
    {
        $IP = self::getIPv4Addr($host);

        if(is_null($port)) $port = self::getPortNum($host);

        $c = @fsockopen($IP, $port, $errnum, $errstr, 0.50);

        if($c && is_resource($c))
        {
            return TRUE;
        }
        else if($c && !is_resource($c))
        {
            return FALSE;
        }
        else
        {
            return NULL;
        }

        fclose($c);
    }

    /**
     * Returns the ban list for a game
     * @param  string $game
     * @return array
     */
    public static function fetchBanList($game)
    {
        $bans = \Ban::where('tbl_games.Name', '=', strtoupper($game))
                 ->join('tbl_playerdata', 'adkats_bans.player_id', '=', 'tbl_playerdata.PlayerID')
                 ->join('tbl_games', 'tbl_playerdata.GameID', '=', 'tbl_games.GameID')
                 ->join('adkats_records_main', 'adkats_bans.latest_record_id', '=', 'adkats_records_main.record_id')
                 ->select('adkats_bans.*', 'adkats_records_main.*')->get();

        $data['aaData'] = array();

        if($bans)
        {
            foreach($bans as $ban)
            {
                $data['aaData'][] = array(
                    (int)$ban->ban_id,
                    link_to_action("PlayerController@" . strtolower($game) . "info", $ban->target_name, $ban->target_id),
                    Helper::convertUTCToLocal($ban->ban_startTime),
                    Helper::convertUTCToLocal($ban->ban_endTime),
                    $ban->record_message,
                    (!is_null($ban->source_id) ? link_to_action("PlayerController@" . strtolower($game) . "info", $ban->source_name, $ban->source_id) : $ban->source_name),
                    link_to_route(strtolower($game) . "banshow", 'Modify', $ban->ban_id, array('class' => 'button compact'))
                );
            }
        }

        return $data;
    }

    public static function roundToNearestFive($n = 0, $datetime = NULL)
    {
        if(!is_null($datetime))
        {
            $min = date('i', strtotime($datetime));

            return 5 * round($min /5);
        }

        return 5 * round($n /5);
    }

    /**
     * If message provided has any of these commands, trigger the correct response
     *
     * @param  string $message Message string to parse
     * @return
     */
    public static function checkParseChatCommand($message = '')
    {
        // Check if the user typed a command instead of an actual message
        if(!is_bool(strpos($message, "@")) && strpos($message, "@") == 0)
        {
            // Break the message down by spaces and put them into an array
            $split = explode(" ", $message);
        }
        else if(!is_bool(strpos($message, "!")) && strpos($message, "!") == 0)
        {
            // Break the message down by spaces and put them into an array
            $split = explode(" ", $message);
        }
        else if(!is_bool(strpos($message, "/@")) && strpos($message, "/@") == 0)
        {
            // Break the message down by spaces and put them into an array
            $split = explode(" ", $message);
        }
        else if(!is_bool(strpos($message, "/!")) && strpos($message, "/!") == 0)
        {
            // Break the message down by spaces and put them into an array
            $split = explode(" ", $message);
        }
        else if(!is_bool(strpos($message, "/")) && strpos($message, "/") == 0)
        {
            // Break the message down by spaces and put them into an array
            $split = explode(" ", $message);
        }
        else
        {
            // Return FALSE if no condition was met
            return FALSE;
        }

        // Retrive the commands from adkats_commmands table
        $commands = DB::table('adkats_commands')->select('command_text', 'command_key')->where('command_playerInteraction', 1)->get();

        // Loop through the command list
        foreach($commands as $command)
        {
            // Get the command from the message and check against the list to make sure it's valid
            if(substr($split[0], 1) == $command->command_text)
            {
                $key = $command->command_key;
                break;
            }
        }

        if(!isset($key))
            return FALSE;

        return $commands;

        return $result;
    }
}

/* End of file Helpers.php */
/* Location: ./classes/Helpers.php */

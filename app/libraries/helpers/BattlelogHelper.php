<?php namespace Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Helpers\Helper;
use Requests;

class BattlelogHelper
{
    /**
     * Main url for battlelog
     * @var string
     */
    public static $url = "http://battlelog.battlefield.com/";

    /**
     * Soldier name
     * @var string
     */
    public $personaName = '';

    /**
     * Soldier ID
     * @var integer
     */
    public $personaId = -1;

    public function fetchPersonaId($soldier = '', $game = 'BF3')
    {
        if(empty($soldier))
            return Helper::doMessage('error', 'You did not provide a soldier name.');

        $player = \Battlelog::where('personauser', '=', $soldier)->where('game', '=', $game)->first();

        if($player) return $player;

        $response = Requests::get(self::$url . strtolower($game) . '/user/' . $soldier . '/', array('X-AjaxNavigation' => 1, 'Expect'));

        $result = json_decode($response->body);

        if(isset($result->context->profilePersonas[0]))
        {
            $profile = $result->context->profilePersonas[0];
            $this->personaName = $profile->personaName;
            $this->personaId   = $profile->personaId;

            $game_id = Helper::fetchGameId(strtoupper($game));
            $playerid = \Player::where('SoldierName', '=', $this->personaName)->where('GameID', '=', $game_id)->pluck('PlayerID');

            if(!$playerid) return;

            $cachePlayer = new \Battlelog;
            $cachePlayer->personaid = $this->personaId;
            $cachePlayer->personaUser = $this->personaName;
            $cachePlayer->game = $game;
            $cachePlayer->player_id = $playerid;
            $cachePlayer->save();

            return \Battlelog::find($playerid);
        }

        return $result;
    }
}

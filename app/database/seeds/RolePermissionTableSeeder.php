<?php

class RolePermissionTableSeeder extends Seeder
{
    public function run()
    {
        $role_list = array(
            array('name' => 'Site Administrator'),
            array('name' => 'Battlefield 3 Admin'),
            array('name' => 'Battlefield 3 Sub-Admin'),
            array('name' => 'Battlefield 4 Admin'),
            array('name' => 'Battlefield 4 Sub-Admin'),
            array('name' => 'Banned'),
            array('name' => 'Registered'));

        $permission_list = array(
            array('name' => 'bf4_kill',    'display_name' => 'Kill Player'),
            array('name' => 'bf4_kick',    'display_name' => 'Kick Player'),
            array('name' => 'bf4_say',     'display_name' => 'Admin Say'),
            array('name' => 'bf4_tsay',    'display_name' => 'Admin Team Say'),
            array('name' => 'bf4_psay',    'display_name' => 'Admin Player Say'),
            array('name' => 'bf4_yell',    'display_name' => 'Admin Yell'),
            array('name' => 'bf4_tyell',   'display_name' => 'Admin Team Yell'),
            array('name' => 'bf4_pyell',   'display_name' => 'Admin Player Yell'),
            array('name' => 'bf4_ban',     'display_name' => 'Perma Ban'),
            array('name' => 'bf4_tban',    'display_name' => 'Temp Ban'),
            array('name' => 'bf4_punish',  'display_name' => 'Punish'),
            array('name' => 'bf4_forgive', 'display_name' => 'Forgive'),
            array('name' => 'bf4_team',    'display_name' => 'Team Switch'),
            array('name' => 'bf4_squad',   'display_name' => 'Squad Switch'),
            array('name' => 'bf4_nuke',    'display_name' => 'Nuke Server'),
            array('name' => 'bf4_kickall', 'display_name' => 'Kick All Players'),
            array('name' => 'bf3_kill',    'display_name' => 'Kill Player'),
            array('name' => 'bf3_kick',    'display_name' => 'Kick Player'),
            array('name' => 'bf3_say',     'display_name' => 'Admin Say'),
            array('name' => 'bf3_tsay',    'display_name' => 'Admin Team Say'),
            array('name' => 'bf3_psay',    'display_name' => 'Admin Player Say'),
            array('name' => 'bf3_yell',    'display_name' => 'Admin Yell'),
            array('name' => 'bf3_tyell',   'display_name' => 'Admin Team Yell'),
            array('name' => 'bf3_pyell',   'display_name' => 'Admin Player Yell'),
            array('name' => 'bf3_ban',     'display_name' => 'Perma Ban'),
            array('name' => 'bf3_tban',    'display_name' => 'Temp Ban'),
            array('name' => 'bf3_punish',  'display_name' => 'Punish'),
            array('name' => 'bf3_forgive', 'display_name' => 'Forgive'),
            array('name' => 'bf3_team',    'display_name' => 'Team Switch'),
            array('name' => 'bf3_squad',   'display_name' => 'Squad Switch'),
            array('name' => 'bf3_nuke',    'display_name' => 'Nuke Server'),
            array('name' => 'bf3_kickall', 'display_name' => 'Kick All Players'),
            array('name' => 'createuser',  'display_name' => 'Create Users'),
            array('name' => 'deleteuser',  'display_name' => 'Delete Users'),
            array('name' => 'edituser',    'display_name' => 'Edit Users'),
            array('name' => 'gamesetting', 'display_name' => 'Edit/View Game Settings'),
            array('name' => 'allperms',    'display_name' => 'All Permissions'),
            array('name' => 'viewplayer',  'display_name' => 'View Player Information'),
            array('name' => 'viewbf4admin','display_name' => 'View Battlefield 4 Admin Section'),
            array('name' => 'editbf4admin','display_name' => 'Edit Battlefield 4 Admin Section'),
            array('name' => 'viewbf3admin','display_name' => 'View Battlefield 3 Admin Section'),
            array('name' => 'editbf3admin','display_name' => 'Edit Battlefield 3 Admin Section'),
            array('name' => 'editbf3ban',  'display_name' => 'Edit Battlefield 3 Bans'),
            array('name' => 'editbf4ban',  'display_name' => 'Edit Battlefield 4 Bans'),
            array('name' => 'fullbfaccess','display_name' => 'Full Battlefield Access'));

        foreach($role_list as $role)
        {
            $newRole = new Role;
            $newRole->name = $role['name'];
            $newRole->save();

            switch($role['name'])
            {
                case "Site Administrator":
                    foreach($permission_list as $perm)
                    {
                        if($perm['name'] == 'allperms')
                        {
                            $newPerm = new Permission;
                            $newPerm->name = $perm['name'];
                            $newPerm->display_name = $perm['display_name'];
                            $newPerm->save();

                            $newRole->perms()->sync(array($newPerm->id));
                            break;
                        }
                    }
                break;

                case "Battlefield 3 Admin":
                    foreach($permission_list as $perm)
                    {
                        if(strpos($perm['name'], "bf3_") === 0 || $perm['name'] == 'editbf3ban' || $perm['name'] == 'viewbf3admin' || $perm['name'] == 'editbf3admin' || $perm['name'] == 'viewplayer')
                        {
                            // Skip if permission keys are nuke or kickall
                            if(in_array($perm['name'], array('bf3_nuke', 'bf3_kickall')))
                            {
                                continue;
                            }

                            $newPerm = new Permission;
                            $newPerm->name = $perm['name'];
                            $newPerm->display_name = $perm['display_name'];
                            $newPerm->save();

                            $addPerm[] = $newPerm->id;
                        }
                    }

                    $newRole->perms()->sync($addPerm);
                break;

                case "Battlefield 3 Sub-Admin":
                    foreach($permission_list as $perm)
                    {
                        if(strpos($perm['name'], "bf3_") === 0 || $perm['name'] == 'viewbf3admin' || $perm['name'] == 'viewplayer')
                        {
                            // Skip if permission keys are nuke or kickall or perma ban
                            if(in_array($perm['name'], array('bf3_nuke', 'bf3_kickall', 'bf3_ban')))
                            {
                                continue;
                            }

                            $newPerm = new Permission;
                            $newPerm->name = $perm['name'];
                            $newPerm->display_name = $perm['display_name'];
                            $newPerm->save();

                            $addPerm[] = $newPerm->id;
                        }
                    }

                    $newRole->perms()->sync($addPerm);
                break;

                case "Battlefield 4 Admin":
                    foreach($permission_list as $perm)
                    {
                        if(strpos($perm['name'], "bf4_") === 0 || $perm['name'] == 'editbf4ban' || $perm['name'] == 'viewbf4admin' || $perm['name'] == 'editbf4admin' || $perm['name'] == 'viewplayer')
                        {
                            // Skip if permission keys are nuke or kickall
                            if(in_array($perm['name'], array('bf4_nuke', 'bf4_kickall')))
                            {
                                continue;
                            }

                            $newPerm = new Permission;
                            $newPerm->name = $perm['name'];
                            $newPerm->display_name = $perm['display_name'];
                            $newPerm->save();

                            $addPerm[] = $newPerm->id;
                        }
                    }

                    $newRole->perms()->sync($addPerm);
                break;

                case "Battlefield 4 Sub-Admin":
                    foreach($permission_list as $perm)
                    {
                        if(strpos($perm['name'], "bf4_") === 0 || $perm['name'] == 'viewbf4admin' || $perm['name'] == 'viewplayer')
                        {
                            // Skip if permission keys are nuke or kickall or perma ban
                            if(in_array($perm['name'], array('bf4_nuke', 'bf4_kickall', 'bf4_ban')))
                            {
                                continue;
                            }

                            $newPerm = new Permission;
                            $newPerm->name = $perm['name'];
                            $newPerm->display_name = $perm['display_name'];
                            $newPerm->save();

                            $addPerm[] = $newPerm->id;
                        }
                    }

                    $newRole->perms()->sync($addPerm);
                break;
            }

            unset($addPerm);
        }

        $id = DB::table('users')->insertGetId(
            array(
                'username'          => 'Admin',
                'email'             => 'admin@localhost',
                'gamer_name'        => '',
                'password'          => Hash::make('admin'),
                'confirmed'         => true,
                'created_at'        => date('Y-m-d H:i:s', time()),
                'updated_at'        => date('Y-m-d H:i:s', time()),
                'confirmation_code' => str_random(32)
            )
        );

        $user = User::find($id);

        $user->roles()->attach(1);
    }
}

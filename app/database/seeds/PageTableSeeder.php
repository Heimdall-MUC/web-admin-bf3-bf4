<?php

class PageTableSeeder extends Seeder
{
    public function run()
    {
        $pages = array(
            // Battlefield 3 Pages
            array('title' => 'Live Scoreboard',     'uri' => 'bf3/scoreboard',              'section' => 'bf3'),
            array('title' => 'Player Search',       'uri' => 'bf3/playersearch',            'section' => 'bf3'),
            array('title' => 'Player Info',         'uri' => 'bf3/playerinfo',              'section' => 'bf3'),
            array('title' => 'Ban List',            'uri' => 'bf3/admin/banlist',           'section' => 'bf3admin'),
            array('title' => 'Manange Bans',        'uri' => 'bf3/admin/banmanangement',    'section' => 'bf3admin'),
            array('title' => 'Manange Players',     'uri' => 'bf3/admin/playermanangement', 'section' => 'bf3admin'),
            array('title' => 'AdKat Records',       'uri' => 'bf3/admin/records',           'section' => 'bf3admin'),
            array('title' => 'Server Chat Logs',    'uri' => 'bf3/admin/chatlog',           'section' => 'bf3admin'),
            // Battlefield 4 Pages
            array('title' => 'Live Scoreboard',     'uri' => 'bf4/scoreboard',              'section' => 'bf4'),
            array('title' => 'Player Search',       'uri' => 'bf4/playersearch',            'section' => 'bf4'),
            array('title' => 'Player Info',         'uri' => 'bf4/playerinfo',              'section' => 'bf4'),
            array('title' => 'Ban List',            'uri' => 'bf4/admin/banlist',           'section' => 'bf4admin'),
            array('title' => 'Manange Bans',        'uri' => 'bf4/admin/banmanangement',    'section' => 'bf4admin'),
            array('title' => 'Manange Players',     'uri' => 'bf4/admin/playermanangement', 'section' => 'bf4admin'),
            array('title' => 'AdKat Records',       'uri' => 'bf4/admin/records',           'section' => 'bf4admin'),
            array('title' => 'Server Chat Logs',    'uri' => 'bf4/admin/chatlog',           'section' => 'bf4admin'),
            // Web Admin Pages
            array('title' => 'Dashboard',               'uri' => 'dashboard',               'section' => 'webadmin'),
            array('title' => 'Member List',             'uri' => 'admin/users',             'section' => 'webadmin'),
            array('title' => 'Server Manangement',      'uri' => 'admin/servers',           'section' => 'webadmin'),
            array('title' => 'Roles &amp; Permissions', 'uri' => 'admin/role',              'section' => 'webadmin')
        );

        // Insert the pages into the database
        DB::table('pages')->insert($pages);
    }
}

<?php
use Illuminate\Database\Migrations\Migration;

class ConfideSetupUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Creates the users table
        if(!Schema::hasTable('users'))
        {
            Schema::create('users', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('username');
                $table->string('email');
                $table->string('gamer_name');
                $table->string('timezone')->default('UTC');
                $table->boolean('notification_popups')->default(true);
                $table->boolean('notification_sounds')->default(true);
                $table->string('password');
                $table->string('confirmation_code');
                $table->boolean('confirmed')->default(false);
                $table->timestamps();
            });
        }

        // Creates password reminders table
        if(!Schema::hasTable('password_reminders'))
        {
            Schema::create('password_reminders', function($table)
            {
                $table->engine = 'InnoDB';
                $table->string('email');
                $table->string('token');
                $table->timestamp('created_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_reminders');
        Schema::dropIfExists('users');
    }

}

<?php

use Illuminate\Database\Migrations\Migration;

class WebadminSetupTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('gamesettings'))
        {
            Schema::create('gamesettings', function($table)
            {
                $table->engine = 'InnoDB';
                $table->timestamps();
                $table->smallInteger('server_id')->unsigned()->primary();
                $table->text('rconHash')->nullable();
                $table->text('settings')->nullable();
                $table->foreign('server_id')
                      ->references('ServerID')->on('tbl_server')
                      ->onDelete('cascade')
                      ->onUpdate('cascade');
            });
        }

        if(!Schema::hasTable('log'))
        {
            Schema::create('log', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->dateTime('created_at');
                $table->string('game', 10)->nullable();
                $table->string('code');
                $table->string('codecolor');
                $table->string('user')->default('Guest');
                $table->string('action')->nullable();
                $table->text('message');
            });
        }

        if(!Schema::hasTable('pages'))
        {
            Schema::create('pages', function($table)
            {
                $table->engine = 'InnoDB';
                $table->string('title', 50);
                $table->string('uri', 50);
                $table->enum('section', array('bf3', 'bf3admin', 'bf4', 'bf4admin', 'webadmin'));
                $table->primary('uri');
            });
        }

        if(!Schema::hasTable('uploads'))
        {
            Schema::create('uploads', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->string('hash', 32);
                $table->string('mime', 100);
                $table->integer('bytes')->unsigned();
                $table->string('extension', 5);
                $table->string('original_name');
                $table->string('file_name');

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gamesettings', function($table)
        {
            $table->dropForeign('gamesettings_server_id_foreign');
        });

        Schema::table('uploads', function($table)
        {
            $table->dropForeign('uploads_user_id_foreign');
        });

        Schema::dropIfExists('gamesettings');
        Schema::dropIfExists('log');
        Schema::dropIfExists('pages');
        Schema::dropIfExists('uploads');
    }
}

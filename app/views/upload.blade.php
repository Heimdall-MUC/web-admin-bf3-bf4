@section('content')
    {{ Form::open(array('action' => 'FileController@store', 'method' => 'post', 'files' => true)) }}
        <p class="inline-label button-height">
            {{ Form::label('files', 'Upload File', array('class' => 'label')) }}
            {{ Form::file('files[]', array('class' => 'file', 'multiple' => true)) }}
        </p>
        {{ Form::submit('Click da button') }}
    {{ Form::close() }}
@stop

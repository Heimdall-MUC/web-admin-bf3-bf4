@section('content')
    <div class="six-columns eight-columns-tablet seven-columns-mobile">
        <p>{{ Helper::get_gravatar($info->user->email, 120, 'mm', 'g', true, array('class' => 'user-icon')) }}</p>
        <p class="inline-small-label">
            <label class="label">Group:</label>
            <span>{{ $info->group->name }}</span>
        </p>

        <p class="inline-label">
            <label class="label">In-game Name:</label>
            <span class="margin-left">{{ $info->user->gamer_name }}</span>
        </p>

        @if(Request::segment(3) == Auth::user()->username)
        <a href="profile/edit" class="button compact">Edit Profile</a>
        @endif
    </div>
@stop

<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if (gt IE 8)|(gt IEMobile 7)]><!--><html class="no-js" lang="en"><!--<![endif]-->

<head>
    <base href="{{ URL::to('/'); }}" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Web Admin - {{ $title }}</title>
    <meta name="description" content="Web Admin was designed and created by the ADK Gaming Community">
    <meta name="author" content="Prophet731, ColColonCleaner, ADKGamers">

    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <meta name="viewport" content="user-scalable=0, initial-scale=1.0">

    <!-- For all browsers -->
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/reset.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/style.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/colors.css?v=1">
    <link rel="stylesheet" media="print" href="{{ Helper::cdn() }}css/print.css?v=1">
    <!-- For progressively larger displays -->
    <link rel="stylesheet" media="only all and (min-width: 480px)" href="{{ Helper::cdn() }}css/480.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 768px)" href="{{ Helper::cdn() }}css/768.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 992px)" href="{{ Helper::cdn() }}css/992.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 1200px)" href="{{ Helper::cdn() }}css/1200.css?v=1">
    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="{{ Helper::cdn() }}css/2x.css?v=1">

    <!-- Webfonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel='stylesheet' type='text/css'>

    <!-- Additional styles -->
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/agenda.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/dashboard.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/backbone-ui.css">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/perka.css">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/files.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/form.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/modal.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/progress-slider.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/switches.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/table.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}js/libs/DataTables/jquery.dataTables.css">
    <link rel="stylesheet" href="{{ Helper::cdn() }}js/libs/glDatePicker/developr.fixed.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}js/libs/formValidator/developr.validationEngine.css">

    <!-- JavaScript at bottom except for Modernizr -->
    <script src="{{ Helper::cdn() }}js/libs/modernizr.custom.js"></script>

    <!-- For Modern Browsers -->
    <link rel="shortcut icon" href="{{ Helper::cdn() }}img/favicons/favicon.png">
    <!-- For everything else -->
    <link rel="shortcut icon" href="{{ Helper::cdn() }}img/favicons/favicon.ico">

    <script src="{{ Helper::cdn() }}js/libs/jquery-1.10.2.min.js"></script>

</head>

<body class="clearfix with-menu carbon">
    <audio controls="hidden" preload="auto" id="report_sound_notify">
        <source src="{{ Helper::cdn() }}media/sounds/computer/computerbeep_71.mp3" type="audio/mpeg">
    </audio>

    <!-- Prompt IE 6 users to install Chrome Frame -->
    <!--[if lt IE 7]><p class="message red-gradient simpler">Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->

    <!-- Button to open/hide menu -->
    <a href="#" id="open-menu"><span>Menu</span></a>

    <!-- Title bar -->
    <header role="banner" id="title-bar">
        <h2>Web Admin</h2>
    </header>

    <!-- Main content -->
    <section role="main" id="main">

        <!-- Visible only to browsers without javascript -->
        <noscript class="message black-gradient simpler">Your browser does not support JavaScript! Some features won't work as expected...</noscript>

        <!-- Main title -->
        <hgroup id="main-title" class="thin">
            <h1>{{ $title }}</h1>
        </hgroup>

        <div class="with-padding columns">
            @yield('content')
        </div>

    </section>
    <!-- End main content -->

    <!-- Sidebar/drop-down menu -->
    <section id="menu" role="complementary">

        <div id="menu-content">

            <header>
                {{ $group->name }}
            </header>

            <div id="profile">
                {{ Helper::get_gravatar($user->email, 64, 'mm', 'g', true, array('class' => 'user-icon')) }}
                <!-- Welcome back -->
                <h4 class="mid-margin-top white">{{ $user->username }}</h4>
                @if($authcheck)
                    <a href="user/logout" class="button anthracite-gradient compact icon-login">Logout</a>
                @else
                    <a href="user/login#form-login" class="button anthracite-gradient compact icon-login">Login</a>
                    <a href="user/login#form-register" class="button anthracite-gradient compact">Register</a>
                @endif
            </div>

            <ul id="access" class="children-tooltip">
                <!-- Icon with count -->
                @if($authcheck)
                <li><a href="user/profile/{{ $user->username }}" title="Profile"><span class="icon-user"></span></a></li>
                <li><a href="javascript://" title="Clear all popup notifications" onclick="$('.notification .close').click()"><span class="icon-trash"></span></a></li>
                <li><a href="http://www.adkgamers.com/forum/265-adk-web-dev-support/" target="_blank" title="Web Admin Support Section"><span class="icon-info-round"></span><span class="count">{{ Config::get('webadmin.version') }}</span></a></li>
                @endif
            </ul>

            <ul class="big-menu collapsible as-accordion">
                <li class="anthracite-gradient"><a href="{{ action('HomeController@index') }}"{{ Helper::isCurrentPage(action('HomeController@index')) }}>Dashboard</a></li>

                @if(Config::get('webadmin.bf3'))
                <li class="with-right-arrow">
                    <span class="anthracite-gradient"><span class="list-count">{{ count($navbf3) }}</span>BF3</span>
                    <ul class="big-menu">
                        @foreach($navbf3 as $nav)
                            <li class="green-gradient"><a href="{{ URL::to($nav->uri) }}"{{ Helper::isCurrentPage($nav->uri) }}>{{ $nav->title }}</a></li>
                        @endforeach
                        @if($authcheck && ($user->can("viewbf3admin") || $user->can("allperms")))
                        <li class="with-right-arrow">
                            <span class="anthracite-gradient"><span class="list-count">{{ count($navbf3admin) }}</span>--&gt; Admin</span>
                            <ul class="big-menu">
                                @foreach($navbf3admin as $nav)
                                    <li class="green-gradient"><a href="{{ URL::to($nav->uri) }}"{{ Helper::isCurrentPage($nav->uri) }}>{{ $nav->title }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif

                @if(Config::get('webadmin.bf4'))
                <li class="with-right-arrow">
                    <span class="anthracite-gradient"><span class="list-count">{{ count($navbf4) }}</span>BF4</span>
                    <ul class="big-menu">
                        @foreach($navbf4 as $nav)
                            <li class="green-gradient"><a href="{{ URL::to($nav->uri) }}"{{ Helper::isCurrentPage($nav->uri) }}>{{ $nav->title }}</a></li>
                        @endforeach
                        @if($authcheck && ($user->can("viewbf4admin") || $user->can("allperms")))
                        <li class="with-right-arrow">
                            <span class="anthracite-gradient"><span class="list-count">{{ count($navbf4admin) }}</span>--&gt; Admin</span>
                            <ul class="big-menu">
                                @foreach($navbf4admin as $nav)
                                    <li class="green-gradient"><a href="{{ URL::to($nav->uri) }}"{{ Helper::isCurrentPage($nav->uri) }}>{{ $nav->title }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif

                @if($authcheck && $user->can("allperms"))
                    <li class="with-right-arrow">
                    <span class="anthracite-gradient"><span class="list-count">{{ count($navweb) }}</span>Admin Backend</span>
                    <ul class="big-menu">
                        @foreach($navweb as $nav)
                            @if($nav->uri != 'dashboard')
                            <li class="green-gradient"><a href="{{ URL::to($nav->uri) }}"{{ Helper::isCurrentPage($nav->uri) }}>{{ $nav->title }}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @endif

            </ul>

        </div>
        <!-- End content wrapper -->

        @if($authcheck && !$user->hasRole('Registered'))
        <ul class="title-menu small-margin-top">
            <li>Recent Admin Reports</li>
        </ul>

        <ul class="message-menu bevel-on-light-subs dark-text-bevel-subs" id="report_container"></ul>
        @endif
        <!--FOOTER -->
        <ul class="title-menu margin-top">
            <li>Footer</li>
        </ul>

        <footer id="menu-footer" class="align-center small">
            <p><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Web Admin</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.adkgamers.com/" property="cc:attributionName" rel="cc:attributionURL">A Different Kind LLC</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.</p>
            <p><a href="https://bitbucket.org/adifferentkindllc/web-admin-bf3-bf4/src/ed2af84ba260eb3f6c91cef840686196b9260b5e/copyright.txt?at=master" title="ADK Copyright licence for this project" target="new">&copy; Copyright 2013-2014</a></p>
        </footer>
        <!-- END FOOTER -->


    </section>
    <!-- End sidebar/drop-down menu -->

    <!-- Scripts -->
    <script>var thisWebVersion = "{{ Config::get('webadmin.version') }}";</script>
    <script src="{{ Helper::cdn() }}js/setup.js"></script>
    <script src="{{ asset('js/functions.js') }}"></script>
    <script src="{{ Helper::cdn() }}js/libs/moment.min.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/livestamp.min.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/DataTables/jquery.dataTables.min.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/jstorage.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/idle-timer.min.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/jquery.blockUI.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/underscore.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/backbone.js"></script>

    <!-- Template functions -->
    <script src="{{ Helper::cdn() }}js/developr.tabs.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.input.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.auto-resizing.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.message.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.notify.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.scroll.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.tooltip.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.navigable.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.collapsible.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.progress-slider.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.table.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/jquery.tablesorter.min.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.content-panel.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.modal.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.confirm.js"></script>
    <script src="{{ Helper::cdn() }}js/libs/glDatePicker/glDatePicker.min.js"></script>
    @if($authcheck && $user->can("allperms"))
    <script type="text/javascript">
    $(function()
    {
        $.post('api/common/latest', {}, function(data, textStatus, xhr) {
            if(data.status == 'error')
            {
                $("#main").message(data.message, {
                    classes: ['red-gradient align-center'],
                    closable: false,
                    showCloseOnHover: false,
                    animate: false,
                });
            }
        });
    });
    </script>
    @endif
    @if($authcheck && !$user->hasRole('Registered'))
    <script type="text/javascript">
    $(function() {
        pullAdminReports();
        report = setInterval(pullAdminReports, 5000);
    });
    </script>
    @endif
</body>
</html>

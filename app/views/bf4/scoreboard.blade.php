@section('content')
    <div class="twelve-columns">
        <select class="select " id="server_select">
            <option value="none">Select Server</option>
            @foreach($serverlist as $server)
                <option value="{{ $server->ServerID }}">{{ Helper::formatServerName($server->ServerID) }} ({{ $server->usedSlots }}/{{ $server->maxSlots }})</option>
            @endforeach
        </select>

        <span class="with-small-padding" style="display:none;" id="auto_refresh_span">
            <input type="checkbox" name="scoreboard_refresh" id="scoreboard_refresh" checked="checked" class="switch with-tooltip medium wide" title="Enable or Disable auto-refresh of scoreboard." data-checkable-options='{"textOn": "AUTO", "textOff": "MANUAL"}' />
        </span>

        <span class="no-padding" style="display:none;" id="scoreboard_refresh_button">
            <a href="javascript://" class="button icon-refresh compact" onclick="getServerData($('#server_select').val());">Refresh</a>
        </span>
    </div>

    <div class="six-columns six-columns-mobile with-tooltip" title="Admins online in server" name="data_container">
        <p class="message black-gradient glossy scrollable" style="max-height:20px;" id="admins_online"></p>
    </div>

    <div class="two-columns hidden-on-mobile" name="data_container">
        <p class="message black-gradient glossy" id="round_time"></p>
    </div>

    <div class="two-columns hidden-on-mobile" name="data_container">
        <p class="message black-gradient glossy" id="server_uptime"></p>
    </div>

    <div class="two-columns six-columns-mobile" name="data_container">
        <p class="message black-gradient glossy" id="current_map"></p>
    </div>

    <div class="four-columns four-columns-tablet twelve-columns-mobile-portrait six-columns-mobile-landscape" name="data_container">
        <div class="table-header">
            US Team <span class="float-right">Tickets: (<span id="ticket_count_us">0</span>)</span>
        </div>
        <table class="simple-table responsive-table-on" id="team1">
            <thead>
                <th scope="col" class="low-padding">#</th>
                <th scope="col">Player</th>
                <th scope="col" class="hide-on-tablet">Score</th>
                <th scope="col" class="hide-on-tablet low-padding">Sq</th>
                <th scope="col" class="low-padding">K</th>
                <th scope="col" class="low-padding">D</th>
                <th scope="col" class="low-padding icon-wifi"></th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="four-columns four-columns-tablet twelve-columns-mobile-portrait six-columns-mobile-landscape" name="data_container">
        <div class="table-header">
            CN/RU Team <span class="float-right">Tickets: (<span id="ticket_count_cn">0</span>)</span>
        </div>
        <table class="simple-table responsive-table-on" id="team2">
            <thead>
                <th scope="col" class="low-padding">#</th>
                <th scope="col">Player</th>
                <th scope="col" class="hide-on-tablet">Score</th>
                <th scope="col" class="hide-on-tablet low-padding">Sq</th>
                <th scope="col" class="low-padding">K</th>
                <th scope="col" class="low-padding">D</th>
                <th scope="col" class="low-padding icon-wifi"></th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="four-columns four-columns-tablet twelve-columns-mobile" name="data_container">
        @if(Auth::check() && ($user->can('bf4_say') || $user->can('allperms')))
        <div class="large-margin-bottom">
            <input type="text" name="chat_type" id="chat_type" class="input full-width" placeholder="Type your message" maxlength="90" />
        </div>
        @endif
        <div id="chatlogs" class="scrollable" style="max-height:1000px;"></div>
    </div>

    @if(Auth::check() && ($user->can('viewbf4admin') || $user->can('allperms')))
    <script src="{{ asset('js/battlefield/4/admin.js') }}"></script>
    @else
    <script src="{{ asset('js/battlefield/4/scoreboard.js') }}"></script>
    @endif
@stop

@section('content')
    @if(Config::get('webadmin.bf3'))
    <div class="four-columns six-columns-tablet twelve-columns-mobile" id="server_population">
        <h3 class="underline thin">BF3 Population</h3>
        <div id="feed"></div>
        <div id="progress"></div>
    </div>
    @endif

    @if(Config::get('webadmin.bf4'))
    <div class="four-columns six-columns-tablet twelve-columns-mobile" id="server_population2">
        <h3 class="underline thin">BF4 Population</h3>
        <div id="feed"></div>
        <div id="progress"></div>
    </div>
    @endif

    @if(Config::get('webadmin.bf3'))
    <div class="two-columns four-columns-tablet six-columns-mobile new-row-tablet">
        <h3 class="underline thin">Recent BF3 Bans</h3>
        <ul class="bullet-list">
            @foreach($bf3bans as $ban)
            <li>
                {{ link_to_action("PlayerController@bf3info", (empty($ban->SoldierName) ? $ban->player_id : $ban->SoldierName), $ban->player_id) }}
                <span data-livestamp="{{ date('c', strtotime($ban->ban_startTime)) }}">
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    @if(Config::get('webadmin.bf4'))
    <div class="two-columns four-columns-tablet six-columns-mobile">
        <h3 class="underline thin">Recent BF4 Bans</h3>
        <ul class="bullet-list">
            @foreach($bf4bans as $ban)
            <li>
                {{ link_to_action("PlayerController@bf4info", (empty($ban->SoldierName) ? $ban->player_id : $ban->SoldierName), $ban->player_id) }}
                <span data-livestamp="{{ date('c', strtotime($ban->ban_startTime)) }}"></span>
            </li>
            @endforeach
        </ul>
    </div>
    @endif

    <script type="text/javascript">
    $(document).ready(function()
    {
        @if(Config::get('webadmin.bf3') && Config::get('webadmin.bf4'))
            populationFeed('bf3');
            populationFeed('bf4');
            setInterval(function(){
                populationFeed('bf3');
                populationFeed('bf4');
            }, 10000);
            $('#server_population, #server_population2').find('#progress').progress(0+'%',{ size: false });
        @elseif(Config::get('webadmin.bf3') && !Config::get('webadmin.bf4'))
            populationFeed('bf3');
            setInterval(populationFeed('bf3'), 10000);
            $('#server_population').find('#progress').progress(0+'%',{ size: false });
        @elseif(!Config::get('webadmin.bf3') && Config::get('webadmin.bf4'))
            populationFeed('bf4');
            setInterval(populationFeed('bf4'), 10000);
            $('#server_population2').find('#progress').progress(0+'%',{ size: false });
        @endif
    });
    </script>
@stop

<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Hello, {{ $user->username }}</h2>

        <div>
            <p>
            This email is to notify you that your password has been changed by the site administrator. Your password has been changed to {{ $newpassword }}
            </p>
            <p>You can change your password on your {{ link_to_action('UserController@edit_profile', 'profile') }} under account settings.</p>
            <p>If you are unable to login with your new password you can visit the {{ link_to('user/login#form-password', 'lost password') }} form to request a new one.</p>
        </div>
    </body>
</html>

@section('content')

    <div class="twelve-columns">

        @if(Session::get('success'))
        <p class="boxed left-border green-gradient">{{ Session::get('success') }}</p>
        @endif

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        <table class="simple-table responsive-table">
            <thead>
                <tr>
                    <th scope="col">Username</th>
                    <th>Role</th>
                    <th class="hide-on-tablet">Created</th>
                    <th>In-Game Name</th>
                    <th>Timezone</th>
                    <th class="hide-on-tablet">Confirmed</th>
                    <th class="low-padding">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ link_to('user/profile/' . $user->username, $user->username) }}</th>
                    <td>{{ $user->getRole()->name }}</td>
                    <td class="hide-on-tablet">{{ Helper::convertUTCToLocal($user->created_at) }}</td>
                    <td>{{ $user->gamer_name }}</td>
                    <td>{{ $user->timezone }}</td>
                    <td class="hide-on-tablet">{{ ($user->confirmed ? 'Yes' : 'No') }}</td>
                    <td class="low-padding">
                        <span class="button-group">
                            {{ link_to_action("AdminUserController@edit", "Edit", $user->id, array('class' => 'button green-gradient icon-card compact')) }}
                            {{ link_to_action("AdminUserController@destroy", "Delete", $user->id, array('class' => 'button red-gradient icon-trash confirm compact')) }}
                        </span>
                    </td>
                </td>
                @endforeach
            </tbody>
        </table>
    </div>

@stop

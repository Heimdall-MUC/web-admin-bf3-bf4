@section('content')

    <div class="twelve-columns">

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        @foreach($errors->all() as $message)
        <p class="boxed left-border red-gradient">{{ $message }}</p>
        @endforeach

        {{ Form::open(array('action' => array('AdminUserController@update', $user->id))) }}

        <fieldset class="fieldset fields-list silver-bg black-inputs">

            <legend class="legend">Edit User</legend>

            <div class="field-block button-height">
                {{ Form::label('user_username', 'Username', array('class' => 'label')) }}
                {{ Form::text('user_username', $user->username, array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('email', 'Email', array('class' => 'label')) }}
                {{ Form::text('email', $user->email, array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('ign', 'In-Game Name', array('class' => 'label')) }}
                {{ Form::text('ign', $user->gamer_name, array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('password', 'Change Password', array('class' => 'label')) }}
                {{ Form::password('password', array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('status', 'Confirmed', array('class' => 'label')) }}
                {{ Form::checkbox('status', 1, $user->confirmed, array('class' => 'switch wider', 'data-text-on' => 'Active', 'data-text-off' => 'Inactive')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('role', 'Role', array('class' => 'label')) }}
                {{ Form::select('role', $roles, $user->getRole()->role_id, array('class' => 'select anthracite-gradient')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('timezone', 'Timezone', array('class' => 'label')) }}
                {{ Form::select('timezone', Helper::generate_timezone_list(), $user->timezone, array('class' => 'select anthracite-gradient')) }}
            </div>

            <div class="field-block button-height">

                <span class="button-group float-right">
                    {{ link_to_action("AdminUserController@index", "Cancel", NULL, array('class' => 'button black-gradient icon-cross')) }}
                    {{ link_to_action("AdminUserController@destroy", "Delete", $user->id, array('class' => 'button red-gradient icon-trash confirm')) }}
                    <button class="button green-gradient icon-download">Save Changes</button>
                </span>

            </div>

        </fieldset>

        {{ Form::close() }}
    </div>

@stop

@section('content')

    <div class="twelve-columns">

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        @foreach($errors->all() as $message)
        <p class="boxed left-border red-gradient">{{ $message }}</p>
        @endforeach

        {{ Form::open(array('action' => array('AdminBattlefieldController@update', $server->server_id))) }}

        <fieldset class="fieldset fields-list silver-bg black-inputs">

            <div class="field-block button-height">
                <small class="input-info">Leave blank to keep the current RCON Password</small>
                {{ Form::label('rconpass', 'RCON Password', array('class' => 'label')) }}
                {{ Form::text('rconpass', NULL, array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height">
                <small class="input-info">Case Sensitive</small>
                {{ Form::label('nameformat', 'Format Server Name', array('class' => 'label')) }}
                {{ Form::text('nameformat', $server->settings, array('class' => 'input full-width')) }}

                <p>
                    <strong>Server Name</strong>: {{ $server->serverinfo->ServerName }}<br/>
                    <strong>Modified Server Name</strong>: <span id="modname">{{ Helper::formatServerName($server->server_id) }}</span>
                </p>

                <p>
                    If you would like to format your server name in the web admin and only show the stuff you actually need to see
                    then add them to the field above. Enter each word and/or character and seprate them by a comma.
                </p>
                <p>
                    For example take this server name

                    <pre class="prettyprint black-gradient glossy">=ADK= 24/7 Metro | No EXPLOSIVES | 1000 Tickets |</pre>

                    and make it look like this

                    <pre class="prettyprint black-gradient glossy">Metro No EXPLOSIVES</pre>

                    This happens based on the following paramaters

                    <pre class="prettyprint black-gradient glossy">=ADK=,24/7,|,1000 Tickets</pre>
                </p>
                <p class="red"><b>THIS WILL NOT CHANGE YOUR ACTUAL SERVER NAME. IT WILL ONLY BE REFLECTED ON THE WEB ADMIN.</b></p>
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('status', 'Status', array('class' => 'label')) }}
                {{ Form::checkbox('status', 'on', ($server->serverinfo->ConnectionState == 'on' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Enabled', 'data-text-off' => 'Disabled')) }}
            </div>

            <div class="field-block button-height">
                <div class="wrapped big-left-icon icon-info-round margin-bottom anthracite-bg">
                    <h4 class="no-margin-bottom">Note</h4>
                    This is only for the web admin. It does not affect your actual game server.
                </div>
            </div>

            <div class="field-block button-height">

                <span class="button-group float-right">
                    {{ link_to_action("AdminBattlefieldController@index", "Cancel", NULL, array('class' => 'button black-gradient icon-cross')) }}
                    {{ link_to_action("AdminBattlefieldController@destroy", "Delete", $server->server_id, array('class' => 'button red-gradient icon-trash confirm')) }}
                    <button class="button green-gradient icon-download">Save Changes</button>
                </span>

            </div>

        </fieldset>

        <input type="hidden" name="serverid" value="{{ $server->serverinfo->ServerID }}">

        {{ Form::close() }}
    </div>

    <script type="text/javascript">
    $("[name='nameformat']").keyup(function()
    {
        $.post('admin/servers/format', {serverid: '{{ $server->serverinfo->ServerID }}', split_string: $(this).val()}, function(data)
        {
            $("#modname").text(data.message);
        });
    });
    </script>

@stop

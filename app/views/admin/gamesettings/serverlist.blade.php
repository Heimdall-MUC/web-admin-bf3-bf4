@section('content')

    <div class="twelve-columns">
        @if(Session::get('success'))
        <p class="boxed left-border green-gradient">{{ Session::get('success') }}</p>
        @endif

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        <table class="simple-table responsive-table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>IP</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Conn. Status</th>
                    <th class="low-padding">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($servers as $server)
                <tr>
                    <td>{{ $server->id }}</td>
                    <td>{{ $server->ip }}</td>
                    <td>{{ $server->name }}</td>
                    <td>
                        @if($server->status)
                        <span class="tag green-bg">Enabled</span>
                        @else
                        <span class="tag red-bg">Disabled</span>
                        @endif
                    </td>
                    <td>
                        @if(!is_null($server->conn))
                            @if($server->conn)
                            <span class="tag green-bg">Successful</span>
                            @else
                            <span class="tag red-bg">Failed! TCP Port {{ Helper::getPortNum($server->ip) }} needs to be open</span>
                            @endif
                        @else
                        <span class="tag black-bg">Connection Failed</span>
                        @endif
                    </td>
                    <td class="low-padding">
                        @if($setting = Gamesetting::find($server->id))
                        {{ link_to_action("AdminBattlefieldController@edit", "Edit", $server->id, array('class' => 'button green-gradient compact')) }}
                        {{ link_to_action("AdminBattlefieldController@destroy", "Delete", $server->id, array('class' => 'button red-gradient confirm compact')) }}
                        @else
                        {{ link_to_action("AdminBattlefieldController@create", "Add", $server->id, array('class' => 'button orange-gradient compact')) }}
                        @endif
                    </td>
                </td>
                @endforeach
            </tbody>
        </table>
    </div>

@stop

@section('content')

    <div class="twelve-columns">
        @if(Session::get('success'))
        <p class="boxed left-border green-gradient">{{ Session::get('success') }}</p>
        @endif

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        <table class="simple-table">
            <thead>
                <tr>
                    <th>Role Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $role)
                <tr>
                    <td>{{ $role->name }}</td>
                    <td>
                        @if(!in_array($role->name, $protected_roles))
                        {{ link_to_action("AdminRoleController@edit", "Edit", $role->id, array('class' => 'button blue-gradient compact')) }}
                        {{ link_to_action("AdminRoleController@destroy", "Delete", $role->id, array('class' => 'button red-gradient compact confirm')) }}
                        @else
                        <button class="button blue-gradient disabled compact">Edit</button>
                        <button class="button red-gradient disabled compact">Delete</button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ link_to_action("AdminRoleController@create", "Create New Role", NULL, array('class' => 'button full-width anthracite-gradient glossy')) }}
    </div>

@stop

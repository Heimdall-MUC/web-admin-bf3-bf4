@section('content')

    <div class="twelve-columns">

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        @foreach($errors->all() as $message)
        <p class="boxed left-border red-gradient">{{ $message }}</p>
        @endforeach

        {{ Form::open(array('action' => array('AdminRoleController@update', $role->id))) }}

        <fieldset class="fieldset fields-list silver-bg">

            <legend class="legend">Edit Role</legend>

            <div class="field-block button-height">
                {{ Form::label('role_name', 'Role Name', array('class' => 'label')) }}
                {{ Form::text('role_name', $role->name, array('class' => 'input full-width')) }}
            </div>

            <div class="field-block button-height columns">
                {{ Form::label('perms', 'Permissions', array('class' => 'label')) }}

                <div class="four-columns six-columns-tablet">
                    <h4 class="thin">Battlefield 3 Permissions</h4>
                    <select class="select easy-multiple-selection check-list allow-empty" multiple="multiple" name="permsbf3[]">
                        @foreach($perms['bf3'] as $permkey => $name)
                        @if(in_array($permkey, $roleperms))
                        <option id="{{ $permkey }}" value="{{ $permkey }}" selected>{{ $name }}</option>
                        @else
                        <option id="{{ $permkey }}" value="{{ $permkey }}">{{ $name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>

                <div class="four-columns six-columns-tablet">
                    <h4 class="thin">Battlefield 4 Permissions</h4>
                    <select class="select easy-multiple-selection check-list allow-empty" multiple="multiple" name="permsbf4[]">
                        @foreach($perms['bf4'] as $permkey => $name)
                        @if(in_array($permkey, $roleperms))
                        <option id="{{ $permkey }}" value="{{ $permkey }}" selected>{{ $name }}</option>
                        @else
                        <option id="{{ $permkey }}" value="{{ $permkey }}">{{ $name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>

                <div class="four-columns twelve-columns-tablet new-row-tablet">
                    <h4 class="thin">General Permissions</h4>
                    <select class="select easy-multiple-selection check-list allow-empty" multiple="multiple" name="permswa[]">
                        @foreach($perms[0] as $permkey => $name)
                        @if(in_array($permkey, $roleperms))
                        <option id="{{ $permkey }}" value="{{ $permkey }}" selected>{{ $name }}</option>
                        @else
                        <option id="{{ $permkey }}" value="{{ $permkey }}">{{ $name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="field-block button-height">
                <span class="button-group">
                    <button class="button" id="select_all_bf3">
                        Select All BF3 Perms
                    </button>
                    <button class="button" id="select_all_bf4">
                        Select All BF4 Perms
                    </button>
                    <button class="button" id="select_all_wa">
                        Select All Gen Perms
                    </button>
                </span>

                <span class="button-group float-right">
                    {{ link_to_action("AdminRoleController@index", "Cancel", NULL, array('class' => 'button black-gradient icon-cross')) }}
                    {{ link_to_action("AdminRoleController@destroy", "Delete", $role->id, array('class' => 'button red-gradient icon-trash confirm')) }}
                    <button class="button green-gradient icon-download">Edit Role</button>
                </span>

            </div>

        </fieldset>

        {{ Form::close() }}
    </div>
    <script type="text/javascript">
    $("#select_all_bf3").click(function()
    {
        event.preventDefault();
        $("[name='permsbf3[]'] option").prop('selected', 'selected').change();
    });

    $("#select_all_bf4").click(function()
    {
        event.preventDefault();
        $("[name='permsbf4[]'] option").prop('selected', 'selected').change();
    });

    $("#select_all_wa").click(function()
    {
        event.preventDefault();
        $("[name='permswa[]'] option").prop('selected', 'selected').change();
    });
    </script>

@stop

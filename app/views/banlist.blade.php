@section('content')
    <div class="twelve-columns">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Player</th>
                    <th>Ban Issued</th>
                    <th>Ban Expires</th>
                    <th>Ban Reason</th>
                    <th>Banning Admin</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("table"),
                tableStyled = false;

            table.dataTable({
                'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
                'aaSorting': [[0,'asc'],[2,'desc'],[3,'desc']],
                'bProcessing': true,
                'sAjaxSource': "{{ Request::segment(1) }}/admin/banlist",
                'fnServerData': function(sSource, aoData, fnCallback)
                {
                    $(".dataTables_processing").addClass('black-gradient glossy white').html('<span class="loader working on-dark"></span> Loading banlist...');
                    $.ajax({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                    }
                }
            });
        });
        </script>
    </div>
@stop

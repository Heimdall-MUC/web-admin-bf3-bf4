<!DOCTYPE html>

<!--[if IEMobile 7]><html class="no-js iem7 oldie linen"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js ie7 oldie linen" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js ie8 oldie linen" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie9 linen" lang="en"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--><html class="no-js linen" lang="en"><!--<![endif]-->

<head>
    <base href="{{ URL::to('/'); }}" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Web Admin</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- http://davidbcalhoun.com/2010/viewport-metatag -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <!-- http://www.kylejlarson.com/blog/2012/iphone-5-web-design/ -->
    <meta name="viewport" content="user-scalable=0, initial-scale=1.0">

    <!-- For all browsers -->
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/reset.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/style.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/colors.css?v=1">
    <link rel="stylesheet" media="print" href="{{ Helper::cdn() }}css/print.css?v=1">
    <!-- For progressively larger displays -->
    <link rel="stylesheet" media="only all and (min-width: 480px)" href="{{ Helper::cdn() }}css/480.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 768px)" href="{{ Helper::cdn() }}css/768.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 992px)" href="{{ Helper::cdn() }}css/992.css?v=1">
    <link rel="stylesheet" media="only all and (min-width: 1200px)" href="{{ Helper::cdn() }}css/1200.css?v=1">
    <!-- For Retina displays -->
    <link rel="stylesheet" media="only all and (-webkit-min-device-pixel-ratio: 1.5), only screen and (-o-min-device-pixel-ratio: 3/2), only screen and (min-device-pixel-ratio: 1.5)" href="{{ Helper::cdn() }}css/2x.css?v=1">

    <!-- Additional styles -->
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/form.css?v=1">
    <link rel="stylesheet" href="{{ Helper::cdn() }}css/styles/switches.css?v=1">

    <!-- Login pages styles -->
    <link rel="stylesheet" media="screen" href="{{ Helper::cdn() }}css/login.css?v=1">

    <!-- JavaScript at bottom except for Modernizr -->
    <script src="{{ Helper::cdn() }}js/libs/modernizr.custom.js"></script>

    <!-- Microsoft clear type rendering -->
    <meta http-equiv="cleartype" content="on">
</head>

<body class="carbon">

    <div id="container">

        <div id="form-wrapper">

            <div id="form-block" class="scratch-metal">
                <div id="form-viewport">

                    <form method="post" action="" id="form-login" class="input-wrapper blue-gradient glossy" title="Login">
                        <ul class="inputs black-input large">
                            <!-- The autocomplete="off" attributes is the only way to prevent webkit browsers from filling the inputs with yellow -->
                            <li><span class="icon-user mid-margin-right"></span><input type="text" name="identity" id="identity" value="" class="input-unstyled" placeholder="Username" autocomplete="off"></li>
                            <li><span class="icon-lock mid-margin-right"></span><input type="password" name="password" id="password" value="" class="input-unstyled" placeholder="Password" autocomplete="off"></li>
                        </ul>

                        <p class="button-height">
                            <button type="submit" class="button glossy float-right" id="login">Login</button>
                            <input type="checkbox" name="remember" id="remember" checked="checked" class="switch tiny mid-margin-right with-tooltip" title="Enable auto-login">
                            <label for="remember">Remember</label>
                        </p>
                    </form>

                    <form method="post" action="" id="form-password" class="input-wrapper orange-gradient glossy" title="Lost password?">

                        <p class="message">
                            If you can’t remember your password, fill the input below with your e-mail and we’ll send you a new one:
                            <span class="block-arrow"><span></span></span>
                        </p>

                        <ul class="inputs black-input large">
                            <li><span class="icon-mail mid-margin-right"></span><input type="email" name="mail" id="mail" value="" class="input-unstyled" placeholder="Your e-mail" autocomplete="off"></li>
                        </ul>

                        <button type="submit" class="button glossy full-width" id="send-password">Send new password</button>

                    </form>

                    <form method="post" action="" id="form-register" class="input-wrapper green-gradient glossy" title="Register">

                        <ul class="inputs black-input large">
                            <li><span class="icon-card mid-margin-right"></span><input type="text" name="gamertag" id="gametag" value="" class="input-unstyled" placeholder="Your In-game Name" autocomplete="off"></li>
                        </ul>
                        <ul class="inputs black-input large">
                            <li><span class="icon-user mid-margin-right"></span><input type="text" name="user" id="user-register" value="" class="input-unstyled" placeholder="Username" autocomplete="off"></li>
                            <li><span class="icon-mail mid-margin-right"></span><input type="email" name="mail" id="mail-register" value="" class="input-unstyled" placeholder="E-mail" autocomplete="off"></li>
                            <li><span class="icon-lock mid-margin-right"></span><input type="password" name="pass" id="pass-register" value="" class="input-unstyled" placeholder="Password" autocomplete="off"></li>
                            <li><span class="icon-lock mid-margin-right"></span><input type="password" name="pass_confirmation" id="passconfirm-register" value="" class="input-unstyled" placeholder="Confirm Password" autocomplete="off"></li>
                        </ul>

                        <button type="submit" class="button glossy full-width" id="send-register">Register</button>

                    </form>

                </div>
            </div>

        </div>
        <input type="hidden" value="{{ Session::get('redirect', URL::to('/')) }}" id="siteurl">


        <h5 class="small-margin-top align-center small">&copy; 2013 <a href="http://www.adkgamers.com/" target="_blank">ADKGamers</a></h5>

    </div>

    <!-- JavaScript at the bottom for fast page loading -->

    <!-- Scripts -->
    <script src="{{ Helper::cdn() }}js/libs/jquery-1.9.1.min.js"></script>
    <script src="{{ Helper::cdn() }}js/setup.js"></script>

    <!-- Template functions -->
    <script src="{{ Helper::cdn() }}js/developr.input.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.message.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.notify.js"></script>
    <script src="{{ Helper::cdn() }}js/developr.tooltip.js"></script>

    <script src="{{ asset('js/login.js') }}"></script>

</body>
</html>

@section('content')

    <div class="twelve-columns">

        @if(Session::get('notice'))
        <p class="boxed left-border blue-gradient">{{ Session::get('notice') }}</p>
        @endif

        @if(Session::get('success'))
        <p class="boxed left-border green-gradient">{{ Session::get('success') }}</p>
        @endif

        @foreach($errors->all() as $message)
        <p class="boxed left-border red-gradient">{{ $message }}</p>
        @endforeach

        {{ Form::open(array('url' => 'profile/edit')) }}
        <div class="standard-tabs white-bg">
            <ul class="tabs">
                <li class="active"><a href="#basic">Basic Info</a></li>
                <li><a href="#account">Account Settings</a></li>
                <li><a href="#site">Site Settings</a></li>
            </ul>

            <div class="tabs-content">
                <div id="basic" class="with-padding">
                    <fieldset class="fieldset fields-list">
                        <div class="field-block button-height required">
                            <label for="ign" class="label"><b>IGN</b></label>
                            <small class="input-info">Current In-Game Name: {{ $user->gamer_name }}</small>
                            <input type="text" name="ign" id="ign" class="input full-width" autocomplete="off">
                            <small class="input-info">This must match your in-game name in order for admin functions to work. Keep this updated!</small>
                        </div>

                        <div class="field-block button-height">
                            <label for="timezone" class="label"><b>Timezone</b></label>
                            <small class="input-info">Current Time: {{ $currentdatetime }}</small>
                            {{ Form::select('timezone', Helper::generate_timezone_list(), $user->timezone, array('class' => 'select anthracite-gradient')) }}
                        </div>
                    </fieldset>
                </div>

                <div id="account" class="with-padding">
                    <fieldset class="fieldset fields-list">
                        <div class="field-block button-height">
                            <label for="current_password" class="label"><b>Current Password</b></label>
                            <input type="password" name="current_password" id="current_password" class="input full-width" autocomplete="off">
                            <small class="input-info">Enter your password if your changing account settings. </small>
                        </div>

                        <div class="field-block button-height">
                            <label for="email" class="label"><b>E-mail</b></label>
                            <small class="input-info">Current email: {{ $user->email }}</small>
                            <input type="text" name="email" id="email" class="input full-width validate[custom[email]]" autocomplete="off">
                            <small class="input-info">Leave blank to keep current email</small>
                            <div class="left-icon icon-info-round mid-margin-top">
                                <h4 class="no-margin-bottom">Tip</h4>
                                To have your avatar shown use your <a href="https://gravatar.com/" target="new">Gravatar</a> email.
                            </div>
                        </div>

                        <div class="field-block button-height">
                            <label for="password" class="label"><b>Password</b></label>
                            <small class="input-info">Minimum of 6 characters</small>
                            <input type="password" name="password" id="password" class="input full-width" autocomplete="off">
                            <small class="input-info">Leave blank to keep current password</small>
                        </div>

                        <div class="field-block button-height">
                            <label for="password_confirmation" class="label"><b>Confirm Password</b></label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="input full-width" autocomplete="off">
                        </div>
                    </fieldset>
                </div>

                <div id="site" class="with-padding">
                    <p class="message orange-gradient">Will be added in the next update (v1.4.2)</p>
                    <fieldset class="fieldset fields-list">
                        <div class="field-block button-height">
                            <label for="notify" class="label"><b>Report Notifications</b></label>
                            <input type="checkbox" class="switch" id="notify">
                            <small class="input-info">Enables or Disables the admin report notifications (pop ups only)</small>
                        </div>

                        <div class="field-block button-height">
                            <label for="notify_sound" class="label"><b>Report Sounds</b></label>
                            <input type="checkbox" class="switch" id="notify_sound">
                            <small class="input-info">Enables or Disables the admin report notifications sounds</small>
                        </div>
                    </fieldset>
                </div>

            </div>
        </div>
        <span class="button-group float-right">
            <a href="user/profile/{{ $user->username }}" class="button red-gradient">Cancel</a>
            <a href="javascript:$('form').submit()" class="button green-gradient icon-download">Save</a>
        </span>
        {{ Form::close() }}
    </div>
@stop

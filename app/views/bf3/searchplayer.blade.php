@section('content')
    <div id="error_msg" class="twelve-columns"></div>
    <div class="ten-columns nine-columns-tablet twelve-columns-mobile block-label">
        <label for="player_search_input" class="label"><small>Search by Name, IP, or EAGUID</small>Search</label>
        <input id="player_search_input" class="input full-width"/>
        <button id="player_search_button" class="button compact blue-gradient glossy clear-both float-right">Search</button>
    </div>
    <div id="player_search_results" class="ten-columns nine-columns-tablet twelve-columns-mobile"></div>
    <div class="two-columns three-columns-tablet hidden-on-mobile">
        <h3 class="thin">Recent Players</h3>
        <ul class="bullet-list">
            @foreach($players as $player)
            <li>{{ link_to_action("PlayerController@bf3info", $player->SoldierName, $player->PlayerID, array('target' => '_blank')) }}</li>
            @endforeach
        </ul>
    </div>

    <script type="text/template" id="player_search_results_template">
        <h3 class="underline thin">Search Results</h3>
        <table class="table responsive-table-on" id="player_search_results_table">
            <thead>
                <tr>
                    <th scope="col">Player ID</th>
                    <th scope="col">Player Name</th>
                    <th scope="col">Player GUID</th>
                    <th scope="col">Game</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </script>
    <input type="hidden" id="thisurl" value="{{ Request::url() }}" />
    <input type="hidden" id="thisgame" value="{{ strtolower(Request::segment(1)) }}" />
    <script src="{{ asset('js/playersearch.js') }}"></script>
@stop

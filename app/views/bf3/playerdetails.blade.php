<?php
    $servers = Servers::select('ServerID', 'ServerName')->where('ConnectionState', 'on')->where('GameID', Helper::fetchGameId('bf3'))->orderBy(Config::get('webadmin.server_list_order'), 'asc')->get();

    if($servers)
    {
        foreach($servers as $server)
        {
            $serverlist[$server->ServerID] = $server->ServerName;
        }
    }
    else
    {
        $serverlist = array();
    }

    foreach(range(1, 12) as $hour)
    {
        if($hour < 10) $hour = '0' . $hour;

        foreach(range(0, 59, 5) as $min)
        {
            if($min < 10) $min = '0' . $min;
            $key = $hour . '_' . $min;
            $times[$key] = $hour . ':' . $min;
            unset($min);
        }
    }
?>
@section('content')

    <div class="twelve-columns">
        <h3 class="underline thin">Links</h3>
        <span class="button-group">
            <a href="http://battlelog.battlefield.com/bf3/user/{{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">Battlelog</a>

            <a href="http://bf3stats.com/stats_pc/{{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">BF3Stats</a>

            <a href="http://cheatometer.hedix.de/?p={{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">Hedix</a>

            <a href="http://www.team-des-fra.fr/CoM/bf3.php?p={{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">TeamDes</a>

            <a href="http://i-stats.net/index.php?action=pcheck&amp;game=BF3&amp;player={{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">I-Stats</a>

            <a href="http://history.anticheatinc.com/bf3/?searchvalue={{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">AntiCheatInc</a>

            <a href="http://metabans.com/search/?phrase={{ $info->player_info->SoldierName }}" target="_blank" class="button blue-gradient glossy">Metabans</a>
        </span>
    </div>

    <div class="six-columns">
        <h3 class="underline thin">General Info</h3>
        <p class="boxed anthracite-bg">Player: {{ $info->player_info->SoldierName }}</p>

        <p class="boxed anthracite-bg">EA GUID: {{ $info->player_info->EAGUID }}</p>

        <p class="boxed anthracite-bg">PB GUID: {{ $info->player_info->PBGUID }}</p>

        <p class="boxed anthracite-bg">IP Address: {{ $info->player_info->IP_Address }}</p>
        <span class="button-group">
            @if($authcheck && empty($info->player_ban->previousBans) && ($user->can('bf3_tban') || $user->can('bf3_ban') || $user->can('allperms')))
            <button id="issueban" class="button red-gradient glossy compact">Issue Ban</button>
            @elseif($authcheck && !empty($info->player_ban->previousBans) && ($user->can('editbf3ban') || $user->can('allperms')))
            {{ link_to_route(strtolower(Request::segment(1)) . "banshow", 'Modify Ban', $info->player_ban->currentBan->ban_id, array('class' => 'button blue-gradient glossy compact')) }}
            @endif
            @if($authcheck && ($user->can('bf3_forgive') || $user->can('allperms')))
            <button id="issueforgive" class="button green-gradient glossy compact">Issue Forgive</button>
            @endif
        </span>

    </div>

    <div class="six-columns">
        <h3 class="underline thin">Infraction Info</h3>
        @if(!empty($info->player_infraction['perServerInfractions']))

        <p class="boxed anthracite-bg">
            <span class="left-icon icon-cross icon-red"></span>
            {{ $info->player_info->SoldierName }} has {{ $info->player_infraction['overallInfactions']->total_points }} infraction points on your servers.
            Consisting of {{ $info->player_infraction['overallInfactions']->punish_points }} punishments and {{ $info->player_infraction['overallInfactions']->forgive_points }} forgives.
        </p>

        <table class="simple-table">
            <thead>
                <tr>
                    <th scope="col">Server</th>
                    <th scope="col">Punishments</th>
                    <th scope="col">Forgives</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($info->player_infraction['perServerInfractions'] as $infraction)
                <tr>
                    <td>{{ $infraction->ServerName }}</td>
                    <td>{{ $infraction->punish_points }}</td>
                    <td>{{ $infraction->forgive_points }}</td>
                    <td>{{ $infraction->total_points }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p class="big-message green-gradient white">
            <span class="left-icon icon-tick"></span>
            {{ $info->player_info->SoldierName }} has been very good thus far!
        </p>
        @endif
    </div>

    <div class="six-columns">
        <h3 class="underline thin">Current Ban</h3>
        @if(!empty($info->player_ban->currentBan) && $info->player_ban->currentBan->ban_status == 'Active')
        <table class="simple-table">
            <thead>
                <tr>
                    <th scope="col">Admin</th>
                    <th scope="col">Issued</th>
                    <th scope="col">Remaining</th>
                    <th scope="col">Reason</th>
                    <th width="150px">Notes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $info->player_ban->currentBan->source_name }}</td>
                    <td>{{ Helper::convertUTCToLocal($info->player_ban->currentBan->ban_startTime) }}</td>
                    <td>{{ Helper::formatBanTimespan(Helper::getRemainingTime($info->player_ban->currentBan->ban_endTime)) }}</td>
                    <td>{{ $info->player_ban->currentBan->record_message }}</td>
                    <td>{{ $info->player_ban->currentBan->ban_notes }}
                </tr>
            </tbody>
        </table>
        @else
        <p class="big-message green-gradient white">
            <span class="left-icon icon-tick"></span>
            {{ $info->player_info->SoldierName }} has no active ban.
        </p>
        @endif
    </div>

    @if(!empty($info->player_ban->previousBans))
    <div class="twelve-columns">
        <h3 class="underline thin">Previous Bans</h3>

        <table class="table" id="banhistory" style="display:none;">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Type</th>
                    <th>Banning Admin</th>
                    <th>Ban Reason</th>
                    <th>Issue Date</th>
                    <th>End Date</th>
                    <th>Server</th>
                </tr>
            </thead>
            <tbody>
                @foreach($info->player_ban->previousBans as $record)
                <tr>
                    <td>{{ $record->record_id }}</td>
                    <td>{{ Helper::getAdkatsCommandName($record->command_action) }}</td>
                    <td>{{ (!is_null($record->source_id) ? link_to_action("PlayerController@bf3info", $record->source_name, $record->source_id, array('target' => '_blank')) : $record->source_name) }}</td>
                    <td>{{ $record->record_message }}</td>
                    <td>{{ Helper::convertUTCToLocal($record->record_time) }}</td>
                    <td>{{ ($record->command_action == 8 ? 'Permanent' : Helper::convertUTCToLocal(strtotime('+' . $record->command_numeric . ' minutes', strtotime($record->record_time)))) }}</td>
                    <td>{{ Helper::formatServerName($record->server_id) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("#banhistory"),
                tableStyled = false;
            table.show();
            table.dataTable({
                'sDom': '<"dataTables_header">t<"dataTables_footer"ip>',
                'aaSorting': [[0,'desc']],
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                    }
                }
            });
        });
        </script>
    </div>
    @endif

    <div class="twelve-columns">
        <h3 class="underline thin">Record History</h3>
        @if(!empty($info->player_records))
        <table class="table" id="history" style="display:none;">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Action</th>
                    <th>Source</th>
                    <th>Target</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th>Server</th>
                </tr>
            </thead>
            <tbody>
                @foreach($info->player_records as $record)
                <tr>
                    <td>{{ $record->record_id }}</td>
                    <td>{{ Helper::getAdkatsCommandName($record->command_action) }}</td>
                    <td>{{ (!is_null($record->source_id) ? link_to_action("PlayerController@bf3info", $record->source_name, $record->source_id, array('target' => '_blank')) : $record->source_name) }}</td>
                    <td>{{ (!is_null($record->target_id) ? link_to_action("PlayerController@bf3info", $record->target_name, $record->target_id, array('target' => '_blank')) : $record->target_name) }}</td>
                    <td>{{ $record->record_message }}</td>
                    <td>{{ Helper::convertUTCToLocal($record->record_time) }}</td>
                    <td>{{ Helper::formatServerName($record->server_id) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("#history"),
                tableStyled = false;
            table.show();
            table.dataTable({
                'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
                'aaSorting': [[0,'desc']],
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                    }
                }
            });
        });
        </script>
        @else
        <p class="big-message anthracite-gradient white">
            <span class="left-icon icon-drawer"></span>
            {{ $info->player_info->SoldierName }} seems to be new. We don't have any records for them.
        </p>
        @endif
    </div>

    @if(!empty($sessions))
    <div class="twelve-columns" id="sessionhistory_container" style="display:none;">
        <h3 class="underline thin">Session History</h3>
        <table class="table" id="sessionhistory">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Rounds</th>
                    <th>Playtime</th>
                    <th>Server</th>
                </tr>
            </thead>
            <tbody>
                @foreach($sessions as $session)
                <tr>
                    <td>{{ $session->SessionID }}</td>
                    <td>{{ Helper::convertUTCToLocal($session->StartTime) }}</td>
                    <td>{{ Helper::convertUTCToLocal($session->EndTime) }}</td>
                    <td>{{ $session->RoundCount }}</td>
                    <td>{{ $session->Playtime }}</td>
                    <td>{{ $session->ServerName }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("#sessionhistory"),
                tableStyled = false;
            table.dataTable({
                'sDom': '<"dataTables_header"lr>t<"dataTables_footer"ip>',
                'aaSorting': [[0,'desc']],
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                    }
                }
            });

            $("#sessionhistory_container").show("fadeIn");
        });
        </script>
    </div>
    @endif

    @if(!empty($stats))
    <div class="twelve-columns" id="stats_container" style="display:none;">
        <h3 class="underline thin">Player Stats (Only for these servers)</h3>
        <table class="table" id="playerstats">
            <thead>
                <tr>
                    <th scope="col">Stats ID</th>
                    <th>First Seen</th>
                    <th>Last Seen</th>
                    <th>Playtime</th>
                    <th>Score</th>
                    <th>Kills</th>
                    <th>Deaths</th>
                    <th>Server</th>
                </tr>
            </thead>
            <tbody>
                @foreach($stats as $stat)
                <tr>
                    <td>{{ $stat->StatsID }}</td>
                    <td>{{ Helper::convertUTCToLocal($stat->FirstSeenOnServer) }}</td>
                    <td>{{ Helper::convertUTCToLocal($stat->LastSeenOnServer) }}</td>
                    <td>{{ Helper::seconds2human($stat->Playtime, FALSE, TRUE) }}</td>
                    <td>{{ number_format($stat->Score) }}</td>
                    <td>{{ number_format($stat->Kills) }}</td>
                    <td>{{ number_format($stat->Deaths) }}</td>
                    <td>{{ Helper::formatServerName($stat->ServerID) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("#playerstats"),
                tableStyled = false;
            table.dataTable({
                'sDom': '',
                'aaSorting': [[0,'desc']],
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                    }
                }
            });

            $("#stats_container").show("fast");
        });
        </script>
    </div>
    @endif

    <div class="twelve-columns">
        <h3 class="underline thin">Player Chatlogs (1000) <button class="button black-gradient" id="loadchat">Load chat <span id="chatloading"></span></button></h3>
        <table class="table" id="chatlog" style="display:none;">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Date/Time</th>
                    <th>Message</th>
                    <th>Server</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>

        <script type="text/javascript">
        var chatloaded = false;
        $("#loadchat").click(function()
        {
            if(chatloaded)
            {
                return false;
            }

            $("#chatloading").addClass('loader').addClass('on-dark');

            $.post('api/battlefield/playerchat/{{ $info->player_info->PlayerID }}', {}, function(data, textStatus, xhr)
            {
                if(xhr.status == 200 )
                {
                    var table = $("#chatlog"),
                        tableStyled = false;
                    table.dataTable({
                        'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
                        'aaSorting': [[0,'desc']],
                        'fnDrawCallback': function(oSettings)
                        {
                            // Only run once
                            if (!tableStyled)
                            {
                                table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                                tableStyled = true;
                                chatloaded = true;
                            }
                        }
                    });

                    $.each(data.more, function(index, val)
                    {
                        table.dataTable().fnAddData([
                            val.ID,
                            val.logDate,
                            val.logMessage,
                            val.ServerName
                        ]);
                    });
                }
            })
            .done(function()
            {
                $("#chatloading").removeClass('loader').removeClass('on-dark');
                $("#chatlog").show();
                $("#loadchat").hide();
            });
        });
        </script>
    </div>
    @if($authcheck && empty($info->player_ban->previousBans) && ($user->can('bf3_tban') || $user->can('bf3_ban') || $user->can('allperms')))
    <script type="text/javascript">
    $(function()
    {
        $("#issueban").click(function()
        {
            var html = '{{ Form::open(array('route' => 'bf3bancreate', 'id' => 'issuebanform')) }}';
            html += '{{ Form::hidden('game', strtoupper(Request::segment(1))) }}';
            html += '<fieldset class="fieldset fields-list">'
                        +'<legend class="legend">Create New Ban</legend>'
                        +'<div class="field-block button-height">'
                        +'    <label for="ban_targetname" class="label black">Player Name</label>'
                        +'    <input type="text" id="ban_targetname" name="ban_targetname" class="input full-width" value="{{ $info->player_info->SoldierName }}" disabled>'
                        +'    {{ Form::hidden('ban_targetname', $info->player_info->SoldierName) }}'
                        +'    {{ Form::hidden('ban_targetid', $info->player_info->PlayerID) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    <label for="ban_playerSourceName" class="label black">Banning Admin</label>'
                        +'    <input type="text" id="ban_playerSourceName" name="ban_playerSourceName" class="input full-width" value="{{ $user->gamer_name }}" disabled>'
                        +'    {{ Form::hidden('ban_playerSourceName', $user->gamer_name) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    <label for="ban_playerServer" class="label black">Server</label>'
                        +'    {{ Form::select('ban_playerServer', $serverlist, NULL, array('class' => 'select')) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    {{ Form::label('ban_playerReason', 'Ban Reason', array('class' => 'label black')) }}'
                        +'    {{ Form::text('ban_playerReason', '', array('class' => 'input full-width', 'maxlength' => 80)) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    {{ Form::label('ban_type', 'Ban Type', array('class' => 'label black')) }}'
                        +'    {{ Form::checkbox('ban_type', 8, FALSE, array('class' => 'switch wider', 'data-text-on' => 'Permanent', 'data-text-off' => 'Temporary')) }}'
                        +'</div>'
                        +'<div class="field-block button-height" id="timeblock">'
                        +'    <label for="ban_playerNewDate" class="label black">New Ban Time</label>'
                        +'    <p>'
                        +'        <span class="input">'
                        +'            <label class="button orange-gradient">Start Date</label>'
                        +'            <span class="icon-calendar"></span>'
                        +'            <input type="text" class="input-unstyled datepicker" id="modifystart_date" name="modifystart_date" value="{{ date('n/j/Y', strtotime(Helper::convertUTCToLocal(time(), true))) }}">'
                        +'        </span>'
                        +'        <span class="input">'
                        +'            <label class="button orange-gradient">Start Time</label>'
                        +'            {{ Form::select('modifystart_time', $times, date('h', strtotime(Helper::convertUTCToLocal(time(), true))) . '_' . Helper::roundToNearestFive(NULL, date('Y-m-d H:i:s', time())), array('class' => 'select compact', 'style' => 'width: 80px')) }}'
                        +'            {{ Form::select('modifystart_meridiem', array('am' => 'AM', 'pm' => 'PM'), date('a', strtotime(Helper::convertUTCToLocal(time(), true))), array('class' => 'select compact', 'style' => 'width: 60px')) }}'
                        +'        </span>'
                        +'    </p>'
                        +'    <p>'
                        +'        <span class="input">'
                        +'            <label class="button orange-gradient">End Date</label>'
                        +'            <span class="icon-calendar"></span>'
                        +'            <input type="text" class="input-unstyled datepicker" id="modifyend_date" name="modifyend_date">'
                        +'        </span>'
                        +'        <span class="input">'
                        +'            <label class="button orange-gradient">End Time</label>'
                        +'            {{ Form::select('modifyend_time', $times, date('h', strtotime(Helper::convertUTCToLocal(time(), true))) . '_' . Helper::roundToNearestFive(NULL, date('Y-m-d H:i:s', time())), array('class' => 'select compact', 'style' => 'width: 80px')) }}'
                        +'            {{ Form::select('modifyend_meridiem', array('am' => 'AM', 'pm' => 'PM'), date('a', strtotime(Helper::convertUTCToLocal(time(), true))), array('class' => 'select compact', 'style' => 'width: 60px')) }}'
                        +'        </span>'
                        +'    </p>'
                        +'</div>'
                        +'<div class="field-block button-height"><p>Bans are enforced by EAGUID by default. You can modify the ban once issued to enforce on other types</p></div>'
                        +'<div class="field-block button-height">'
                        +'    {{ Form::submit('Issue Ban', array('class' => 'button green-gradient')) }}'
                        +'</div>'
                    +'</fieldset>';
            html += '{{ Form::close() }}';
            $.modal({
                'title': 'Issue Ban',
                'content': html,
                onOpen: function()
                {
                    $.modal.current.getModalContentBlock().find('.datepicker').glDatePicker({
                        monthNames: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
                        selectableDOW: null,
                        zIndex: 999999999999999999,
                        onShow: function(calendar) {
                            calendar.addClass('black').show();
                        }
                    });

                    $("#ban_type").change(function()
                    {
                        if($(this).is(":checked"))
                        {
                            $("#timeblock").fadeOut();
                        }
                        else
                        {
                            $("#timeblock").fadeIn();
                        }
                    });

                    $("#issuebanform").submit(function(event)
                    {
                        event.preventDefault();

                        notify('Please wait...', 'Processing your request', {
                            autoClose: false,
                            classes: ['blue-gradient']
                        });

                        var curmodal = $.modal.current.getModalContentBlock()

                        $.ajax({
                            url: $(this).attr('action'),
                            type: $(this).attr('method'),
                            dataType: 'json',
                            data: $(this).serialize(),
                        })
                        .done(function(data) {
                            if(data.status == 'success')
                            {
                                $('.notification .close').click();

                                notify(data.status.toUpperCase(), data.message, {
                                    autoClose: false,
                                    classes: ['green-gradient']
                                });

                                curmodal.closeModal();
                                location.reload();
                            }
                            else
                            {
                                $('.notification .close').click();

                                curmodal.clearMessages();

                                $.each(data.more.errors, function(index, val) {
                                    curmodal.message(val, {
                                        classes: ['red-gradient'],
                                        showCloseOnHover: false,
                                        groupSimilar: false
                                    });
                                });
                            }
                        })
                        .fail(function() {
                            notify('ERROR', 'We could not process your request. Please try again.', {
                                autoClose: false,
                                classes: ['red-gradient']
                            });
                        });
                    });
                },
                'actions': {
                    'Cancel': {
                        color: 'red',
                        click: function(modal){modal.closeModal();}
                    }
                },
                'buttons': {
                    'Cancel': {
                        classes: 'blue-gradient glossy big full-width',
                        click: function(modal){modal.closeModal();}
                    }
                }
            });
        });
    });
    </script>
    @endif

    @if($authcheck && ($user->can('bf3_forgive') || $user->can('allperms')))
    <script type="text/javascript">
    $(function()
    {
        $("#issueforgive").click(function()
        {
            var html = '<fieldset class="fieldset fields-list" id="forgive_form">'
                        +'<legend class="legend">Issue Forgives</legend>'
                        +'<div class="field-block button-height">'
                        +'    <label for="forgive_server" class="label black">Server</label>'
                        +'    {{ Form::select('forgive_server', $serverlist, NULL, array('class' => 'select')) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    <label for="forgive_amount" class="label black">Amount</label>'
                        +'    {{ Form::selectRange('forgive_amount', 1, 100, null, array('class' => 'select')) }}'
                        +'</div>'
                        +'<div class="field-block button-height">'
                        +'    <label for="forgive_reason" class="label black">Reason</label>'
                        +'    {{ Form::text('forgive_reason', 'ForgivePlayer', array('class' => 'input full-width')) }}'
                        +'</div>'
                        +'{{ Form::hidden('forgive_targetid', $info->player_info->PlayerID) }}'
                        +'{{ Form::hidden('forgive_targetname', $info->player_info->SoldierName) }}'
                        +'{{ Form::hidden('forgive_game', strtoupper(Request::segment(1))) }}'
                    +'</fieldset>';
            $.modal({
                'title': 'Player Forgive',
                'content': html,
                'actions': {
                    'Cancel': {
                        color: 'red',
                        click: function(modal){modal.closeModal();}
                    }
                },
                'buttons': {
                    'Cancel': {
                        classes: 'blue-gradient glossy',
                        click: function(modal){modal.closeModal();}
                    },
                    'Issue': {
                        classes: 'green-gradient glossy',
                        click: function(modal){
                            notify('Please wait...', 'Processing your request', {
                                autoClose: false,
                                classes: ['blue-gradient']
                            });

                            $.ajax({
                                url: 'api/battlefield/forgive',
                                type: 'POST',
                                dataType: 'json',
                                data: $("#forgive_form").serialize(),
                            })
                            .done(function(data) {
                                if(data.status == 'success')
                                {
                                    $('.notification .close').click();

                                    notify(data.status.toUpperCase(), data.message, {
                                        autoClose: false,
                                        classes: ['green-gradient']
                                    });

                                    modal.closeModal();
                                    setTimeout(function() {
                                        location.reload();
                                    }, 3000);
                                }
                                else
                                {
                                    $('.notification .close').click();

                                    modal.getModalContentBlock().clearMessages();

                                    $.each(data.more.errors, function(index, val) {
                                        modal.getModalContentBlock().message(val, {
                                            classes: ['red-gradient'],
                                            showCloseOnHover: false,
                                            groupSimilar: false,
                                        });
                                    });
                                }
                            })
                            .fail(function() {
                                $('.notification .close').click();
                                notify('ERROR', 'We could not process your request. Please try again.', {
                                    autoClose: false,
                                    classes: ['red-gradient']
                                });
                            });
                        }
                    }
                }
            });
        });
    });
    </script>
    @endif
@stop

@section('content')
    <div class="twelve-columns">
        <p id="working">Processing... <span class="loader working on-dark"></span></p>
        <table class="table" style="display:none;">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th>Action</th>
                    <th>Source</th>
                    <th>Target</th>
                    <th>Message</th>
                    <th>Date</th>
                    <th>Server</th>
                </tr>
            </thead>
                @foreach($records as $record)
                <tr>
                    <td>{{ $record->record_id }}</td>
                    <td>{{ $record->command_name }}</td>
                    <td>{{ (!is_null($record->source_id) ? link_to_action("PlayerController@bf3info", $record->source_name, $record->source_id, array('target' => '_blank')) : $record->source_name) }}</td>
                    <td>{{ (!is_null($record->target_id) ? link_to_action("PlayerController@bf3info", $record->target_name, $record->target_id, array('target' => '_blank')) : $record->target_name) }}</td>
                    <td>{{ $record->record_message }}</td>
                    <td>{{ Helper::convertUTCToLocal($record->record_time) }}</td>
                    <td>{{ Helper::formatServerName($record->server_id) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
        $(document).ready(function()
        {
            var table = $("table"),
                tableStyled = false;
            table.dataTable({
                'sDom': '<"dataTables_header"lfr>t<"dataTables_footer"ip>',
                'aaSorting': [[0,'desc']],
                'fnDrawCallback': function(oSettings)
                {
                    // Only run once
                    if (!tableStyled)
                    {
                        table.closest('.dataTables_wrapper').find('.dataTables_length select').addClass('select blue-gradient glossy').styleSelect();
                        tableStyled = true;
                        table.show();
                        $("#working").remove();
                    }
                }
            });
        });
        </script>
    </div>
@stop

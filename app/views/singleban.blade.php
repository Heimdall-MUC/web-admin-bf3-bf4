@section('content')
    <div class="twelve-columns">
        @if(Session::get('success'))
        <p class="boxed left-border green-gradient">{{ Session::get('success') }}</p>
        @endif

        @if(Session::get('error'))
        <p class="boxed left-border red-gradient">{{ Session::get('error') }}</p>
        @endif

        @foreach($errors->all() as $message)
        <p class="boxed left-border red-gradient">{{ $message }}</p>
        @endforeach
        <p class="button-height">{{ link_to_route("{$game}banlist", "Back to Banlist", NULL, array('class' => 'button icon-left-fat blue-gradient')) }}</p>
        <div class="side-tabs white-bg same-height">

            <ul class="tabs">
                <li class="active"><a href="#baninfo" class="icon-info-round">Info</a></li>
                <li class="<?php echo ($edit ? NULL : 'disabled'); ?>"><a href="#banmodify">Modify</a></li>
                <li class="<?php echo ($edit ? NULL : 'disabled'); ?>"><a href="#newban">Create New Ban</a></li>
            </ul>

            <div class="tabs-content">

                <div id="baninfo" class="with-padding">
                    <fieldset class="fieldset fields-list">

                        <legend class="legend">Ban Record #{{ $ban->ban_id }}</legend>

                        <div class="field-block button-height">
                            <label for="ban_playerName" class="label black">Player Name</label>
                            <input type="text" id="ban_playerName" class="input full-width" value="{{ $ban->player->SoldierName }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerSourceName" class="label black">Banning Admin</label>
                            <input type="text" id="ban_playerSourceName" class="input full-width" value="{{ $ban->record->source_name }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerServer" class="label black">Server</label>
                            <input type="text" id="ban_playerServer" class="input full-width" value="{{ Helper::formatServerName($ban->record->server_id) }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerReason" class="label black">Ban Reason</label>
                            <input type="text" id="ban_playerReason" class="input full-width" value="{{ $ban->record->record_message }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label class="label black">Ban Notes</label>
                            <p>{{ $ban->ban_notes }}</p>
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_status', 'Ban Status', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_status', 'Active', ($ban->ban_status == 'Active' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Active', 'data-text-off' => 'Disabled', 'disabled' => 'disabled')) }}
                        </div>

                        <div class="field-block button-height">
                            <label class="label black">Details</label>
                            <p>Ban was issued on <b>{{ Helper::convertUTCToLocal($ban->ban_startTime) }}</b> and will end on <b>{{ Helper::convertUTCToLocal($ban->ban_endTime) }}</b></p>
                            <p>
                                {{ link_to_action("PlayerController@" . $game . "info", $ban->player->SoldierName . " profile", $ban->player->PlayerID, array('target' => '_blank', 'class' => 'button compact black-gradient glossy icon-user')) }}
                                {{ (!is_null($ban->record->source_id) ? link_to_action("PlayerController@" . $game . "info", $ban->record->source_name . " profile", $ban->record->source_id, array('target' => '_blank', 'class' => 'button compact black-gradient glossy icon-user')) : $ban->record->source_name) }}
                            </p>
                        </div>

                    </fieldset>
                </div>

                <div id="banmodify" class="with-padding">
                    {{ Form::open(array('route' => array($game . 'banupdate', $ban->ban_id))) }}
                    <fieldset class="fieldset fields-list">

                        <legend class="legend">Update Ban Record #{{ $ban->ban_id }}</legend>

                        <div class="field-block button-height">
                            <label for="ban_playerName" class="label black">Player Name</label>
                            <input type="text" id="ban_playerName" class="input full-width" value="{{ $ban->player->SoldierName }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerSourceName" class="label black">Banning Admin</label>
                            <input type="text" id="ban_playerSourceName" class="input full-width" value="{{ $ban->record->source_name }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerServer" class="label black">Server</label>
                            <input type="text" id="ban_playerServer" class="input full-width" value="{{ Helper::formatServerName($ban->record->server_id) }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_playerReason', 'Ban Reason', array('class' => 'label black')) }}
                            {{ Form::text('ban_playerReason', $ban->record->record_message, array('class' => 'input full-width', 'maxlength' => 80)) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_status', 'Ban Status', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_status', 'Active', ($ban->ban_status == 'Active' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Active', 'data-text-off' => 'Disabled')) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_type', 'Ban Type', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_type', 8, ($ban->record->command_action == 8 ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Permanent', 'data-text-off' => 'Temporary')) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_enforce_ip', 'Enforce by IP', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_enforce_ip', 'Y', ($ban->ban_enforceIP == 'Y' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Yes', 'data-text-off' => 'No')) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_enforce_name', 'Enforce by Name', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_enforce_name', 'Y', ($ban->ban_enforceName == 'Y' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Yes', 'data-text-off' => 'No')) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_enforce_guid', 'Enforce by EAGUID', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_enforce_guid', 'Y', ($ban->ban_enforceGUID == 'Y' ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Yes', 'data-text-off' => 'No')) }}
                        </div>

                        <div class="field-block button-height" id="timeblock">
                            <label for="ban_playerNewDate" class="label black">New Ban Time</label>

                            <p>
                                <span class="input">
                                    <label class="button orange-gradient">End Date</label>
                                    <span class="icon-calendar"></span>
                                    <input type="text" class="input-unstyled datepicker" id="modifyend_date" name="modifyend_date" value="{{ date('n/j/Y', strtotime(Helper::convertUTCToLocal($ban->ban_endTime, true))) }}">
                                </span>

                                <span class="input">
                                    <label class="button orange-gradient">End Time</label>
                                    {{ Form::select('modifyend_time', $times, date('h', strtotime(Helper::convertUTCToLocal($ban->ban_endTime, true))) . '_' . Helper::roundToNearestFive(NULL, $ban->ban_endTime), array('class' => 'select compact', 'style' => 'width: 80px')) }}
                                    {{ Form::select('modifyend_meridiem', array('am' => 'AM', 'pm' => 'PM'), date('a', strtotime(Helper::convertUTCToLocal($ban->ban_endTime, true))), array('class' => 'select compact', 'style' => 'width: 60px')) }}
                                </span>
                            </p>
                        </div>

                        <div class="field-block button-height">
                            {{ Form::submit('Update Ban', array('class' => 'button green-gradient')) }}
                        </div>

                    </fieldset>
                    {{ Form::close() }}
                </div>

                <div id="newban" class="with-padding">
                    <p class="boxed left-border">
                        This will create a new ban for this player. You will be set as the admin with this name <b>{{ $user->gamer_name }}</b>.
                    </p>
                    {{ Form::open(array('route' => $game . 'bancreate')) }}
                    {{ Form::hidden('ban_id', $ban->ban_id) }}
                    {{ Form::hidden('game', strtoupper(Request::segment(1))) }}
                    <fieldset class="fieldset fields-list">

                        <legend class="legend">Create New Ban</legend>

                        <div class="field-block button-height">
                            <label for="ban_playerName" class="label black">Player Name</label>
                            <input type="text" id="ban_playerName" name="ban_playerName" class="input full-width" value="{{ $ban->player->SoldierName }}" disabled>
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerSourceName" class="label black">Banning Admin</label>
                            <input type="text" id="ban_playerSourceName" name="ban_playerSourceName" class="input full-width" value="{{ $user->gamer_name }}" readonly>
                            {{ Form::hidden('ban_playerSourceName', $user->gamer_name) }}
                        </div>

                        <div class="field-block button-height">
                            <label for="ban_playerServer" class="label black">Server</label>
                            {{ Form::select('ban_playerServer', $serverlist, NULL, array('class' => 'select')) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_playerReason', 'Ban Reason', array('class' => 'label black')) }}
                            {{ Form::text('ban_playerReason', '', array('class' => 'input full-width', 'maxlength' => 80)) }}
                        </div>

                        <div class="field-block button-height">
                            {{ Form::label('ban_type', 'Ban Type', array('class' => 'label black')) }}
                            {{ Form::checkbox('ban_type', 8, ($ban->record->command_action == 8 ? TRUE : FALSE), array('class' => 'switch wider', 'data-text-on' => 'Permanent', 'data-text-off' => 'Temporary')) }}
                        </div>

                        <div class="field-block button-height" id="timeblock">
                            <label for="ban_playerNewDate" class="label black">New Ban Time</label>

                            <p>
                                <span class="input">
                                    <label class="button orange-gradient">Start Date</label>
                                    <span class="icon-calendar"></span>
                                    <input type="text" class="input-unstyled datepicker" id="modifystart_date" name="modifystart_date" value="{{ date('m/d/Y') }}">
                                </span>

                                <span class="input">
                                    <label class="button orange-gradient">Start Time</label>
                                    {{ Form::select('modifystart_time', $times, NULL, array('class' => 'select compact', 'style' => 'width: 80px')) }}
                                    {{ Form::select('modifystart_meridiem', array('am' => 'AM', 'pm' => 'PM'), date('a'), array('class' => 'select compact', 'style' => 'width: 60px')) }}
                                </span>
                            </p>

                            <p>
                                <span class="input">
                                    <label class="button orange-gradient">End Date</label>
                                    <span class="icon-calendar"></span>
                                    <input type="text" class="input-unstyled datepicker" id="modifyend_date" name="modifyend_date">
                                </span>

                                <span class="input">
                                    <label class="button orange-gradient">End Time</label>
                                    {{ Form::select('modifyend_time', $times, NULL, array('class' => 'select compact', 'style' => 'width: 80px')) }}
                                    {{ Form::select('modifyend_meridiem', array('am' => 'AM', 'pm' => 'PM'), date('a'), array('class' => 'select compact', 'style' => 'width: 60px')) }}
                                </span>
                            </p>
                        </div>

                        <div class="field-block button-height">
                            {{ Form::submit('Create Ban', array('class' => 'button green-gradient')) }}
                        </div>

                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function()
    {
        $('#banmodify, #newban').on('showtabinit', function()
        {
            $(this).find('.datepicker').glDatePicker({
                monthNames: [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ],
                selectableDOW: null,
                zIndex: 100,
                onShow: function(calendar) {
                    calendar.addClass('black').show();
                }
            });
        });

        $("#banmodify").find('#ban_type').change(function()
        {
            if($(this).is(":checked"))
            {
                $("#banmodify").find("#timeblock").fadeOut();
            }
            else
            {
                $("#banmodify").find("#timeblock").fadeIn();
            }
        });

        if($("#banmodify").find('#ban_type').is(":checked"))
        {
            $("#banmodify").find("#timeblock").fadeOut();
        }

        $("#newban").find('#ban_type').change(function()
        {
            if($(this).is(":checked"))
            {
                $("#newban").find("#timeblock").fadeOut();
            }
            else
            {
                $("#newban").find("#timeblock").fadeIn();
            }
        });

        if($("#newban").find('#ban_type').is(":checked"))
        {
            $("#newban").find("#timeblock").fadeOut();
        }
    });
    </script>
@stop
